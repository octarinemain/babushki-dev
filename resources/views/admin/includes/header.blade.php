    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="theme-color" content="#2e2f32">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="description" content="">
        <title>Admin</title>
        <link rel="stylesheet" href="{{ asset('public/admin/css/normalize.css') }}">
        <link rel="stylesheet" href="{{ asset('public/admin/css/swiper.min.css') }}">
        <link rel="stylesheet" href="{{ asset('public/admin/css/jquery-ui-datepicker.min.css') }}">
        <link rel="stylesheet" href="{{ asset('public/admin/css/main.css') }}">
        <link rel="stylesheet" href="{{ asset('public/admin/fonts/roboto/roboto.css') }}">
        <link rel="stylesheet" href="{{ asset('public/frontend/css/jquery.kladr.min.css') }}">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <script src="{{ asset('public/admin/js/jquery.min.js') }}"></script>
        <script src="{{ asset('public/frontend/js/jquery.kladr.min.js') }}" defer></script>
        <style >
            .swiper-slide.menu.swiper-slide-active{
                width: 250px;
            }
            .page-conent__open-menu {
                width: calc(100% - 250px) !important;
            }
        </style>

    </head>
    <body>
        <div id="app">
        @if(Auth::guard('admin')->check())
            @include('admin.layouts.menu')
        @endif

        <section class="page-conent  @if(Auth::guard('admin')->check()) page-conent__open-menu @endif">
            <header class="page-header">
                <div class="page-header__wrap">
                    <div class="menu-button cross">
                        <div class="bar"></div>
                        <div class="bar"></div>
                        <div class="bar"></div>
                    </div>
                </div>
            </header>
@yield('content')
@extends('admin.includes.footer')