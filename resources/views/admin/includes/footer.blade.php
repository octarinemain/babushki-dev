</section>
</div>
<script>
    $(function() {
        var $city = $('[name="city"]');
        var $city_1 = $('[name="filter_city"]');

        $.kladr.setDefault({
            parentInput: '.js-form-address',
            verify: true,
            select: function (obj) {
                setLabel($(this), obj.type);
            },
            check: function (obj) {
                var $input = $(this);
                $('[name="city"]').val()

                if (obj) {
                    setLabel($input, obj.type);
                }
            },
            checkBefore: function () {
                var $input = $(this);
            }
        });

        $city.kladr('type', $.kladr.type.city);
        $city_1.kladr('type', $.kladr.type.city);

        function setLabel($input, text) {
            text = text.charAt(0).toUpperCase() + text.substr(1).toLowerCase();
        }

        var hiddenBlock = $('#kladr_autocomplete').find('.autocomplete');
        var popup = $('.send-gift');

        popup.on('scroll', function() {
            hiddenBlock.css('display', 'none');
        })
    });
</script>

<footer>
</footer>
<script src="{{ asset('public/admin/js/swiper.min.js') }}"></script>
<script src="{{ asset('public/admin/js/jquery-ui-datepicker.min.js') }}"></script>
<script src="{{ asset('public/admin/js/main.js') }}"></script>
<script src="{{ asset('public/admin/js/app.js') }}"></script>
</body>
</html>