@extends('frontend.includes.header')

@section('content')
<section>
    <header class="header header_white">
        <div class="header__container container">
            @include('frontend.layouts.menu')
        </div>
    </header>
    <section class="reg-page">
        <div class="reg-page-form">
            <form class="form-horizontal" method="POST" action="{{ route('customer.login.submit') }}">
                {{ csrf_field() }}

                <div class="reg-page-form__title">
                    <p>Sign In</p>
                </div>
                @foreach ($errors->all() as $error)
                    <li class="registration-error">
                        <span>{{ $error }}</span>
                    </li>
                @endforeach
                <div class="reg-page-form__char search-form__char">
                    <span for="select" class="search-form__label">Your login</span>
                    <input type="text" class="reg-page-form__input" name="email" value="{{  old('email') }}">
                </div>
                <div class="reg-page-form__char search-form__char">
                    <span for="select" class="search-form__label">Pass</span>
                    <input type="password" class="reg-page-form__input" name="password" >
                </div>


                <div class="reg-page-form__char search-form__char reg-page-form__char_flex">
                    <button class="reg-page-form__btn reg-page-form__btn_left reg-page-form__btn_margin aqua-btn">Sign In</button>
                    <a href="{{ route('customer.password.request') }}" class="search-form__link">Forgot Your Password?</a>
                </div>

            </form>
        </div>
    </section>
</section>
<footer class="footer">
    <div class="container">
        @include('frontend.layouts.footer-content')
    </div>
    <div class="footer__bottom">
        @include('frontend.layouts.bottom-line')
    </div>
</footer>
@endsection
