@extends('admin.includes.header')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2 centered-form-wrap ">
            <div class="panel panel-default centered-form-wrap__popup centered-form-wrap__popup_login popup__wrap">
                <div class="panel-heading popup__title popup__title_left"> admin Login</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('admin.login.submit') }}">
                        {{ csrf_field() }}
                        @foreach ($errors->all() as $error)
                            <li class="registration-error popup__title">
                                <span>{{ $error }}</span>
                            </li>
                        @endforeach
                        <div class="popup__column-label form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="popup__column-label form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <button type="submit" class="red_btn page-actions__action btn btn--blue">Войти</button>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
