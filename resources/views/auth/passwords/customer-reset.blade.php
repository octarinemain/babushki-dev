@extends('frontend.includes.header')

@section('content')
    <section>
        <header class="header header_white">
            <div class="header__container container">
                @include('frontend.layouts.menu')
            </div>
        </header>
        <section class="reg-page">
            <div class="reg-page-form">
                <form class="form-horizontal" method="POST" action="{{ route('customer.password.request') }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="token" value="{{ $token }}">

                    <div class="reg-page-form__title">
                        <p>Reset password</p>
                    </div>
                    @foreach ($errors->all() as $error)
                        <li class="registration-error">
                            <span>{{ $error }}</span>
                        </li>
                    @endforeach
                    <div class="reg-page-form__char search-form__char">
                        <span for="select" class="search-form__label">Your login</span>
                        <input type="text" class="reg-page-form__input" name="email" value="{{ $email or old('email') }}">
                    </div>
                    <div class="reg-page-form__char search-form__char">
                        <span for="select" class="search-form__label">Pass</span>
                        <input type="password" class="reg-page-form__input" name="password" >
                    </div>
                    <div class="reg-page-form__char search-form__char">
                        <span for="select" class="search-form__label">Pass confirm</span>
                        <input type="password" class="reg-page-form__input" name="password_confirmation" >
                    </div>
                    <button class="reg-page-form__btn aqua-btn">Reset</button>
                </form>
            </div>
        </section>
    </section>
    <footer class="footer">
        <div class="container">
            @include('frontend.layouts.footer-content')
        </div>
        <div class="footer__bottom">
            @include('frontend.layouts.bottom-line')
        </div>
    </footer>
@endsection

