<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <title></title>
</head>

<body style="font-family:Gotham, 'Helvetica Neue', Helvetica, Arial, sans-serif; background-color:#f0f2ea; margin:0; padding:0; color:#000000;">

<table width="100%" bgcolor="#f0f2ea" cellpadding="0" cellspacing="0" border="0">
    <tbody>
    <tr>
        <td style="padding:40px 0;">
            <!-- begin main block -->
            <table cellpadding="0" cellspacing="0" width="608" border="0" align="center">
                <tbody>
                <tr>
                    <td>
                        <!-- begin wrapper -->
                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                            <tbody>
                            <tr>
                                <td width="8" height="4" colspan="2" style="background:url({{ asset('/public/frontend/img/shadow-top-left.png') }}) no-repeat 100% 100%;">
                                    <p style="margin:0; font-size:1px; line-height:1px;">&nbsp;</p>
                                </td>
                                <td height="4" style="background:url({{ asset('/public/frontend/img/shadow-top-center.png') }}) repeat-x 0 100%;">
                                    <p style="margin:0; font-size:1px; line-height:1px;">&nbsp;</p>
                                </td>
                                <td width="8" height="4" colspan="2" style="background:url({{ asset('/public/frontend/img/shadow-top-right.png') }}) no-repeat 0 100%;">
                                    <p style="margin:0; font-size:1px; line-height:1px;">&nbsp;</p>
                                </td>
                            </tr>

                            <tr>
                                <td width="4" height="4" style="background:url({{ asset('/public/frontend/img/shadow-left-top.png') }}) no-repeat 100% 0;">
                                    <p style="margin:0; font-size:1px; line-height:1px;">&nbsp;</p>
                                </td>
                                <td colspan="3" rowspan="3" bgcolor="#FFFFFF" style="padding:0 0 30px;">
                                    <!-- begin content -->
                                    <img src="{{ asset('/public/frontend/img/header.jpg') }}" width="640" height="230" alt="summer‘s coming trimm your sheeps"
                                         style="display:block; border:0; margin:0 0 44px; background:#eeeeee;">
                                    <p style="margin:0 30px 33px; font-size:18px; line-height:24px; font-weight: 400; color: #000000;">
                                        Здравствуйте, {{ $check->customer->name }}!
                                        <br>
                                        <br>Ваш чек был отклонён
                                        <br>Причина: {{ $admin_message }}
                                        <br>
                                    </p>
                                    <p style="margin:0; border-top:1px solid #e5e5e5; font-size:5px; line-height:5px; margin:0 30px 29px;">&nbsp;</p>
                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                        <tbody>
                                        <tr valign="top">
                                            <td width="30">
                                                <p style="margin:0; font-size:1px; line-height:1px;">&nbsp;</p>
                                            </td>
                                            <td>
                                                <p style="margin:0 0 4px; font-weight:400; color:#000000; font-size:14px; line-height:22px;text-align: center;">Если
                                                    у вас возникли проблемы или вопросы, <a href="mailto:info@jubileynoe.ru"
                                                                                            style="color: red;">напишите
                                                        нам</a></p>
                                                <p style="margin:0 0 4px; font-weight:400; color:#000000; font-size:14px; line-height:22px;text-align: center;"><a
                                                            href="http://xn--90aiakgkqi1l.xn--p1ai/customer/cancel-email-notifications/id={{ $check->customer->id }}&email={{ $check->customer->email }}" style="color: red;">Отписаться
                                                        от рассылки</a></p>
                                            </td>
                                            <td width="30">
                                                <p style="margin:0; font-size:1px; line-height:1px;">&nbsp;</p>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <!-- end content -->
                                </td>
                                <td width="4" height="4" style="background:url({{ asset('/public/frontend/img/shadow-right-top.png') }}) no-repeat 0 0;">
                                    <p style="margin:0; font-size:1px; line-height:1px;">&nbsp;</p>
                                </td>
                            </tr>


                            <tr>
                                <td width="4" style="background:url({{ asset('/public/frontend/img/shadow-left-center.png') }}) repeat-y 100% 0;">
                                    <p style="margin:0; font-size:1px; line-height:1px;">&nbsp;</p>
                                </td>
                                <td width="4" style="background:url({{ asset('/public/frontend/img/shadow-right-center.png') }}) repeat-y 0 0;">
                                    <p style="margin:0; font-size:1px; line-height:1px;">&nbsp;</p>
                                </td>
                            </tr>

                            <tr>
                                <td width="4" height="4" style="background:url({{ asset('/public/frontend/img/shadow-left-bottom.png') }}) repeat-y 100% 100%;">
                                    <p style="margin:0; font-size:1px; line-height:1px;">&nbsp;</p>
                                </td>
                                <td width="4" height="4" style="background:url({{ asset('/public/frontend/img/shadow-right-bottom.png') }}) repeat-y 0 100%;">
                                    <p style="margin:0; font-size:1px; line-height:1px;">&nbsp;</p>
                                </td>
                            </tr>

                            <tr>
                                <td width="4" height="4" style="background:url({{ asset('/public/frontend/img/shadow-bottom-corner-left.png') }}) no-repeat 100% 0;">
                                    <p style="margin:0; font-size:1px; line-height:1px;">&nbsp;</p>
                                </td>
                                <td width="4" height="4" style="background:url({{ asset('/public/frontend/img/shadow-bottom-left.png') }}) no-repeat 100% 0;">
                                    <p style="margin:0; font-size:1px; line-height:1px;">&nbsp;</p>
                                </td>
                                <td height="4" style="background:url({{ asset('/public/frontend/img/shadow-bottom-center.png') }}) repeat-x 0 0;">
                                    <p style="margin:0; font-size:1px; line-height:1px;">&nbsp;</p>
                                </td>
                                <td width="4" height="4" style="background:url({{ asset('/public/frontend/img/shadow-bottom-right.png') }}) no-repeat 0 0;">
                                    <p style="margin:0; font-size:1px; line-height:1px;">&nbsp;</p>
                                </td>
                                <td width="4" height="4" style="background:url({{ asset('/public/frontend/img/shadow-bottom-corner-right.png') }}) no-repeat 0 0;">
                                    <p style="margin:0; font-size:1px; line-height:1px;">&nbsp;</p>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <!-- end wrapper-->
                    </td>
                </tr>
                </tbody>
            </table>
            <!-- end main block -->
        </td>
    </tr>
    </tbody>
</table>
</body>

</html>