<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=display-width, initial-scale=1.0, maximum-scale=1.0,">
    <title>Юбилейное</title>
    
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0,"> 
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet" type="text/css">
    
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet" type="text/css">
    
    
    <style type="text/css">
    
        html { width: 100%; background-color: #f4f5f8;}
        body {margin:0; padding:0; width:100%; -webkit-text-size-adjust:none; -ms-text-size-adjust:none;}
        img {display:block !important; border:0; -ms-interpolation-mode:bicubic;}
        a {color: #17adbc;text-decoration: none;white-space: nowrap;}
        a:hover {text-decoration: underline;}
        @media only screen and (max-width:800px)
        {
            body {width:auto !important;}
            table[class=display-width], .display-width {width:100% !important;}		
            table[class=display-width-inner] {width:600px !important;}			
        }
        
        @media only screen and (max-width:639px)
        {
            body {width:auto !important;}
            table[class=display-width], .display-width {width:100% !important;}
            table[class=display-width-inner],
            table[class=display-width-child]
            {width:100% !important;}
            table[class=display-width-child] .button-width .display-button {width:auto !important;}
            .hide-height {display:none !important;}
            .hide-border {border:0px !important;}
            .height15 {height:15px !important;}
            .txt-center {text-align:center !important;}
            .price {width:220px !important;}
            table[class=display-width] .button-center {
                text-align:center !important;
                margin:0 auto !important;
                display:block !important;
                width:111px !important;
            }
            .respo-center {width:300px !important;}
            .testimo-center {width:240px !important;}
            .underline-image img{
                margin:0 auto !important;
            }
        }

        @media only screen and (max-width:480px) {
            
            table[class=display-width] table {width:100% !important;}
            table[class=display-width] .button-width .display-button {width:auto !important;}
            table[class=display-width-child] .price {width:220px !important;}
            table[class=display-width-inner] .respo-center {width:300px !important;}
            table[class=display-width-child] .portfolio {width:100% !important;}
            table[class=display-width-child] .port-img {width:240px !important;}
            table[class=display-width-inner] .testimo-center {width:240px !important;}
        }

        @media only screen and (max-width:360px)
        {
            table[class=display-width] table, .display-width table {width:100% !important;}
            table[class=display-width] .button-width .display-button {width:auto !important;}
            table[class=display-width-child] .price {width:220px !important;}
            table[class=display-width-inner] .respo-center {width:273px !important;}
        }
        
        @media only screen and (max-width:333px)
        {
            table[class=display-width-inner] .respo-center {width:100% !important;}
        }
        
    </style>
        <!--[if mso]>
            <style>
                .MsoNormal {font-family: Arial, Helvetica Neue, Helvetica, sans-serif !important;}
                .heading {font-family:  Arial, Helvetica Neue, Helvetica, sans-serif !important;}
            </style>
        <![endif]--> 
</head>
<body>
    <!-- MENU STARTS -->
    {{--<table align="center" bgcolor="#f4f5f8" border="0" cellpadding="0" cellspacing="0" width="100%">--}}
        {{--<tbody><tr>--}}
            {{--<td align="center">--}}
                {{--<table align="center" bgcolor="#f4f5f8" border="0" class="display-width" cellpadding="0" cellspacing="0" width="563">--}}
                    {{--<tbody><tr>--}}
                        {{--<td align="center" class="padding" style="width: 50%;"></td>--}}
                        {{--<td align="center" class="padding" style="padding:30px;">--}}
                            {{--<a href="https://kiwiroom.co.nz" style="color:#666666; margin-left: auto; margin-right: auto; text-decoration:none;">--}}
                                {{--<img src="https://kiwiroom.co.nz/public/frontend/img/dark-logo-for-mail.png" alt="kiwiroom" width="165" height="56" style="border-radius:5px; margin:0; border:0; padding:0; display:block;">--}}
                            {{--</a>--}}
                        {{--</td>--}}
                        {{--<td align="center" class="padding" style="width: 50%;"></td>--}}
                    {{--</tr>--}}
                {{--</tbody></table>--}}
            {{--</td>--}}
        {{--</tr>--}}
    {{--</tbody></table>--}}
    <!-- MENU ENDS -->
    
    <!-- HEADER STARTS -->
    <table align="center" bgcolor="#f4f5f8" border="0" cellpadding="0" cellspacing="0" width="100%">
        <tbody><tr>
            <td align="center">
                <table align="center" border="0" cellpadding="0" cellspacing="0" class="display-width" width="563">
                    <tbody><tr>
                        <td align="center">
                            <div style="margin:auto;">
                                <table align="center" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" class="display-width" width="100%" style="background-color:#ffffff">
                                    <tbody><tr>
                                        <td align="center" class="padding" style="padding:0 30px;">
                                            <table align="center" border="0" class="display-width-inner" cellpadding="0" cellspacing="0" width="600">
                                                <tbody><tr>
                                                    <td height="30"></td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table align="center" border="0" cellspacing="0" cellpadding="0" width="100%">
                                                            <tbody><tr>
                                                                <td align="center" width="81" style="color:#000000; color: #000000; font-family: 'Proxima Nova - Semibold', Helvetica Neue, Arial, Verdana, Trebuchet MS; font-size: 20px; font-weight: 800; line-height: 19px; letter-spacing: 0.4px;
                                                                ">
                                                                    Заявка на обратную связь
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="20"></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" class="heading" style="color:#000000; font-family:'Proxima Nova', Helvetica Neue, Arial, Verdana, Trebuchet MS, sans-serif; width: 493px; height: 77px; color: #000000; font-size: 15px; font-weight: 300; line-height: 21px; letter-spacing: 0.3px;">
                                                                    ФИО: {{ $request->name }}<br>
                                                                    E-mail: {{ $request->email }}<br>
                                                                    Тема: {{ $request->question }}<br>
                                                                    Сообщение: {{ $request->message }}<br>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="30"></td>
                                                            </tr>
                                                        </tbody></table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td height="30"></td>
                                                </tr>
                                            </tbody></table>
                                        </td>
                                    </tr>
                                </tbody></table>
                            </div>
                            <!--[if gte mso 9]> </v:textbox> </v:rect> <![endif]-->	
                        </td>
                    </tr>
                </tbody></table>
            </td>
        </tr>
    </tbody></table>	
    <!-- HEADER ENDS -->

    <!-- FOOTER STARTS -->	
    <!-- FOOTER ENDS -->

</body></html>