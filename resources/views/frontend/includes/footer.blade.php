<footer class="footer">
    <div class="container">
        <ul class="footer__nav">
            <li><a href="tel:8-800-555-8000">8-800-555-8000</a></li>
            <li><a href="mailto:info@jubileynoe.ru">info@jubileynoe.ru</a></li>
        </ul>
        <ul class="footer__nav footer__nav_bottom">
            <li><a target="_blank" href="https://ru.mondelezinternational.com/privacy-policy">Политика конфиденциальности</a></li>
            <li><a target="_blank" href="{{ asset('/public/Правила Акции Пр 45 0401_на сайт.pdf') }}">Правила акции</a></li>
            <li><a target="_blank" href="https://ru.mondelezinternational.com/legal-notices">Пользовательское соглашение</a></li>
        </ul>
        <div class="footer__logo">
            <img src="{{ asset('public/frontend/img/logo-mondelez.png') }}" alt="">
        </div>
        <div class="footer__author">
            <p>ООО «Мон’дэлис Русь».</p>
            <p>Часть группы компаний Mondelēz International. Все права защищены.</p>
        </div>
    </div>
</footer>
<!-- Stock -->
<div class="stock">
    <div class="container stock_container flex-cont justify-c align-c">
        <div class="stock__close js-close-popup"></div>
        <div class="stock__img">
            <img src="{{ asset('public/frontend/img/tea.png') }}" alt="">
        </div>
        <div class="stock__text">
            <p>Акция «Соберите отмеЧайный сервиз!» теперь находится по адресу сервиз.юбилейное.рф</p>
        </div>
        <a href="http://сервиз.юбилейное.рф" class="btn btn_stock">Перейти</a>
    </div>
</div>
<!-- Cookies -->
<div class="cookies">
    <div class="container">
        <div class="cookies__text">
            <p>Мы используем технологию «cookies», чтобы упростить навигацию на нашем сайте. Чтобы принять политику
                “сookies” нажимите кнопку ниже или продолжите навигацию по сайту. Чтобы узнать больше о том, как мы
                используем “сookies”, <a href="#">нажмите здесь</a></p>
        </div>
        <a href="#" class="btn btn_cook">Принять</a>
    </div>
</div>

<!-- Popup -->
<div class="popup entrance">
    <div class="popup__wrap">
        <div class="popup__close js-close-popup"></div>
        <div class="title">
            <span>Войти на сайт</span>
        </div>
        <div class="form form--popupMargin">
            <form class="login">
                {{ csrf_field() }}
                <div class="ajax-validate-error">
                </div>
                <div class="form__card">
                    <div class="form__tips">
                        <span>Адрес электронной почты</span>
                    </div>
                    <div class="form__input">
                        <input type="email" name="email">
                    </div>
                </div>
                <div class="form__card">
                    <div class="form__tips">
                        <span>Пароль</span>
                    </div>
                    <div class="form__input">
                        <input type="password" name="password">
                    </div>
                </div>
                <div class="text text_alignc">
                    <a  class="js-popup-button" data-popupShow="reset-link" >Забыл пароль?</a>
                </div>
                <div class="btn-wrap btn-wrap_minMargin">
                    <button type="submit" class="btn btn_def btn_disable" disabled="disabled">Войти</button>
                </div>
                <div class="form__text">
                    <p>Впервые на сайте?</p>
                    <div class="text">
                        <a href="#" class="js-popup-button" data-popupShow="reg-step-1">Зарегистрируйтесь!</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div> <!--Актиный попап - .js-popup-show-->

<div class="popup reg-step-1">
    <div class="popup__wrap">
        <div class="popup__close js-close-popup"></div>
        <div class="title">
            <span>Регистрация</span>
        </div>
        <div class="form form--popupMargin">
            <div class="form__text form__text_textLeft" style="text-align: center;">
                <p>Срок регистрации кодов и чеков завершен.</p>
            </div>
            {{--<form class="register-step-1">--}}
                {{--{{ csrf_field() }}--}}
                {{--<div class="form__text form__text--minMargin ">--}}
                    {{--<p>Заполни, пожалуйста, все поля формы регистрации</p>--}}
                {{--</div>--}}
                {{--<div class="form__step">--}}
                    {{--<span>Шаг 1 из 2</span>--}}
                {{--</div>--}}
                {{--<div class="ajax-validate-error">--}}
                {{--</div>--}}
                {{--<div class="form__card">--}}
                    {{--<div class="form__tips">--}}
                        {{--<span>Фамилия</span>--}}
                    {{--</div>--}}
                    {{--<div class="form__input">--}}
                        {{--<input type="text" name="surname">--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="form__card">--}}
                    {{--<div class="form__tips">--}}
                        {{--<span>Имя</span>--}}
                    {{--</div>--}}
                    {{--<div class="form__input">--}}
                        {{--<input type="text" name="name">--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="form__card">--}}
                    {{--<div class="form__tips">--}}
                        {{--<span>Отчество</span>--}}
                    {{--</div>--}}
                    {{--<div class="form__input">--}}
                        {{--<input type="text" name="second_name">--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="form__card">--}}
                    {{--<div class="form__tips">--}}
                        {{--<span>Адрес электронной почты</span>--}}
                    {{--</div>--}}
                    {{--<div class="form__input">--}}
                        {{--<input type="email" name="email">--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="btn-wrap btn-wrap_minMargin">--}}
                    {{--<button type="submit" class="btn btn_def btn_disable" disabled="disabled">Дальше</button>--}}
                {{--</div>--}}
            {{--</form>--}}
        </div>
    </div>
</div>

<div class="popup reg-step-2">
    <div class="popup__wrap">
        <div class="popup__close js-close-popup"></div>
        <div class="title">
            <span>Регистрация</span>
        </div>
        <div class="form form--popupMargin">
            <form class="register-step-2">
                {{ csrf_field() }}
                <div class="form__step">
                    <span>Шаг 2 из 2</span>
                </div>
                <div class="ajax-validate-error">
                </div>
                <div class="form__card">
                    <div class="form__tips">
                        <span>Придумайте пароль</span>
                    </div>
                    <div class="form__input">
                        <input type="password" name="password">
                    </div>
                </div>
                <div class="form__card">
                    <div class="form__tips">
                        <span>Повторите пароль</span>
                    </div>
                    <div class="form__input">
                        <input type="password" name="password_confirmation">
                    </div>
                </div>
                <div class="form__card">
                    <label class="checkbox">
                        <input type="checkbox" name="rules" required/>
                        <div class="checkbox__text">Я согласен c <a  target="_blank" href="{{ asset('/public/Правила Акции Пр 45 0401_на сайт.pdf') }}">Правилами акции</a></div>
                    </label>
                    <label class="checkbox">
                        <input type="checkbox" name="personalAgreement" required/>
                        <div class="checkbox__text">Я согласен на обработку моих персональных данных</div>
                    </label>
                </div>
                <div class="captcha">
                    <div class="form__text form__text--noMargin captcha_text">
                        <p>Введите символы с картинки</p>
                    </div>
                    <div class="captcha__wrap flex-cont align-center justify-sb">
                        <div class="captcha__img flex-cont align-c">
                            <span>{!! captcha_img() !!}</span>
                        </div>
                        <div class="captcha__send">
                            <input class="captcha__input" type="text" name="captcha">
                        </div>
                    </div>
                </div>
                <div class="btn-wrap btn-wrap_minMargin">
                    <button type="submit" class="btn btn_def btn_pop btn_disable" disabled="disabled">Зарегистрироваться</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="popup reg-step-3">
    <div class="popup__wrap">
        <div class="popup__close js-close-popup"></div>
        <div class="title">
            <span>Готово</span>
        </div>
        <div class="form form--popupMargin">
            <form class="register-step-3">
                <div class="form__text form__text--minMargin ">
                    <p>Письмо с интсрукцией отправлено Вамна почту</p>
                </div>
                <div class="btn-wrap btn-wrap_minMargin">
                    <button type="button" class="btn btn_def btn_pop js-close-popup">Понятно</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="popup reg-step-3 contact-thank-form">
    <div class="popup__wrap">
        <div class="popup__close js-close-popup"></div>
        <div class="title">
            <span>Спасибо!</span>
        </div>
        <div class="form form--popupMargin">
            <form class="">
                <div class="form__text form__text--minMargin ">
                    <p>Письмо отправлено на почту</p>
                    <p>Скоро с Вами свяжутся, ожидайте.</p>
                </div>
                <div class="btn-wrap btn-wrap_minMargin">
                    <button type="button" class="btn btn_def btn_pop js-close-popup">Спасибо</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="popup reg-step-3  @if(isset($accessToken)) js-popup-show @endif">
    <div class="popup__wrap">
        <div class="popup__close js-close-popup"></div>
        <div class="title">
            <span>Готово</span>
        </div>
        <div class="form form--popupMargin">
            <form class="register-step-3">
                <div class="form__text form__text--minMargin ">
                    <p>Ура! Регистрация прошла успешно!</p>
                </div>
                <div class="btn-wrap btn-wrap_minMargin">
                    <button type="button" class="btn btn_def btn_pop js-popup-button"   @if(isset($accessToken)) data-popupShow="download-check" @else  data-popupShow="entrance"  @endif >Загрузить чек</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="popup reset-link">
    <div class="popup__wrap">
        <div class="popup__close js-close-popup"></div>
        <div class="title">
            <span>Восстановление пароля</span>
        </div>
        <div class="form form--popupMargin">
            <form class="reset-step-1">
                {{ csrf_field() }}
                <div class="form__step">
                    <span>Шаг 1 из 2</span>
                </div>
                <div class="ajax-validate-error">
                </div>
                <div class="form__card">
                    <div class="form__tips">
                        <span>Введите Вашу почту</span>
                    </div>
                    <div class="form__input">
                        <input type="email" name="email">
                    </div>
                </div>
                <div class="btn-wrap btn-wrap_minMargin">
                    <button type="submit" class="btn btn_def btn_pop btn_disable">Отправить инструкцию</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="popup reset-link-send">
    <div class="popup__wrap">
        <div class="popup__close js-close-popup"></div>
        <div class="title">
            <span>Готово</span>
        </div>
        <div class="form form--popupMargin">
            <form class="reset-step-2">
                <div class="form__text form__text--minMargin ">
                    <p>На указанную почту была выслана инструкция.</p>
                </div>
                <div class="btn-wrap btn-wrap_minMargin">
                    <button type="button" class="btn btn_def btn_pop js-close-popup" >Понятно</button>
                </div>
            </form>
        </div>
    </div>
</div>
@if(isset($reset_token))
    <div class="popup reset-update-password js-popup-show">
        <div class="popup__wrap">
            <div class="popup__close js-close-popup"></div>
            <div class="title">
                <span>Восстановление пароля</span>
            </div>
            <div class="form form--popupMargin">
                <form class="reset-step-3">
                    {{ csrf_field() }}
                    <div class="form__step">
                        <span>Шаг 2 из 2</span>
                    </div>
                    <div class="ajax-validate-error">
                    </div>
                    <div class="form__card">
                        <div class="form__tips">
                            <span>Введите Вашу почту</span>
                        </div>
                        <div class="form__input">
                            <input type="hidden" name="token" value="{{ $reset_token }}">
                            <input type="email" name="email">
                        </div>
                    </div>
                    <div class="form__card">
                        <div class="form__tips">
                            <span>Новый пароль</span>
                        </div>
                        <div class="form__input">
                            <input type="password" name="password">
                        </div>
                    </div>
                    <div class="form__card">
                        <div class="form__tips">
                            <span>Повторите новый пароль</span>
                        </div>
                        <div class="form__input">
                            <input type="password" name="password_confirmation">
                        </div>
                    </div>
                    <div class="btn-wrap btn-wrap_minMargin">
                        <button type="submit" class="btn btn_def btn_pop btn_disable">Обновить пароль</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endif
<div class="popup download-check">
    <div class="popup__wrap">
        <div class="popup__close js-close-popup"></div>
        <div class="title">
            <span>Загрузить чек</span>
        </div>
        <div class="form form--popupMargin">
            <form>
                <div class="text-download-wrap">
                    @if(date('Y') == '2019')
                        <div class="form__text form__text_textLeft" style="text-align: center;">
                            <p>Срок регистрации кодов и чеков завершен.</p>
                        </div>
                    @else
                        <div class="form__text form__text_textLeft">
                            <p>Нажмите на кнопку «Загрузить чек» и сделайте фото чека.</p>
                        </div>
                        <div class="form__text form__text_textLeft">
                            <p>Постарайтесь, чтобы фото получилось четким, а текст читаемым</p>
                        </div>
                        <div class="form__text form__text_textLeft">
                            <p>Если чек длинный, сделайте и загрузите несколько фото.</p>
                        </div>
                        <div class="form__text form__text_textLeft">
                            <p>Если на чеке есть QR-код, сделайте его фото отдельным кадром.</p>
                        </div>
                        <div class="form__text form__text_textLeft">
                            <p>За один раз можно зарегистрировать только один чек.</p>
                        </div>
                        <div class="form__text form__text_textLeft">
                            <p>Если фото будет плохого качества, мы не сможем принять ваш чек, так что постарайтесь :-)</p>
                        </div>
                    @endif
                </div>
                @if(date('Y') != '2019')
                    <div class="btn-wrap btn-wrap_minMargin">
                        <button type="button" class="btn btn_def btn_down js-popup-button"  @if(Auth::guard('customer')->check()) data-popupShow="download-check-photo" @else data-popupShow="entrance"  @endif  >Загрузить чек</button>
                    </div>
                @endif

            </form>
        </div>
    </div>
</div>

<div id="download-photo" class="popup download-check-photo">
    <div class="popup__wrap">
        <div class="popup__close js-close-popup"></div>
        <div class="title">
            <span>Ваш чек</span>
        </div>
        <div class="form form--popupMargin">
            <form class="upload-check">
                {{ csrf_field() }}
                <div class="ajax-validate-error">
                </div>
                <div id="download-wrap" class="photo-download-wrap"></div>
                <label for="file-photo-check" id="photo-download" class="file-photo">
                    <input type="file" class="file-photo-check" name="image[]"  id="file-photo-check" data-url="/customer/upload" accept="image/*,image/jpeg">
                    <div class="btn-wrap">
                        <p class="btn btn_def btn_file">Добавить фото</p>
                    </div>
                </label>
                <div class="btn-wrap btn-wrap_minMargin">
                    <input type="hidden" name="photo_ids" id="photo_ids" value="" />
                    <button type="submit" class="btn btn_def">Отправить на модерацию</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="popup download-check-end">
    <div class="popup__wrap">
        <div class="popup__close js-close-popup"></div>
        <div class="title">
            <span>Большое спасибо!</span>
        </div>
        <div class="form form--popupMargin">
            <form action="/customer/profile">
                <div class="form__text form__text--minMargin ">
                    <p>Мы получили чек и проверяем его.</p>
                </div>
                <div class="form__text form__text--minMargin ">
                    <p>Подробнее о статусе проверки можно узнать в Личном кабинете.</p>
                </div>
                <div class="btn-wrap btn-wrap_minMargin">
                    <button type="submit" class="btn btn_def btn_pop">Понятно</button>
                </div>
            </form>
        </div>
    </div>
</div>
@if(Auth::guard('customer')->check())
    <div class="popup send-gift">
        <div class="popup__wrap">
            <div class="popup__close js-close-popup"></div>
            <div class="title">
                <span>Отправка подарка</span>
            </div>
            <div class="form form--popupMargin">
                <form class="js-form-address get-prize">
                    {{ csrf_field() }}
                    <input hidden name="check_id" id="check_id">
                    <div class="form__text form__text--minMargin ">
                        <p>Почта России с радостью доставит ваш подарок. Просто заполните форму для  его получения.</p>
                    </div>
                    <div class="form__card">
                        <div class="form__tips">
                            <span>Фамилия Имя Отчество</span>
                        </div>
                        <div class="form__input">
                            <div class="add-name" readonly>
                                {{ Auth::guard('customer')->user()->surname }} {{ Auth::guard('customer')->user()->name }} {{ Auth::guard('customer')->user()->second_name }}
                            </div>
                        </div>
                    </div>
                    <div class="form__card">
                        <div class="form__tips">
                            <span>Адрес электронной почты</span>
                        </div>
                        <div class="form__input">
                            <div class="add-email" readonly>
                                {{ Auth::guard('customer')->user()->email }}
                            </div>
                        </div>
                    </div>
                    <div class="form__card prize-size">
                        <div class="form__tips">
                            <span>Размерный ряд</span>
                        </div>
                        <div class="form__input">
                            <select name="size">
                                <option>M</option>
                                <option>S</option>
                            </select>
                        </div>
                    </div>
                    <div class="form__card">
                        <div class="form__tips">
                            <span>Индекс</span>
                        </div>
                        <div class="form__input">
                            <input type="text" name="reg_index">
                        </div>
                    </div>
                    <div class="form__card">
                        <div class="form__tips">
                            <span>Регион</span>
                        </div>
                        <div class="form__input">
                            <input type="text" name="region">
                        </div>
                    </div>
                    <div class="form__card">
                        <div class="form__tips">
                            <span>Город</span>
                        </div>
                        <div class="form__input">
                            <input type="text" name="city">
                        </div>
                    </div>
                    <div class="form__card">
                        <div class="form__tips">
                            <span>Улица</span>
                        </div>
                        <div class="form__input">
                            <input type="text" name="street">
                        </div>
                    </div>
                    <div class="form__card form__card_inline">
                        <div class="form__tips">
                            <span>Дом</span>
                        </div>
                        <div class="form__input">
                            <input type="text" name="house">
                        </div>
                    </div>
                    <div class="form__card form__card_inline form__card_mleft">
                        <div class="form__tips">
                            <span>Квартира</span>
                        </div>
                        <div class="form__input">
                            <input type="text" name="flat">
                        </div>
                    </div>
                    <div class="btn-wrap btn-wrap_minMargin">
                        <button type="submit" class="btn btn_def btn_disable" disabled="disabled">Подтвердить</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endif
<div class="popup send-prize">
    <div class="popup__wrap">
        <div class="popup__close js-close-popup"></div>
        <div class="title">
            <span>Отправка подарка</span>
        </div>
        <div class="form form--popupMargin">
            <form action="/customer/profile">
                <div class="form__text form__text--minMargin ">
                    <p>Большое спасибо! Подробнее о статусе доставки можно узнать</p>
                </div>
                <div class="form__text form__text--minMargin ">
                    <p>в Личном кабинете.</p>
                </div>
                <div class="btn-wrap btn-wrap_minMargin">
                    <button type="submit" class="btn btn_def btn_pop">Понятно</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="popup address">
    <div class="popup__wrap">
        <div class="popup__close js-close-popup"></div>
        <div class="title">
            <span>Отправка подарка</span>
        </div>
        <div class="form form--popupMargin">
            <form>
                <div class="form__text form__text--minMargin ">
                    <p>Все почти упаковано и скоро отправится по адресу:</p>
                </div>
                <div id="text-address-wrap" class="text-address-wrap">
                    <p></p>
                    <p></p>
                </div>
                <div class="form__text form__text--minMargin ">
                    <p>Получив свой подарок, вы cможете сказать «Спасибо!» бабушке в нашей </p>
                    <p>Галерее благодарностей</p>
                </div>
                <div class="btn-wrap btn-wrap_minMargin">
                    <button type="button" class="btn btn_def btn_pop js-close-popup">Понятно</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="popup yes-reg-card ">
    <div class="popup__wrap popup__wrap_big">
        <div class="popup__close js-close-popup"></div>
        <div class="title">
            <span>Код принят</span>
        </div>
        <div class="form form--popupMargin">
            <form>
                <div class="form__text form__text--minMargin">
                    <p>Спасибо! Мы приняли ваш код. Теперь вы можете отправить открытку с теплыми пожеланиями.</p>
                </div>
                <div class="btn-wrap btn-wrap_bigMargin">
                    <button type="button" class="btn btn_def btn_pop js-popup-button" data-popupShow="send-card-slider" >Отправить открытку</button>
                </div>
            </form>
        </div>
    </div>
</div>
@if(Auth::guard('customer')->check())
    <div class="popup send-card">
        <div class="popup__wrap popup__wrap_big">
            <div class="popup__close js-close-popup"></div>
            <div class="title">
                <span class="code-status">Отправить открытку</span>
            </div>
            <div class="form form--popupMargin">
                <form class="send-code">
                    {{ csrf_field() }}
                    @if(date('Y') == '2019')
                        <div class="form__text form__text_textLeft" style="text-align: center;">
                            <p>Срок регистрации кодов и чеков завершен.</p>
                        </div>
                    @else
                        @if(Auth::guard('customer')->user()->new_codes != '[]')
                            <div class="form__text form__text--minMargin">
                                <p>Вы можете отправить <span class="text-cur">{{ count(Auth::guard('customer')->user()->new_codes) }}</span> открытки.</p>
                            </div>
                            <div class="btn-wrap btn-wrap_bg">
                                <button type="button" class="btn btn_def btn_pop js-popup-button" data-popupShow="send-card-slider">Выбрать открытку</button>
                            </div>
                        @endif
                        <div class="form__text form__text--minMargin">
                            <p  class="code-status-text">Регистрируйте коды с пачек печенья Юбилейное «Зимнее ассорти», чтобы отправлять друзьям и родным открытки с теплыми пожеланиями.</p>
                        </div>
                        <div class="form__card form__card_Mbig">
                            <div class="form__input">
                                <input class="void-code" type="text" placeholder="введите код" name="code_name">
                            </div>
                        </div>
                        <div class="btn-wrap btn-wrap_minMargin">
                            <button type="submit" class="btn btn_def btn_pop" >Зарегистрировать код</button>
                        </div>
                        <a id="where-checks" href="#" class="where-checks">Где найти код?</a>
                        <div class="where-checks-wrap">
                            <div class="form__text form__text_little">
                                <p>Код можно найти на внутренней стороне пачек</p>
                                <p>печенья Юбилейное «Зимнее ассорти»</p>
                            </div>
                            <div class="img-code">
                                <img src="{{ asset('/public/frontend/img/code-in-wrapper.png') }}" alt="">
                            </div>
                        </div>
                    @endif
                </form>
            </div>
        </div>
    </div>
@endif
<div class="popup send-card-slider">
    <div class="popup__wrap popup__wrap_big">
        <div class="popup__close js-close-popup"></div>
        <div class="title">
            <span>Выберите открытку</span>
        </div>
        <div class="form form--popupMargin">
            <form>
                <div id="choose-slider" class="swiper-container slide-choose">
                    <div class="swiper-wrapper slide-choose__wrap">
                        <div class="swiper-slide slide-choose__item">
                            <div class="img-card">
                                <img src="{{ asset('/public/frontend/img/открытка 1fin.jpg') }}" alt="">
                            </div>
                        </div>
                        <div class="swiper-slide slide-choose__item">
                            <div class="img-card">
                                <img src="{{ asset('/public/frontend/img/открытка 2fin.jpg') }}" alt="">
                            </div>
                        </div>
                        <div class="swiper-slide slide-choose__item">
                            <div class="img-card">
                                <img src="{{ asset('/public/frontend/img/открытка 3fin.jpg') }}" alt="">
                            </div>
                        </div>
                        <div class="swiper-slide slide-choose__item">
                            <div class="img-card">
                                <img src="{{ asset('/public/frontend/img/открытка 4fin.jpg') }}" alt="">
                            </div>
                        </div>
                        <div class="swiper-slide slide-choose__item">
                            <div class="img-card">
                                <img src="{{ asset('/public/frontend/img/открытка 5fin.jpg') }}" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="choose-btn swiper-button-next"></div>
                    <div class="choose-btn swiper-button-prev"></div>
                    <div class="swiper-pagination"></div>
                </div>
                <div class="btn-wrap btn-wrap_minMargin">
                    <button type="button" id="choose-btn" class="btn btn_def btn_pop js-popup-button" data-popupShow="send-card-address">Выбрать</button>
                </div>
            </form>
        </div>
    </div>
</div>

@if(Auth::guard('customer')->check())
    <div class="popup send-card-address">
        <div class="popup__wrap">
            <div class="popup__close js-close-popup"></div>
            <div class="title">
                <span>Заполните адрес</span>
            </div>
            <div class="form form--popupMargin">
                <form class="js-form-address get-card">
                    {{ csrf_field() }}
                    <input hidden name="code_name_hide" id="code_name">
                    <input hidden name="card_number" id="select-slider">
                    <div class="form__text form__text--minMargin ">
                        <p>Наш почтальон с радостью доставит вашу открытку.</p>
                        <p>Просто заполните форму для отправки.</p>
                    </div>
                    <div class="form__card">
                        <div class="form__tips">
                            <span>Кому доставляем</span>
                        </div>
                        <div class="form__input">
                            <input type="text" name="whom">
                        </div>
                    </div>
                    <div class="form__card">
                        <div class="form__tips">
                            <span>От кого</span>
                        </div>
                        <div class="form__input">
                            <input type="text" readonly value="{{ Auth::guard('customer')->user()->surname }} {{ Auth::guard('customer')->user()->name }} {{ Auth::guard('customer')->user()->second_name }}" name="from_whom">
                        </div>
                    </div>
                    <div class="form__card">
                        <div class="form__tips">
                            <span>Откуда отправляем</span>
                        </div>
                        <div class="form__input">
                            <input type="text" value="" name="from_where">
                        </div>
                    </div>
                    <div class="form__card">
                        <div class="form__tips">
                            <span>Индекс (куда доставляем)</span>
                        </div>
                        <div class="form__input">
                            <input type="text" name="reg_index">
                        </div>
                    </div>
                    <div class="form__card">
                        <div class="form__tips">
                            <span>Регион  (куда доставляем)</span>
                        </div>
                        <div class="form__input">
                            <input type="text" name="region">
                        </div>
                    </div>
                    <div class="form__card">
                        <div class="form__tips">
                            <span>Город (куда доставляем)</span>
                        </div>
                        <div class="form__input">
                            <input type="text" name="city">
                        </div>
                    </div>
                    <div class="form__card">
                        <div class="form__tips">
                            <span>Улица (куда доставляем)</span>
                        </div>
                        <div class="form__input">
                            <input type="text" name="street">
                        </div>
                    </div>
                    <div class="form__card form__card_inline">
                        <div class="form__tips">
                            <span>Дом (куда доставляем)</span>
                        </div>
                        <div class="form__input">
                            <input type="text" name="house">
                        </div>
                    </div>
                    <div class="form__card form__card_inline form__card_mleft">
                        <div class="form__tips">
                            <span>Квартира  (куда доставляем)</span>
                        </div>
                        <div class="form__input">
                            <input type="text" name="flat">
                        </div>
                    </div>
                    <div class="btn-wrap btn-wrap_minMargin">
                        <button type="submit" class="btn btn_def btn_disable" disabled="disabled">Подтвердить</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endif
<div class="popup yes-send-card-slider">
    <div class="popup__wrap popup__wrap_big">
        <div class="popup__close js-close-popup"></div>
        <div class="title">
            <span>Ура!</span>
        </div>
        <div class="form form--popupMargin">
            <form action="/customer/profile">
                <a href="#" class="img-card">
                    <img id="choose-card" src="" alt="">
                </a>
                <div class="form__text form__text--minMargin">
                    <p>Открытка отправится совсем скоро!</p>
                </div>
                <div class="btn-wrap btn-wrap_bigMargin">
                    <button type="button" class="btn btn_def btn_pop js-close-popup">Хорошо</button>
                </div>
            </form>
        </div>
    </div>
</div>
{{--<div class="popup address-card">--}}
{{--<div class="popup__wrap">--}}
{{--<div class="popup__close js-close-popup"></div>--}}
{{--<div class="title">--}}
{{--<span>Отправка подарка</span>--}}
{{--</div>--}}
{{--<div class="form form--popupMargin">--}}
{{--<form>--}}
{{--<div class="form__text form__text--minMargin ">--}}
{{--<p>Все почти упаковано и скоро отправится по адресу:</p>--}}
{{--</div>--}}
{{--<div id="text-address-card-wrap" class="text-address-wrap">--}}
{{--<p></p>--}}
{{--<p></p>--}}
{{--</div>--}}
{{--<div class="btn-wrap btn-wrap_minMargin">--}}
{{--<button type="button" class="btn btn_def btn_pop js-close-popup">Понятно</button>--}}
{{--</div>--}}
{{--</form>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
<div id="gallery-card-slider" class="popup gallery-card-slider">
    <div class="popup__wrap popup__wrap_big">
        <div class="popup__close js-close-popup"></div>
        <div class="title">
            <span></span>
            <p></p>
        </div>
        <div class="form form--popupMargin">
            <div class="swiper-container slide-card">
                <div class="swiper-wrapper slide-card__wrap">
                    <div class="swiper-slide slide-card__item slide-card__item_img">
                        <div class="img-card">
                            <img src="" alt="">
                        </div>
                    </div>
                    <div class="swiper-slide slide-card__item slide-card__item_video">
                        <div class="img-card">
                            <video src="" controls="controls"></video>
                        </div>
                    </div>
                </div>
                <div class="card-btn swiper-button-next"></div>
                <div class="card-btn swiper-button-prev"></div>
            </div>
            <div class="form__text form__text--minMargin ">
                <p></p>
            </div>
        </div>
    </div>
</div>
<div id="gallery-card-add" class="popup gallery-card-add">
    <div class="popup__wrap popup__wrap_big">
        <div class="popup__close js-close-popup"></div>
        <div class="title">
            <span>Отправить благодарность</span>
        </div>
        <div class="form__text form__text--minMargin ">
            <p>Теплые слова важны для всех, но для бабушек — в особенности!</p>
        </div>
        <div class="form form--popupMargin">
            <form class="send-grandmother-thank" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input class="name-grandmother" name="grand_name" hidden >
                <div class="form__text form__text--minMargin ">
                    <p>Здесь можно оставить свое фото или видео со словами благодарности для бабушки, связавшей вам подарок.</p>
                </div>
                <div class="form__card">
                    <div class="form__tips">
                        <span>Ваше имя</span>
                    </div>
                    <div class="form__input">
                        <input type="text" name="name">
                    </div>
                </div>
                <div class="form__card">
                    <div class="form__tips">
                        <span>Текст (не более 140 символов)</span>
                    </div>
                    <div class="form__input">
                        <textarea name="message"></textarea>
                    </div>
                </div>
                <div class="download-file-congratulate flex-cont justify-sb ">
                    <label for="file-card-photo" id="card-download" class="download-congratulate">
                        <input type="file" name="image" id="file-card-photo" class="file-card-photo" accept="image/*,image/jpeg">
                        <div class="btn-send">
                            <span class="congratulate-icon icon-photo-send"></span>
                        </div>
                        <p>Добавить фото</p>
                    </label>
                    <label for="file-card-video" id="video-download" class="download-congratulate">
                        <input type="file" name="video" id="file-card-video" class="file-card-video" accept="video/mp4,video/x-m4v,video/*">
                        <div class="btn-send">
                            <span class="congratulate-icon icon-video-send"></span>
                        </div>
                        <p>Добавить видео</p>
                    </label>
                </div>
                <div class="btn-wrap btn-wrap_bigMargin">
                    <button type="submit" class="btn btn_def btn_pop btn_disable js-close-popup" disabled="disabled">Спасибо!</button>
                </div>
            </form>
        </div>
        <div class="social">
            <p class="social__text">Поделиться радостью с друзьями</p>
            <div class="social__icon addthis_inline_share_toolbox"></div>
        </div>
    </div>
</div>

@if(isset($show_winner))
    <div class="popup check-successful  js-popup-show">
        <div class="popup__wrap">
            <div class="popup__close js-close-popup"></div>
            <div class="title">
                <span>Поздравляем!</span>
            </div>
            <div class="popup__text popup__text--marginTop popup__text_marginminus">
            <span> Вас ждет подарок от «Юбилейного»! Просто загляните в
				<a href="/customer/profile">Личный кабинет.</a>
			</span>
            </div>
            <div class="btn-wrap btn-wrap_minMargin">
                <button type="submit" class="btn btn_def js-close-popup">Спасибо!</button>
            </div>
            <div class="social social_blue">
                <p class="social__text">Поделиться радостью с друзьями</p>
                <div class="social__icon addthis_inline_share_toolbox"></div>
            </div>
        </div>
    </div>
@endif

<!-- Show preloader > .preloader_active -->
<div class="preloader">
    <img src="{{ asset('public/frontend/img/preloader.gif') }}" alt="">
</div>
<script type="text/javascript" src="https://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5a8d3889047b6c4a" ></script>

<script type="text/javascript" src="https://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5a8d3889047b6c4a"></script>
</body>

</html>