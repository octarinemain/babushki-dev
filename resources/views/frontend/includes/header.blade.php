 <!doctype html>
<html lang="ru">

<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Юбилейное</title>
    <!--Main Style-->
    <link rel="shortcut icon" type="image/png" href="{{ asset('public/frontend/img/favicons.ico') }}"/>
	<link rel="stylesheet" href="{{ asset('public/frontend/css/library.min.css') }}">
	<link rel="stylesheet" href="{{ asset('public/frontend/css/jquery.kladr.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/frontend/css/style.css') }}">

    <!--Scripts-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js" defer></script>
	<script src="{{ asset('public/frontend/js/jquery.kladr.min.js') }}" defer></script>
    <script src="{{ asset('public/frontend/js/library.min.js') }}" defer></script>
    <script src="{{ asset('public/frontend/js/forms.js') }}" defer></script>
	<script src="{{ asset('public/frontend/js/app.js') }}" defer></script>
	<script src="{{ asset('public/frontend/js/jquery.ui.widget.js') }}" defer></script>
	<script src="{{ asset('public/frontend/js/jquery.iframe-transport.js') }}" defer></script>
	<script src="{{ asset('public/frontend/js/jquery.fileupload.js') }}" defer></script>

</head>

<body>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-126763492-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-126763492-1');
</script>
<aside class="header">
    <div class="container header_container">
        <div class="header__wrap">
            <a href="/" class="logo">
                <span class="icon-logo"></span>
            </a>
            <a class="header__burger" href="#">
                <span class="icon-menu"></span>
                <span class="icon-menu-close"></span>
            </a>
            <nav class="nav">
                <a class="nav__link {{ request()->is('/') ? 'active' : '' }}" href="/">Главная</a> <!-- Active link - .active -->
                <a class="nav__link {{ request()->is('grandmothers') ? 'active' : '' }}" href="/grandmothers">Наши бабушки</a>
                <a class="nav__link {{ request()->is('presents') ? 'active' : '' }}" href="/presents">Подарки от бабушек</a>
                <a class="nav__link {{ request()->is('products') ? 'active' : '' }}" href="/products">Продукт</a>
                <a class="nav__link {{ request()->is('winners') ? 'active' : '' }}" href="/winners">Победители</a>
                <a class="nav__link {{ request()->is('contact') ? 'active' : '' }}" href="/contact">Обратная связь</a>
                <div class="personal-area  @if(Auth::guard('customer')->check()) login  @endif">
                    <!-- Для залогиненого юзера добавить класс - .login -->
                    <div class="personal-area__no-entry js-popup-button" data-popupShow="entrance">
                        <div class="personal-area__photo">
                            <span class="icon-LK-icon personal-area__photo_icon"></span>
                        </div>
                        <div class="personal-area__text personal-area__text_hover">
                            <p>Личный кабинет</p>
                        </div>
                    </div>
                    <div class="personal-area__entry">
                        <div class="personal-area__info flex-cont">
                            <div class="personal-area__photo">
                                <span class="icon-LK-icon personal-area__photo_icon"></span>
                            </div>
                            <div class="personal-area__text personal-area__text_hover">
                                @if(Auth::guard('customer')->check())
                                    <p>
                                        <a href="/customer/profile">
                                            {{ Auth::guard('customer')->user()->surname }}
                                            {{ Auth::guard('customer')->user()->name }}
                                            {{ Auth::guard('customer')->user()->second_name }}
                                        </a>
                                    </p>
                                @endif
                            </div>
                        </div>
                        <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="personal-area__exit-btn">Выйти</a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST"
                              style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </div>
                </div>
                <div class="text-card-wrap">
                    <p class="text-card-wrap__text">
                        Поздравьте родных и любимых
                    </p>
                    <a href="#" class="text-card-wrap__send-card js-popup-button" @if(Auth::guard('customer')->check()) data-popupShow="send-card" @else data-popupShow="entrance" @endif></a>
                </div>
            </nav>
            <div class="personal-area-photo @if(Auth::guard('customer')->check()) login-photo  @endif"> <!-- Для залогиненого юзера добавить класс - .login-photo -->
                <div class="personal-area-photo__wrap">
                    <span class="icon-LK-icon personal-area-photo_icon login-photo"></span>
                </div>
            </div>
        </div>
    </div>
</aside>
@yield('content')
@extends('frontend.includes.footer')