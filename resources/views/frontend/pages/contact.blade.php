@extends('frontend.includes.header')


@section('content')
    <div class="height-helper height-helper_new-bg">
        <div class="content-wrap">
            <!--Main-->
            <main class="main main_new-bg">
                <section class="feedback">
                    <div class="container container_little">
                        <div class="title title_big">
                            <span>Обратная связь</span>
                        </div>
                        <div class="text text_big">
                            <p>Если у вас появились вопросы, напишите нам,</p>
                            <p>мы обязательно ответим!</p>
                        </div>
                        <div class="substrate">
                                <form class="form-wrap contact-form" >
                                    {{ csrf_field() }}
                                <div class="form flex-cont justify-sb">
                                    <div class="form__globalCard">
                                        <div class="ajax-validate-error">
                                        </div>
                                        <div class="form__card">
                                            <div class="form__tips">
                                                <span>Ваше имя</span>
                                            </div>
                                            <div class="form__input">
                                                <input id="contact-name" type="text" name="name">
                                            </div>
                                        </div>
                                        <div class="form__card">
                                            <div class="form__tips">
                                                <span>Электроннная почта</span>
                                            </div>
                                            <div class="form__input">
                                                <input id="contact-email" type="email" name="email">
                                            </div>
                                        </div>
                                        <div class="form__card">
                                            <div class="form__tips">
                                                <span>Тема обращения</span>
                                            </div>
                                            <div class="form__input">
                                                <select name="question">
                                                    <option>Выберите из списка</option>
                                                    <option>Вопрос по работе сайта</option>
                                                    <option>Вопрос по правилам акции</option>
                                                    <option>Вопрос по продукту</option>
                                                    <option>Вопрос по призам</option>
                                                    <option>Другое</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form__globalCard">
                                        <div class="form__card">
                                            <div class="form__tips">
                                                <span>Текст</span>
                                            </div>
                                            <div class="form__input">
                                                <textarea id="contact-message" name="message" required></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="captcha captcha_feedback">
                                    <div class="form__text form__text--noMargin captcha_text">
                                        <p>Введи символы с картинки:</p>
                                    </div>
                                    <div class="captcha__wrap flex-cont align-center justify-c">
                                        <div class="captcha__img flex-cont align-c">
                                            <span>{!! captcha_img('flat') !!}</span>
                                        </div>
                                        <div class="captcha__send">
                                            <input class="captcha__input" type="text" name="captcha">
                                        </div>
                                    </div>
                                </div>
                                <div class="btn-wrap btn-wrap_bigMargin">
                                    <button type="submit" class="btn btn_def ">Отправить сообщение</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </section>
            </main>
        </div>
    </div>

@endsection

