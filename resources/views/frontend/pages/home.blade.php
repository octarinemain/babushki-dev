@extends('frontend.includes.header')

@section('content')
    <div class="height-helper">
        <div class="content-wrap">
            <!--Main-->
            <main class="main">
                <section class="conditions">
                    <div class="container conditions_container">
                        <div class="title title_big title_white">
                            <span>Теплые подарки от бабушек</span>
                        </div>
                        <div class="text text_big">
                            <p>Этой зимой бабушки связали для вас в подарок теплые и уютные вещи,</p>
                            <p>чтобы поделиться теплом своего сердца и вновь почувствовать себя</p>
							<p>важными и нужными. Выигрывайте и носите с удовольствием!</p>
                            <div class="btn-wrap">
                                <a  href="/grandmothers" class="btn btn_min">
                                    Подробнее
                                </a>
                            </div>
                        </div>
                        <div class="conditions__info">
                            <div class="swiper-container conditions__content">
                                <div class="swiper-wrapper conditions__step flex-cont justify-sa">
                                    <div class="swiper-slide">
                                        <div class="condition-wrap">
                                            <div class="condition-swipper">
                                                <p class="condition-text">Покупайте</p>
                                                <p class="contition-text-min">печенье «Юбилейное»</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="condition-wrap">
                                            <div class="condition-swipper">
                                                <p class="condition-text">Загружайте</p>
                                                <p class="contition-text-min">чеки от покупок</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="condition-wrap">
                                            <div class="condition-swipper">
                                                <p class="condition-text">Выигрывайте</p>
                                                <p class="contition-text-min">теплые подарки от бабушек</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="condition-btn swiper-button-next"></div>
                                <div class="condition-btn swiper-button-prev"></div>
                            </div>
                            <div class="btn-wrap">
                                <a href="#" class="btn btn_big btn_abs js-popup-button" data-popupShow="download-check">
                                    <span>Загрузить чек</span>
                                </a>
                            </div><!-- Если нужно сделать кнопку disable, добавляем к btn, класс "btn_disable" -->
                            <div class="text-stock">
                                <p>Сроки проведения акции</p>
                                <p>с 1 октября по 31 декабря 2018</p>
                            </div>
                        </div>
                    </div>
                </section>
            </main>
        </div>
        <div class="text-card-wrap">
            <p class="text-card-wrap__text">
                Поздравьте родных и любимых
            </p>
            <a href="#" class="text-card-wrap__send-card js-popup-button" @if(Auth::guard('customer')->check()) data-popupShow="send-card" @else data-popupShow="entrance" @endif></a>
        </div>
        <div class="popup download-reg @if(!isset($show_winner) && !isset($reset_token)) js-popup-show @endif">
            <div class="popup__wrap popup__wrap_very-big">
                <div class="popup__close js-close-popup"></div>
                <div class="sides-wrap flex-cont">
                    <div class="side">
                        <div class="title">
                            <span>Выигрывайте теплые подарки от бабушек</span>
                        </div>
                        <div class="side__img">
                            <img src="{{ asset('/public/frontend/img/biscuit-white.png') }}" alt="">
                        </div>
                        <div class="form__text form__text--minMargin ">
                            <p>Загружайте чеки от покупок печенья «Юбилейное»</p>
                        </div>
                        <div class="btn-wrap btn-wrap_bigMargin">
                            <a href="#" class="btn btn_def js-popup-button" data-popupShow="download-check">
                                <span>Загрузить чек</span>
                            </a>
                        </div>
                    </div>
                    <div class="side side_other">
                        <div class="title">
                            <span>Отправляйте открытки с теплыми пожеланиями</span>
                        </div>
                        <div class="side__img">
                            <img src="{{ asset('/public/frontend/img/pac-biscuit.png') }}" alt="">
                        </div>
                        <div class="form__text form__text--minMargin ">
                            <p>Регистрируйте коды с пачек «Зимнее ассорти»</p>
                        </div>
                        <div class="btn-wrap btn-wrap_bigMargin">
                            <a href="#" class="btn btn_def js-popup-button" @if(Auth::guard('customer')->check()) data-popupShow="send-card" @else data-popupShow="entrance" @endif>
                                <span>Зарегистрировать код</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

