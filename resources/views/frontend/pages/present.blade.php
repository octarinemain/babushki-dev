@extends('frontend.includes.header')

@section('content')
    <div class="height-helper height-helper_new-bg">
        <div class="content-wrap">
            <!--Main-->
            <main class="main main_new-bg">
                <section class="gift-colections">
                    <div class="container container_gift">
                        <div class="title title_big">
                            <span>Теплые и модные</span>
                        </div>
                        <div class="text text_big text_gift">
                            <p>В розыгрыше свитеров участвуют только чеки из сети магазинов «Пятерочка», «Перекресток» и «Карусель». В розыгрыше шапок, варежек и носочков участвуют все зарегистрированные чеки.</p>
						</div>
						<div class="wrapper-for-newy-wrap">
							<div class="swiper-container newy-wrap">
								<div class="swiper-wrapper">
									<div class="swiper-slide newy-wrap__slide">
										<img src="{{ asset('/public/frontend/img/01.jpg') }}" alt="">
									</div>
									<div class="swiper-slide newy-wrap__slide">
										<img src="{{ asset('/public/frontend/img/02.jpg') }}" alt="">
									</div>
									<div class="swiper-slide newy-wrap__slide">
										<img src="{{ asset('/public/frontend/img/03.jpg') }}" alt="">
									</div>
									<div class="swiper-slide newy-wrap__slide">
										<img src="{{ asset('/public/frontend/img/04.jpg') }}" alt="">
									</div>
									<div class="swiper-slide newy-wrap__slide">
										<img src="{{ asset('/public/frontend/img/05.jpg') }}" alt="">
									</div>
									<div class="swiper-slide newy-wrap__slide">
										<img src="{{ asset('/public/frontend/img/06.jpg') }}" alt="">
									</div>
									<div class="swiper-slide newy-wrap__slide">
										<img src="{{ asset('/public/frontend/img/07.jpg') }}" alt="">
									</div>
									<div class="swiper-slide newy-wrap__slide">
										<img src="{{ asset('/public/frontend/img/08.jpg') }}" alt="">
									</div>
									<div class="swiper-slide newy-wrap__slide">
										<img src="{{ asset('/public/frontend/img/09.jpg') }}" alt="">
									</div>
									<div class="swiper-slide newy-wrap__slide">
										<img src="{{ asset('/public/frontend/img/10.jpg') }}" alt="">
									</div>
									<div class="swiper-slide newy-wrap__slide">
										<img src="{{ asset('/public/frontend/img/11.jpg') }}" alt="">
									</div>
									<div class="swiper-slide newy-wrap__slide">
										<img src="{{ asset('/public/frontend/img/12.jpg') }}" alt="">
									</div>
									<div class="swiper-slide newy-wrap__slide">
										<img src="{{ asset('/public/frontend/img/13.jpg') }}" alt="">
									</div>
									<div class="swiper-slide newy-wrap__slide">
										<img src="{{ asset('/public/frontend/img/14.jpg') }}" alt="">
									</div>
									<div class="swiper-slide newy-wrap__slide">
										<img src="{{ asset('/public/frontend/img/15.jpg') }}" alt="">
									</div>
								</div>
							</div>
							<div class="gift-btn swiper-button-prev"></div>
							<div class="gift-btn swiper-button-next"></div>
						</div>
                        <div class="tabs-colections tabs">
                            <ul class="colections-wrap flex-cont justify-c tabs__caption">
                                <li class="colections-wrap__items active">Коллекция октября</li>
                                <li class="colections-wrap__items">Коллекция ноября</li>
                                <li class="colections-wrap__items">Коллекция декабря</li>
                            </ul>
                            <div class="colection-block-wrap tabs__content active">
                                <div class="clolection-clothes-wrap flex-cont justify-sb">
                                    <div class="clothes-wrap">
                                        <div class="clothes-wrap__img">
                                            <img src="{{ asset('/public/frontend/img/pullover.png') }}" alt="">
                                        </div>
                                        <div class="clothes-wrap__text">
                                            Уютный свитер
                                        </div>
                                    </div>
                                    <div class="clothes-wrap">
                                        <div class="clothes-wrap__img">
                                            <img src="{{ asset('/public/frontend/img/socks-1.png') }}" alt="">
                                        </div>
                                        <div class="clothes-wrap__text">
                                            Теплые носочки
                                        </div>
                                    </div>
                                    <div class="clothes-wrap">
                                        <div class="clothes-wrap__img">
                                            <img src="{{ asset('/public/frontend/img/cap.png') }}" alt="">
                                        </div>
                                        <div class="clothes-wrap__text">
                                            Совсем не колючая шапка
                                        </div>
                                    </div>
                                    <div class="clothes-wrap">
                                        <div class="clothes-wrap__img">
                                            <img src="{{ asset('/public/frontend/img/gloves.png') }}" alt="">
                                        </div>
                                        <div class="clothes-wrap__text">
                                            Яркие варежки
                                        </div>
                                    </div>
                                </div>
							</div>
							<div class="colection-block-wrap tabs__content">
                                <div class="clolection-clothes-wrap flex-cont justify-sb">
                                    <div class="clothes-wrap">
                                        <div class="clothes-wrap__img">
                                            <img src="{{ asset('/public/frontend/img/pullover2.png') }}" alt="">
                                        </div>
                                        <div class="clothes-wrap__text">
                                            Уютный свитер
                                        </div>
                                    </div>
                                    <div class="clothes-wrap">
                                        <div class="clothes-wrap__img">
                                            <img src="{{ asset('/public/frontend/img/socks-2.png') }}" alt="">
                                        </div>
                                        <div class="clothes-wrap__text">
                                            Теплые носочки
                                        </div>
                                    </div>
                                    <div class="clothes-wrap">
                                        <div class="clothes-wrap__img">
                                            <img src="{{ asset('/public/frontend/img/cap2.png') }}" alt="">
                                        </div>
                                        <div class="clothes-wrap__text">
                                            Совсем не колючая шапка
                                        </div>
                                    </div>
                                    <div class="clothes-wrap">
                                        <div class="clothes-wrap__img">
                                            <img src="{{ asset('/public/frontend/img/gloves2.png') }}" alt="">
                                        </div>
                                        <div class="clothes-wrap__text">
                                            Яркие варежки
                                        </div>
                                    </div>
                                </div>
							</div>
							<div class="colection-block-wrap tabs__content">
                                <div class="clolection-clothes-wrap flex-cont justify-sb">
                                    <div class="clothes-wrap">
                                        <div class="clothes-wrap__img">
                                            <img src="{{ asset('/public/frontend/img/pullover3.png') }}" alt="">
                                        </div>
                                        <div class="clothes-wrap__text">
                                            Уютный свитер
                                        </div>
                                    </div>
                                    <div class="clothes-wrap">
                                        <div class="clothes-wrap__img">
                                            <img src="{{ asset('/public/frontend/img/socks-3.png') }}" alt="">
                                        </div>
                                        <div class="clothes-wrap__text">
                                            Теплые носочки
                                        </div>
                                    </div>
                                    <div class="clothes-wrap">
                                        <div class="clothes-wrap__img">
                                            <img src="{{ asset('/public/frontend/img/cap3.png') }}" alt="">
                                        </div>
                                        <div class="clothes-wrap__text">
                                            Совсем не колючая шапка
                                        </div>
                                    </div>
                                    <div class="clothes-wrap">
                                        <div class="clothes-wrap__img">
                                            <img src="{{ asset('/public/frontend/img/gloves3.png') }}" alt="">
                                        </div>
                                        <div class="clothes-wrap__text">
                                            Яркие варежки
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="text text_big">
                            <p>Хотите выиграть один из этих подарков?</p>
                            <p>Загружайте чеки от покупки печенья «Юбилейное»!</p>
                        </div>
                        <div class="btn-wrap btn-wrap_minMargin">
                            <a href="#" class="btn btn_def js-popup-button" data-popupShow="download-check">
                                <span>Загрузить чек</span>
                            </a>
                        </div>
                    </div>
                </section>
            </main>
        </div>
    </div>
@endsection

