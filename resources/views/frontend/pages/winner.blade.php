@extends('frontend.includes.header')
@section('content')
    <div class="height-helper height-helper_new-bg">
        <div class="content-wrap">
            <!--Main-->
            <main class="main main_new-bg">
                <section class="winners">
                    <div class="container container_little">
                        <div class="title title_big">
                            <span>Победители</span>
                        </div>
                        <div class="form">
                            <form>
                                <div class="form__card">
                                    <div class="form__tips form__tips--big">
                                        <span>Поиск по почте:</span>
                                    </div>
                                    <div class="form__input form__input--maxWidth">
                                        <input id="search-winners">
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="substrate">
                            <div class="winners__table">
                                <table class="table table--padding">
                                    <thead>
                                    <th class="head-winner">Победитель</th>
                                    <th class="head-name">Имя пользователя</th>
                                    <th class="head-phone">Почта</th>
                                    <th class="head-city">Город</th>
                                    <th class="head-date">Дата</th>
                                    <th class="head-prize">Приз</th>
                                    </thead>
                                    <tbody>
                                    @foreach($winners as $winner)
                                        <tr class="search-tr">
                                            <?php
                                            $result = substr($winner->customer->surname, 0, 2);
                                            ?>
                                            <td class="win-name">{{ $winner->customer->name }} <?php echo $result; ?>. </td>
                                            <td >
                                                <span hidden class="win-number"> {{ $winner->customer->email }}</span>
                                                <?php
                                                $result = substr($winner->customer->email, 0, 2);
                                                $result_1 = substr($winner->customer->email, 7, 50);
                                                echo $result.'*****'.$result_1;
                                                ?>

                                            </td>
                                            <td class="win-city">{{ $winner->city }}</td>
                                            <td class="win-date">{{ $winner->date_of_participation }}</td>
                                            <td class="win-prize">
                                                @if($winner->prize == 'Носки')
                                                    <img src="{{ asset('/public/frontend/img/prize-Socks-mob.png') }}" alt="">
                                                @elseif($winner->prize == 'Шапка')
                                                    <img src="{{ asset('/public/frontend/img/prize-Hat-mob.png') }}" alt="">
                                                @elseif($winner->prize == 'Варежки')
                                                    <img src="{{ asset('/public/frontend/img/prize-gloves-mob.png') }}" alt="">
                                                @elseif($winner->prize == 'Свитер')
                                                    <img src="{{ asset('/public/frontend/img/prize-sweater-mob.png') }}" alt="">
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </section>
            </main>
        </div>
    </div>

@endsection

