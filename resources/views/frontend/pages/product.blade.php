@extends('frontend.includes.header')
@section('content')
    <div class="height-helper height-helper_new-bg">
        <div class="content-wrap">
            <!--Main-->
            <main class="main main_new-bg">
                <section class="product">
                    <div class="container">
                        <div class="product__wrap">
                            <div class="title title_big">
                                <span>Продукт</span>
                            </div>
                            <div class="text text_big">
                                <p>Продукт, участвующий в акции.</p>
                            </div>
                            <div class="swiper-container product-slider">
                                <div class="swiper-wrapper product-slider__wrap">
                                    <div class="swiper-slide product-item">
                                        <div class="product-item__img">
                                            <img src="{{ asset('/public/frontend/img/products/product.png') }}" alt="">
                                        </div>
                                        <div class="product-item__text">
                                            <p>Печенье Юбилейное</p>
                                            <p class="product-item__text_subtext">
												Традиционное
                                            </p>
                                        </div>
                                        <div class="btn-wrap">
                                            <a target="_blank" href="https://market.yandex.ru/search?text=%D0%BF%D0%B5%D1%87%D0%B5%D0%BD%D1%8C%D0%B5%20%D1%8E%D0%B1%D0%B8%D0%BB%D0%B5%D0%B9%D0%BD%D0%BE%D0%B5%20%D0%BA%D1%83%D0%BF%D0%B8%D1%82%D1%8C&clid=545&local-offers-first=0"  class="btn btn_min btn_middle">
                                                Купить
                                            </a>
                                        </div>
                                    </div>
                                    <div class="swiper-slide product-item">
                                        <div class="product-item__img">
                                            <img src="{{ asset('/public/frontend/img/products/product2.png') }}" alt="">
                                        </div>
                                        <div class="product-item__text">
                                            <p>Печенье Юбилейное</p>
                                            <p class="product-item__text_subtext">
												С ягодами черники
                                            </p>
                                        </div>
                                        <div class="btn-wrap">
                                            <a target="_blank" href="https://market.yandex.ru/search?text=%D0%BF%D0%B5%D1%87%D0%B5%D0%BD%D1%8C%D0%B5%20%D1%8E%D0%B1%D0%B8%D0%BB%D0%B5%D0%B9%D0%BD%D0%BE%D0%B5%20%D0%BA%D1%83%D0%BF%D0%B8%D1%82%D1%8C&clid=545&local-offers-first=0"  class="btn btn_min btn_middle">
                                                Купить
                                            </a>
                                        </div>
                                    </div>
                                    <div class="swiper-slide product-item">
                                        <div class="product-item__img">
                                            <img src="{{ asset('/public/frontend/img/products/product3.png') }}" alt="">
                                        </div>
                                        <div class="product-item__text">
                                            <p>Печенье Юбилейное</p>
                                            <p class="product-item__text_subtext">
                                                Какао с глазурью
                                            </p>
                                        </div>
                                        <div class="btn-wrap">
                                            <a target="_blank" href="https://market.yandex.ru/search?text=%D0%BF%D0%B5%D1%87%D0%B5%D0%BD%D1%8C%D0%B5%20%D1%8E%D0%B1%D0%B8%D0%BB%D0%B5%D0%B9%D0%BD%D0%BE%D0%B5%20%D0%BA%D1%83%D0%BF%D0%B8%D1%82%D1%8C&clid=545&local-offers-first=0"  class="btn btn_min btn_middle">
                                                Купить
                                            </a>
                                        </div>
                                    </div>
                                    <div class="swiper-slide product-item">
                                        <div class="product-item__img">
                                            <img src="{{ asset('/public/frontend/img/products/product4.png') }}" alt="">
                                        </div>
                                        <div class="product-item__text">
                                            <p>Печенье Юбилейное</p>
                                            <p class="product-item__text_subtext">
												С кусочками клюквы
                                            </p>
                                        </div>
                                        <div class="btn-wrap">
                                            <a target="_blank" href="https://market.yandex.ru/search?text=%D0%BF%D0%B5%D1%87%D0%B5%D0%BD%D1%8C%D0%B5%20%D1%8E%D0%B1%D0%B8%D0%BB%D0%B5%D0%B9%D0%BD%D0%BE%D0%B5%20%D0%BA%D1%83%D0%BF%D0%B8%D1%82%D1%8C&clid=545&local-offers-first=0"  class="btn btn_min btn_middle">
                                                Купить
                                            </a>
                                        </div>
                                    </div>
                                    <div class="swiper-slide product-item">
                                        <div class="product-item__img">
                                            <img src="{{ asset('/public/frontend/img/products/product5.png') }}" alt="">
                                        </div>
                                        <div class="product-item__text">
                                            <p>Печенье Юбилейное</p>
                                            <p class="product-item__text_subtext">
												С тёмной глазурью
                                            </p>
                                        </div>
                                        <div class="btn-wrap">
                                            <a target="_blank" href="https://market.yandex.ru/search?text=%D0%BF%D0%B5%D1%87%D0%B5%D0%BD%D1%8C%D0%B5%20%D1%8E%D0%B1%D0%B8%D0%BB%D0%B5%D0%B9%D0%BD%D0%BE%D0%B5%20%D0%BA%D1%83%D0%BF%D0%B8%D1%82%D1%8C&clid=545&local-offers-first=0"  class="btn btn_min btn_middle">
                                                Купить
                                            </a>
                                        </div>
									</div>
									<div class="swiper-slide product-item">
                                        <div class="product-item__img">
                                            <img src="{{ asset('/public/frontend/img/products/product6.png') }}" alt="">
                                        </div>
                                        <div class="product-item__text">
                                            <p>Печенье Юбилейное</p>
                                            <p class="product-item__text_subtext">
												С тёмной глазурью
                                            </p>
                                        </div>
                                        <div class="btn-wrap">
                                            <a target="_blank" href="https://market.yandex.ru/search?text=%D0%BF%D0%B5%D1%87%D0%B5%D0%BD%D1%8C%D0%B5%20%D1%8E%D0%B1%D0%B8%D0%BB%D0%B5%D0%B9%D0%BD%D0%BE%D0%B5%20%D0%BA%D1%83%D0%BF%D0%B8%D1%82%D1%8C&clid=545&local-offers-first=0"  class="btn btn_min btn_middle">
                                                Купить
                                            </a>
                                        </div>
                                    </div>
                                    <div class="swiper-slide product-item">
                                        <div class="product-item__img">
                                            <img src="{{ asset('/public/frontend/img/products/product7.png') }}" alt="">
                                        </div>
                                        <div class="product-item__text">
                                            <p>Печенье Юбилейное</p>
                                            <p class="product-item__text_subtext">
												Молочное
                                            </p>
                                        </div>
                                        <div class="btn-wrap">
                                            <a target="_blank" href="https://market.yandex.ru/search?text=%D0%BF%D0%B5%D1%87%D0%B5%D0%BD%D1%8C%D0%B5%20%D1%8E%D0%B1%D0%B8%D0%BB%D0%B5%D0%B9%D0%BD%D0%BE%D0%B5%20%D0%BA%D1%83%D0%BF%D0%B8%D1%82%D1%8C&clid=545&local-offers-first=0"  class="btn btn_min btn_middle">
                                                Купить
                                            </a>
                                        </div>
                                    </div>
                                    <div class="swiper-slide product-item">
                                        <div class="product-item__img">
                                            <img src="{{ asset('/public/frontend/img/products/product8.png') }}" alt="">
                                        </div>
                                        <div class="product-item__text">
                                            <p>Печенье Юбилейное</p>
                                            <p class="product-item__text_subtext">
												Молочное с глазурью
                                            </p>
                                        </div>
                                        <div class="btn-wrap">
                                            <a target="_blank" href="https://market.yandex.ru/search?text=%D0%BF%D0%B5%D1%87%D0%B5%D0%BD%D1%8C%D0%B5%20%D1%8E%D0%B1%D0%B8%D0%BB%D0%B5%D0%B9%D0%BD%D0%BE%D0%B5%20%D0%BA%D1%83%D0%BF%D0%B8%D1%82%D1%8C&clid=545&local-offers-first=0"  class="btn btn_min btn_middle">
                                                Купить
                                            </a>
                                        </div>
                                    </div>
                                    <div class="swiper-slide product-item">
                                        <div class="product-item__img">
                                            <img src="{{ asset('/public/frontend/img/products/product9.png') }}" alt="">
                                        </div>
                                        <div class="product-item__text">
                                            <p>Печенье Юбилейное</p>
                                            <p class="product-item__text_subtext">
												Молочное с глазурью
                                            </p>
                                        </div>
                                        <div class="btn-wrap">
                                            <a target="_blank" href="https://market.yandex.ru/search?text=%D0%BF%D0%B5%D1%87%D0%B5%D0%BD%D1%8C%D0%B5%20%D1%8E%D0%B1%D0%B8%D0%BB%D0%B5%D0%B9%D0%BD%D0%BE%D0%B5%20%D0%BA%D1%83%D0%BF%D0%B8%D1%82%D1%8C&clid=545&local-offers-first=0"  class="btn btn_min btn_middle">
                                                Купить
                                            </a>
                                        </div>
                                    </div>
                                    <div class="swiper-slide product-item">
                                        <div class="product-item__img">
                                            <img src="{{ asset('/public/frontend/img/products/product10.png') }}" alt="">
                                        </div>
                                        <div class="product-item__text">
                                            <p>Печенье Юбилейное</p>
                                            <p class="product-item__text_subtext">
												Ореховое с глазурью
                                            </p>
                                        </div>
                                        <div class="btn-wrap">
                                            <a target="_blank" href="https://market.yandex.ru/search?text=%D0%BF%D0%B5%D1%87%D0%B5%D0%BD%D1%8C%D0%B5%20%D1%8E%D0%B1%D0%B8%D0%BB%D0%B5%D0%B9%D0%BD%D0%BE%D0%B5%20%D0%BA%D1%83%D0%BF%D0%B8%D1%82%D1%8C&clid=545&local-offers-first=0"  class="btn btn_min btn_middle">
                                                Купить
                                            </a>
                                        </div>
									</div>
									<div class="swiper-slide product-item">
                                        <div class="product-item__img">
                                            <img src="{{ asset('/public/frontend/img/products/product11.png') }}" alt="">
                                        </div>
                                        <div class="product-item__text">
                                            <p>Печенье Юбилейное</p>
                                            <p class="product-item__text_subtext">
												Молочное
                                            </p>
                                        </div>
                                        <div class="btn-wrap">
                                            <a target="_blank" href="https://market.yandex.ru/search?text=%D0%BF%D0%B5%D1%87%D0%B5%D0%BD%D1%8C%D0%B5%20%D1%8E%D0%B1%D0%B8%D0%BB%D0%B5%D0%B9%D0%BD%D0%BE%D0%B5%20%D0%BA%D1%83%D0%BF%D0%B8%D1%82%D1%8C&clid=545&local-offers-first=0"  class="btn btn_min btn_middle">
                                                Купить
                                            </a>
                                        </div>
                                    </div>
                                    <div class="swiper-slide product-item">
                                        <div class="product-item__img">
                                            <img src="{{ asset('/public/frontend/img/products/product12.png') }}" alt="">
                                        </div>
                                        <div class="product-item__text">
                                            <p>Печенье Юбилейное</p>
                                            <p class="product-item__text_subtext">
												С овсяными хлопьями
                                            </p>
                                        </div>
                                        <div class="btn-wrap">
                                            <a target="_blank" href="https://market.yandex.ru/search?text=%D0%BF%D0%B5%D1%87%D0%B5%D0%BD%D1%8C%D0%B5%20%D1%8E%D0%B1%D0%B8%D0%BB%D0%B5%D0%B9%D0%BD%D0%BE%D0%B5%20%D0%BA%D1%83%D0%BF%D0%B8%D1%82%D1%8C&clid=545&local-offers-first=0"  class="btn btn_min btn_middle">
                                                Купить
                                            </a>
                                        </div>
                                    </div>
                                    <div class="swiper-slide product-item">
                                        <div class="product-item__img">
                                            <img src="{{ asset('/public/frontend/img/products/product13.png') }}" alt="">
                                        </div>
                                        <div class="product-item__text">
                                            <p>Печенье Юбилейное</p>
                                            <p class="product-item__text_subtext">
												Постное
                                            </p>
                                        </div>
                                        <div class="btn-wrap">
                                            <a target="_blank" href="https://market.yandex.ru/search?text=%D0%BF%D0%B5%D1%87%D0%B5%D0%BD%D1%8C%D0%B5%20%D1%8E%D0%B1%D0%B8%D0%BB%D0%B5%D0%B9%D0%BD%D0%BE%D0%B5%20%D0%BA%D1%83%D0%BF%D0%B8%D1%82%D1%8C&clid=545&local-offers-first=0"  class="btn btn_min btn_middle">
                                                Купить
                                            </a>
                                        </div>
                                    </div>
                                    <div class="swiper-slide product-item">
                                        <div class="product-item__img">
                                            <img src="{{ asset('/public/frontend/img/products/product14.png') }}" alt="">
                                        </div>
                                        <div class="product-item__text">
                                            <p>Печенье Юбилейное</p>
                                            <p class="product-item__text_subtext">
												Традиционное
                                            </p>
                                        </div>
                                        <div class="btn-wrap">
                                            <a target="_blank" href="https://market.yandex.ru/search?text=%D0%BF%D0%B5%D1%87%D0%B5%D0%BD%D1%8C%D0%B5%20%D1%8E%D0%B1%D0%B8%D0%BB%D0%B5%D0%B9%D0%BD%D0%BE%D0%B5%20%D0%BA%D1%83%D0%BF%D0%B8%D1%82%D1%8C&clid=545&local-offers-first=0"  class="btn btn_min btn_middle">
                                                Купить
                                            </a>
                                        </div>
                                    </div>
                                    <div class="swiper-slide product-item">
                                        <div class="product-item__img">
                                            <img src="{{ asset('/public/frontend/img/products/product15.png') }}" alt="">
                                        </div>
                                        <div class="product-item__text">
                                            <p>Печенье Юбилейное</p>
                                            <p class="product-item__text_subtext">
												Традиционное
                                            </p>
                                        </div>
                                        <div class="btn-wrap">
                                            <a target="_blank" href="https://market.yandex.ru/search?text=%D0%BF%D0%B5%D1%87%D0%B5%D0%BD%D1%8C%D0%B5%20%D1%8E%D0%B1%D0%B8%D0%BB%D0%B5%D0%B9%D0%BD%D0%BE%D0%B5%20%D0%BA%D1%83%D0%BF%D0%B8%D1%82%D1%8C&clid=545&local-offers-first=0"  class="btn btn_min btn_middle">
                                                Купить
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="product-btn swiper-button-next"></div>
                            <div class="product-btn swiper-button-prev"></div>
                        </div>

                    </div>
                </section>
            </main>
        </div>
    </div>

@endsection

