@extends('frontend.includes.header')

@section('content')
    <div class="height-helper height-helper_new-bg">
        <div class="content-wrap">
            <!--Main-->
            <main class="main main_new-bg">
                <section class="ours-grandmothers">
                    <div class="container">
                        <div class="title title_big">
                            <span>Теплые подарки от бабушек</span>
						</div>
						{{--<div class="fond-wrap">--}}
							{{--<div class="fond-wrap__text">При поддержке фонда</div>--}}
							{{--<div class="fond-wrap__img">--}}
								{{--<img src="{{ asset('/public/frontend/img/picture-text.png') }}" alt="">--}}
							{{--</div>--}}
						{{--</div>--}}
                        <div class="text text_middle text_absol">
							{{--<p>Этот проект создан совместно с фондом «Старость в радость», чтобы</p>--}}
							{{--<p>одинокие бабушки вновь почувствовали себя важными и нужными. </p>--}}
							<p>Мы попросили бабушек связать для вас теплые свитера, а еще шапки, носочки и</p>
							<p>варежки. Все вещи созданы вручную, с большим вниманием и заботой.</p>
                        </div>
                        <div class="wrap-grandmothers">
                            <div class="title title_middle">
                                <span>Бабушки-мастерицы</span>
							</div>
                            <div class="text text_middle text_minMargin">
							<!-- Раскомитив блок, удалить mb100 mt100 и первую "p" -->
                                <!-- <p>Здесь вы совсем скоро сможете познакомиться с нашими бабушками.</p>Первая "p" -->
                                <p>Здесь вы можете поблагодарить наших бабушек-мастериц. Уделите</p>
                                <p>полминутки, скажите несколько теплых слов, а мы обязательно передадим</p>
                                <p>вашу благодарность по назначению.</p>
                            </div>
                            <div id="grandmothers-slider" class="swiper-container grandmothers-slider">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide grandmothers-slider__item">
                                        <div class="about-grandmother">
                                            <div class="about-grandmother__photo">
												<img src="{{ asset('/public/frontend/img/irina-vikt.png') }}" alt="">
                                            </div>
                                            <div class="about-grandmother__name">
												Ирина Викторовна
                                            </div>
                                            <div class="about-grandmother__fond">
												Бабушка-умелица
                                            </div>
                                            <div class="about-grandmother__text">
                                                Эта бабушка обожает старину и неспешные прогулки по московским улочкам. Всё тепло своего сердца она отдает трём внукам, а теперь и вам.
                                            </div>
                                        </div>
                                        <div class="btn-wrap btn-wrap_minMargin">
											{{--data-popupShow="gallery-card-add"--}}
                                            <a  class="btn btn_def btn_congratulate js-popup-button" data-popupShow="gallery-card-add">
                                                <span>Сказать спасибо</span>
                                            </a>
                                        </div>
                                    </div>
									<div class="swiper-slide grandmothers-slider__item">
                                        <div class="about-grandmother">
                                            <div class="about-grandmother__photo">
												<img src="{{ asset('/public/frontend/img/elena-germ.png') }}" alt="">
                                            </div>
                                            <div class="about-grandmother__name">
												Елена Германовна
                                            </div>
                                            <div class="about-grandmother__fond">
												Бабушка-мастерица
                                            </div>
                                            <div class="about-grandmother__text">
												Эта бабушка – мастерица на все руки: умеет рисовать, шить, вязать и лепить. В каждую вещь она вкладывает частичку себя, поэтому её носочки и варежки совсем не колючие.
                                            </div>
                                        </div>
                                        <div class="btn-wrap btn-wrap_minMargin">
											{{--data-popupShow="gallery-card-add"--}}
                                            <a  class="btn btn_def btn_congratulate js-popup-button" data-popupShow="gallery-card-add">
                                                <span>Сказать спасибо</span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="swiper-slide grandmothers-slider__item">
                                        <div class="about-grandmother">
                                            <div class="about-grandmother__photo">
												<img src="{{ asset('/public/frontend/img/galina-nik.png') }}" alt="">
                                            </div>
                                            <div class="about-grandmother__name">
												Галина Николаевна
                                            </div>
                                            <div class="about-grandmother__fond">
												Бабушка-затейница
                                            </div>
                                            <div class="about-grandmother__text">
												У этой бабушки увлечений – не счесть, включая садоводство, валяние и даже нумерологию. Но больше всего ей нравится заботиться о близких и дарить им радость.
                                            </div>
                                        </div>
                                        <div class="btn-wrap btn-wrap_minMargin">
                                            {{--data-popupShow="gallery-card-add"--}}
                                            <a  class="btn btn_def btn_congratulate js-popup-button" data-popupShow="gallery-card-add">
                                                <span>Сказать спасибо</span>
                                            </a>
                                        </div>
                                    </div>
									<div class="swiper-slide grandmothers-slider__item">
                                        <div class="about-grandmother">
                                            <div class="about-grandmother__photo">
												<img src="{{ asset('/public/frontend/img/elena-vlad.png') }}" alt="">
                                            </div>
                                            <div class="about-grandmother__name">
												Елена Владимировна
                                            </div>
                                            <div class="about-grandmother__fond">
												Бабушка-путешественница
                                            </div>
                                            <div class="about-grandmother__text">
												Эта бабушка любит все новое и интересное, однажды она сама 	путешествовала по Индии! В её руках обычный клубочек шерсти превращается в согревающий подарок.
                                            </div>
                                        </div>
                                        <div class="btn-wrap btn-wrap_minMargin">
                                            {{--data-popupShow="gallery-card-add"--}}
                                            <a  class="btn btn_def btn_congratulate js-popup-button" data-popupShow="gallery-card-add">
                                                <span>Сказать спасибо</span>
                                            </a>
                                        </div>
                                    </div>
									<div class="swiper-slide grandmothers-slider__item">
                                        <div class="about-grandmother">
                                            <div class="about-grandmother__photo">
												<img src="{{ asset('/public/frontend/img/irina-vlad.png') }}" alt="">
                                            </div>
                                            <div class="about-grandmother__name">
												Ирина Владимировна
                                            </div>
                                            <div class="about-grandmother__fond">
												Бабушка-искусница
                                            </div>
                                            <div class="about-grandmother__text">
												Эта бабушка обожает театр и классическую музыку! Она так любит вязать, что с удовольствием подарит вам тепло носочков и своего сердца к сезону холодов.
                                            </div>
                                        </div>
                                        <div class="btn-wrap btn-wrap_minMargin">
                                            {{--data-popupShow="gallery-card-add"--}}
                                            <a  class="btn btn_def btn_congratulate js-popup-button" data-popupShow="gallery-card-add">
                                                <span>Сказать спасибо</span>
                                            </a>
                                        </div>
                                    </div>
									<div class="swiper-slide grandmothers-slider__item">
                                        <div class="about-grandmother">
                                            <div class="about-grandmother__photo">
												<img src="{{ asset('/public/frontend/img/olga-viktorovna.png') }}" alt="">
                                            </div>
                                            <div class="about-grandmother__name">
												Ольга Викторовна
                                            </div>
                                            <div class="about-grandmother__fond">
												Бабушка-цветочница
                                            </div>
                                            <div class="about-grandmother__text">
												Эта бабушка обожает цветы, особенно фиалки, ведь они радуют глаз и душу. Лепит, рисует и окружает душевным теплом всех, кому вяжет теплые подарочки.
                                            </div>
                                        </div>
                                        <div class="btn-wrap btn-wrap_minMargin">
                                            {{--data-popupShow="gallery-card-add"--}}
                                            <a  class="btn btn_def btn_congratulate js-popup-button" data-popupShow="gallery-card-add">
                                                <span>Сказать спасибо</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="grandmothers-btn swiper-button-next"></div>
                                <div class="grandmothers-btn swiper-button-prev"></div>
                            </div>
						</div>
                        @if($thanks != '[]')
                        <div class="gallery-wrap">
                            <div class="title title_middle">
                                <span>Галерея благодарностей</span>
                            </div>
                            <div id="congratulate-wrap" class="congratulate-wrap flex-cont justify-c">
								@foreach($thanks as $thank)
								<div class="congratulate-box-wrap">
									<div class="congratulate-box">
										<input class="grand-img" hidden value="{{ asset('/public/storage/GrandImage/'. $thank->image) }}">			
										<input class="grand-img-name" hidden value="{{ $thank->image }}">
										<input class="grand-video" hidden value="{{ asset('/public/storage/Video/'. $thank->video) }}">
										<input class="grand-video-name" hidden value="{{ $thank->video }}">
										<div class="congratulate-box__heading">
											<p class="congratulate-box__heading_for">Для бабушки {{ $thank->grand_name }}</p>
											<p class="congratulate-box__heading_from">от {{ $thank->name }}</p>
										</div>
										<div class="congratulate-box__content">
											<div class="congratulate-photo">
												<div class="icon-btn js-popup-button"  data-popupShow="gallery-card-slider">
													<span class="congratulate-icon icon-photo-send"></span>
												</div>
											</div>
											<div class="congratulate-video">
                                                <div class="icon-btn js-popup-button"  data-popupShow="gallery-card-slider">
													<span class="congratulate-icon icon-video-send"></span>
												</div>
											</div>
										</div>
										<div class="congratulate-box__text">
											{{ $thank->message }}
										</div>
										<a class="congratulate-box__all js-popup-button" href="#" data-popupShow="gallery-card-slider">Смотреть полностью</a>
									</div>
								</div>
								@endforeach
								<div class="congratulate-box-wrap_test"></div>
                            </div>
                            <div class="btn-wrap btn-wrap_bigMargin">
                                <a href="#" id="show-more" class="btn btn_def btn_congratulate">
                                    <span>Показать еще</span>
                                </a>
                            </div>
                        </div>
                        @endif
                    </div>
                </section>
            </main>
        </div>
    </div>
@endsection

