
<div class="header__section">
    <a href="/" class="logo"><img src="{{ asset('public/frontend/img/logo-dark.svg') }}" alt="kiwiroom"></a>
    <div class="header__blocks take">
        <div class="header__links-blocks">
            <a href="#" class="header__links header__links_clicker">Search Adverts</a>
            <img class="header__links-img open" src="{{ asset('public/frontend/img/add.svg') }}" alt="+">
            <img class="header__links-img close" src="{{ asset('public/frontend/img/add_min.svg') }}" alt="+">
            <nav class="header__menu">
                <ul class="header__menu-block">
                    <li class="header__menu-paragraph">
                        <a href="/search" class="header__menu-links">Landlord Adverts</a>
                    </li>
                    <li class="header__menu-paragraph">
                        <a href="/search-tenant-advert" class="header__menu-links">Tenant Adverts</a>
                    </li>
                </ul>
            </nav>
        </div>
        <div class="header__links-blocks">
            <a href="#" class="header__links header__links_clicker">Search people</a>
            <img class="header__links-img open" src="{{ asset('public/frontend/img/add.svg') }}" alt="+">
            <img class="header__links-img close" src="{{ asset('public/frontend/img/add_min.svg') }}" alt="+">
            <nav class="header__menu">
                <ul class="header__menu-block">
                    <li class="header__menu-paragraph">
                        <a href="/search-landlord" class="header__menu-links">Landlord</a>
                    </li>
                    <li class="header__menu-paragraph">
                        <a href="/search-tenant" class="header__menu-links">Tenant</a>
                    </li>
                </ul>
            </nav>
        </div>
        @if(Auth::guard('customer')->check())
            <div class="header__links-blocks">
                <a href="#" class="header__links header__links_clicker add"><span class="link-icon add"></span>Add</a>
                <img class="header__links-img open" src="{{ asset('public/frontend/img/add.svg') }}" alt="+">
                <img class="header__links-img close" src="{{ asset('public/frontend/img/add_min.svg') }}" alt="+">
                <nav class="header__menu">
                    <ul class="header__menu-block">
                        @if(Auth::guard('customer')->user()->role == 'landlord')
                            <li class="header__menu-paragraph">
                                <a href="/customer/landlord-publish" class="header__menu-links">Property advert</a>
                            </li>
                        @endif
                        @if(Auth::guard('customer')->user()->role == 'agent')
                            @if(Auth::guard('customer')->user()->agency_id != 0)
                                    @if(Auth::guard('customer')->user()->agency->admin_confirm == 1)
                                    <li class="header__menu-paragraph">
                                        <a href="/customer/landlord-publish" class="header__menu-links">Property advert</a>
                                    </li>
                                    @endif
                            @else
                                <li class="header__menu-paragraph">
                                    <a href="/customer/agency" class="header__menu-links">Agency</a>
                                </li>
                            @endif
                        @endif

                        @if(Auth::guard('customer')->user()->role == 'tenant' && Auth::guard('customer')->user()->tenant_advert == '')
                            <li class="header__menu-paragraph">
                                <a href="/customer/tenant-publish" class="header__menu-links">Advert</a>
                            </li>
                        @endif
                        <li class="header__menu-paragraph">
                            <a href="/customer/landlord/review" class="header__menu-links">Property feedback</a>
                        </li>
                        <li class="header__menu-paragraph">
                            <a href="/customer/tenant/future-review" class="header__menu-links">Tenant feedback</a>
                        </li>
                    </ul>
                </nav>
            </div>
        @endif
        <div class="header__links-blocks take"></div>
    </div>
</div>
<div class="header__blocks drop">
    {{--<select class="header__select select">--}}
        {{--<option>NZD</option>--}}
        {{--<option>KZD</option>--}}
        {{--<option>RZD</option>--}}
    {{--</select>--}}
    @if(Auth::guard('customer')->check())
    <div class="header__links-blocks dropped">
        @if(Auth::guard('customer')->user()->role == 'tenant')
            <a href="{{ url('/customer/tenant') }}" class="header__links"><span class="link-icon user"></span>My profile</a>
        @else
            <a href="{{ url('/customer/landlord') }}" class="header__links"><span class="link-icon user"></span>My profile</a>
        @endif
    </div>
    <div class="header__links-blocks dropped">
        <a href="{{ url('/customer/wishlist') }}" class="header__links header__links_wishlist"><svg xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:cc="http://creativecommons.org/ns#" xmln:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd" xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape" height="451" width="451" fill="#000000" version="1.1" x="0px" y="0px" viewBox="0 0 100 100"><g transform="translate(0,-952.36218)"><path style="text-indent:0;text-transform:none;direction:ltr;block-progression:tb;baseline-shift:baseline;color:#000000;enable-background:accumulate;" d="m 34.279575,971.38874 c -4.7243,0 -9.4248,1.88753 -12.9688,5.65625 -7.088,7.53731 -7.0786,19.55183 0,27.09381 l 26.4688,28.25 a 3.0003,3.0003 0 0 0 4.375,0 c 8.842,-9.4027 17.6891,-18.816 26.5312,-28.2188 7.0879,-7.53743 7.0879,-19.55634 0,-27.09376 -7.088,-7.5374 -18.8807,-7.53755 -25.9687,0 l -2.7188,2.875 -2.7187,-2.90625 c -3.5441,-3.7687 -8.2758,-5.65625 -13,-5.65625 z m 0,5.875 c 3.0892,0 6.1819,1.27701 8.625,3.875 l 4.9375,5.25 a 3.0003,3.0003 0 0 0 4.3437,0 l 4.9063,-5.21875 c 4.8861,-5.19607 12.3326,-5.19596 17.2187,0 4.8861,5.19596 4.8861,13.67902 0,18.87501 -8.115,8.6296 -16.1975,17.2768 -24.3125,25.9063 l -24.3125,-25.9375 c -4.884,-5.20374 -4.8861,-13.67923 0,-18.87506 2.443,-2.59797 5.5045,-3.875 8.5938,-3.875 z" fill-opacity="1" stroke="none" marker="none" visibility="visible" display="inline" overflow="visible"/></g></svg>Watchlist</a>
    </div>
    <div class="header__links-blocks dropped">
        <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="header__links"><span class="link-icon exit"></span>Logout</a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST"
              style="display: none;">
            {{ csrf_field() }}
        </form>
    </div>
    @else
    <div class="header__links-blocks dropped">
        <a href="{{ route('customer.login') }}" class="header__links"><span class="link-icon lock"></span>Login</a>
    </div>
    <div class="header__links-blocks dropped">
        <a href="{{ url('/customer/register') }}" class="header__links">Register</a>
    </div>
    @endif


</div>
<div class="header__blocks menu-btn">
    <div class="bar1"></div>
    <div class="bar2"></div>
    <div class="bar3"></div>
</div>