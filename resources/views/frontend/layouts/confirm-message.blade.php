<div class="container">
    @if(Auth::guard('customer')->user()->confirm_files == '[]')
        <div class="one-prop-page-stat one-prop-page-stat_disabled one-prop-page-stat_text-left">
            <span>Your identity is not confirmed, confirm it to get more potential tenants</span>
            <a href="{{ url('/customer/confirm-file') }}" class="confirm-btn">Confirm</a>
        </div>
    @else
        <div class="one-prop-page-stat one-prop-page-stat_enable one-prop-page-stat_text-left">
            <span>Your files are received, wait.</span>
        </div>
    @endif
</div>