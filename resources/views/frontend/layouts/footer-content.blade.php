<div class="footer__top">
    <div class="footer__l-side">
        <a href="#" class="logo footer__logo"><img src="{{ asset('public/frontend/img/logo.svg') }}" alt="kiwiroom"></a>
        <p class="footer__logo-p">Let’s find your dream home.</p>
    </div>
    <div class="footer__r-side">
        <ul class="footer__links-block">
            @if($settings->where('name', 'link_1')->first()->value != '')
                <li class="footer__links">
                    <a href="{{ $settings->where('name', 'link_1')->first()->value }}" class="footer__link">{{ $settings->where('name', 'link_name_1')->first()->value }}</a>
                </li>
            @endif
            @if($settings->where('name', 'link_2')->first()->value != '')
                <li class="footer__links">
                    <a href="{{ $settings->where('name', 'link_2')->first()->value }}" class="footer__link">{{ $settings->where('name', 'link_name_2')->first()->value }}</a>
                </li>
            @endif
            @if($settings->where('name', 'link_3')->first()->value != '')
                <li class="footer__links">
                    <a href="{{ $settings->where('name', 'link_3')->first()->value }}" class="footer__link">{{ $settings->where('name', 'link_name_3')->first()->value }}</a>
                </li>
            @endif
            @if($settings->where('name', 'link_4')->first()->value != '')
                <li class="footer__links">
                    <a href="{{ $settings->where('name', 'link_4')->first()->value }}" class="footer__link">{{ $settings->where('name', 'link_name_4')->first()->value }}</a>
                </li>
            @endif
        </ul>
        <ul class="footer__links-block footer__links-block_soc">
            <li class="footer__links footer__links_soc">
                <p class="footer__link footer__link_soc">Follow us</p>
            </li>
            @if($settings->where('name', 'twitter')->first()->value != '')
                <li class="footer__links footer__links_soc">
                    <a href="{{ $settings->where('name', 'twitter')->first()->value }}" class="footer__link footer__link_soc"><img src="{{ asset('public/frontend/img/twitter.png') }}"></a>
                </li>
            @endif
            @if($settings->where('name', 'instagram')->first()->value != '')
                <li class="footer__links footer__links_soc">
                    <a href="{{ $settings->where('name', 'instagram')->first()->value }}" class="footer__link footer__link_soc">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 56.7 56.7" style="enable-background:new 0 0 56.7 56.7;" xml:space="preserve"><style type="text/css">	.st0f{fill:#FFFFFF;}</style><path class="st0f" d="M43.4,4.8H13c-5.3,0-9.6,4.3-9.6,9.6v10.1v20.3c0,5.3,4.3,9.6,9.6,9.6h30.4c5.3,0,9.6-4.3,9.6-9.6V24.5V14.4  C53,9.1,48.7,4.8,43.4,4.8z M46.2,10.5l1.1,0v1.1v7.3l-8.4,0l0-8.4L46.2,10.5z M21.1,24.5c1.6-2.2,4.2-3.6,7.1-3.6s5.5,1.4,7.1,3.6  c1,1.4,1.7,3.2,1.7,5.1c0,4.8-3.9,8.7-8.7,8.7c-4.8,0-8.7-3.9-8.7-8.7C19.5,27.7,20.1,26,21.1,24.5z M48.2,44.8  c0,2.6-2.1,4.8-4.8,4.8H13c-2.6,0-4.8-2.1-4.8-4.8V24.5h7.4c-0.6,1.6-1,3.3-1,5.1c0,7.5,6.1,13.6,13.6,13.6  c7.5,0,13.6-6.1,13.6-13.6c0-1.8-0.4-3.5-1-5.1h7.4V44.8z"/></svg>
                    </a>
                </li>
            @endif
            @if($settings->where('name', 'facebook')->first()->value != '')
                <li class="footer__links footer__links_soc">
                    <a href="{{ $settings->where('name', 'facebook')->first()->value }}" class="footer__link footer__link_soc">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 56.7 56.7" style="enable-background:new 0 0 56.7 56.7;" xml:space="preserve"><style type="text/css">	.st0f{fill:#FCFCFC;}</style><path class="st0f" d="M40.4,21.7h-7.6v-5c0-1.9,1.2-2.3,2.1-2.3c0.9,0,5.4,0,5.4,0V6.1l-7.4,0c-8.2,0-10.1,6.2-10.1,10.1v5.5H18v8.5  h4.8c0,10.9,0,24.1,0,24.1h10c0,0,0-13.3,0-24.1h6.8L40.4,21.7z"/></svg>
                    </a>
                </li>
            @endif
        </ul>
    </div>
</div>