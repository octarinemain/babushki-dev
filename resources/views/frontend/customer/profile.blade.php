@extends('frontend.includes.header')

@section('content')
    <div class="height-helper height-helper_new-bg">
        <div class="content-wrap">
            <!--Main-->
            <main class="main main_new-bg">
                <section class="personal-info">
                    <div class="container container_little">
                        <div class="title title_big">
                            <span>Личный кабинет</span>
                        </div>
                        <div class="tabs">
                            <ul class="nav-tabs flex-cont justify-c">
                                <li class="active">
                                    <span class="tabs_text-desk">Ваши чеки</span>
                                    <span class="tabs_text-mob">Чеки</span>
                                </li>
                                <li>
                                    <span class="tabs_text-desk">Ваши подарки</span>
                                    <span class="tabs_text-mob">Подарки</span>
                                </li>
                                <li>
                                    <span class="tabs_text-desk">Ваши коды</span>
                                    <span class="tabs_text-mob">Коды</span>
                                </li>
                            </ul>
                            <div class="substrate content-tabs-wrap">
                                <div class="content-tabs active @if(Auth::guard('customer')->user()->checks != '[]') checks-add @endif "> <!-- Что бы добавить чеки, добавь .checks-add  -->
                                    @if(Auth::guard('customer')->user()->checks == '[]')
                                        <div class="checks-no">
                                        <div class="text text_big">
                                            <p>Загружайте чеки от покупок печенья Юбилейного</p>
                                            <p>и выигрывайте теплые шапки, носки и варежки от наших милых бабушек.</p>
                                        </div>
                                        <ul class="partners-logo">
                                            <li></li>
                                            <li></li>
                                            <li></li>
                                        </ul>
                                        <div class="text text_big">
                                            <p>А если вы загрузите чек от покупки в магазинах</p>
                                            <p>Пятерочка, Перекресток или Карусель, вы сможете выиграть уникальный теплый свитер!</p>
                                        </div>
                                    </div>
                                    @else
                                        <div class="checks-yes">
                                        <div class="text text_big">
                                            <p>Загружайте чеки от покупок печенья Юбилейного</p>
                                            <p>и выигрывайте <a href="#">теплые подарки</a> от бабушек.</p>
                                        </div>
                                        <div class="personal-table">
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th>Номер чека</th>
                                                    <th>Дата регистрации</th>
                                                    <th>Фото чека</th>
                                                    <th>Статус чека</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach(Auth::guard('customer')->user()->checks as $check)
                                                    <tr>
                                                    <td class="personal-num">{{ $check->id }}</td>
                                                    <td>{{ date('d-m-Y', strtotime($check->created_at)) }}</td>
                                                    <td class="personal-photo-check">
														@if($check->photo != '[]')
															<div class="check-photo">
																<img src="{{ asset('/public/storage/Check/'.$check->photo[0]->image) }}" alt="">
															</div>
														@endif
                                                    </td>
                                                    <td class="error">
                                                        @if($check->status_1 == 'Ожидает заявки')
                                                            Принят
                                                        @elseif($check->status_1 == 'Отклонен')
                                                            <a class="question">{{ $check->status_1 }}</a>
                                                        @else
                                                            {{ $check->status_1 }}
                                                        @endif
                                                    </td>
                                                </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    @endif
                                    <div class="btn-wrap btn-wrap_bigMargin">
                                        <a href="#" class="btn btn_def js-popup-button" data-popupShow="download-check">Загрузить
                                            чек</a>
                                    </div>
                                </div>
                                <div class="content-tabs  @if(Auth::guard('customer')->user()->checks != '[]') checks-add @endif " >
                                    @if(Auth::guard('customer')->user()->checks == '[]')
                                        <div class="checks-no">
                                            <div class="text text_big text_table">
                                                <p>У вас пока нет подарков.</p>
                                            </div>
                                            <div class="text text_big text_table">
                                                <p>Загружайте чеки от покупок печенья Юбилейного</p>
                                                <p>и выигрывайте теплые подарки от милых бабушек.</p>
                                            </div>
                                            <div class="btn-wrap btn-wrap_bigMargin">
                                                <a href="#" class="btn btn_def js-popup-button" data-popupShow="download-check">Загрузить
                                                    чек</a>
                                            </div>
                                        </div>
                                    @else
                                        <div class="checks-yes">
                                            <div class="personal-table">
                                                <table id="table-prize" class="table table_prize">
                                                    <thead>
                                                    <tr>
                                                        <th>Номер чека</th>
                                                        <th>Дата регистрации</th>
                                                        <th class="heading-photo-checks">Фото чека</th>
                                                        <th>Приз</th>
                                                        <th>Статус выдачи</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach(Auth::guard('customer')->user()->checks as $check)
                                                        @if($check->prize != '')
                                                            <tr class="prize-text-wrap">
                                                                <td class="personal-num">{{ $check->id }}</td>
                                                                <td>{{ date('d-m-Y', strtotime($check->created_at)) }}</td>
                                                                <td class="personal-photo-check personal-photo-check_gift">
                                                                    <div class="check-photo">
                                                                        <img src="{{ asset('/public/storage/Check/'.$check->photo[0]->image) }}" alt="">
                                                                    </div>
                                                                </td>
                                                                <td class="gift">
                                                                    <input hidden id="prize-type" value="{{ $check->prize }}" />
                                                                    @if($check->prize == 'Носки')
                                                                        <img src="{{ asset('/public/frontend/img/prize-Socks-mob.png') }}" alt="">
                                                                    @elseif($check->prize == 'Шапка')
                                                                        <img src="{{ asset('/public/frontend/img/prize-Hat-mob.png') }}" alt="">
                                                                    @elseif($check->prize == 'Варежки')
                                                                        <img src="{{ asset('/public/frontend/img/prize-gloves-mob.png') }}" alt="">
                                                                    @elseif($check->prize == 'Свитер')
                                                                        <img src="{{ asset('/public/frontend/img/prize-sweater-mob.png') }}" alt="">
                                                                    @endif
                                                                </td>
                                                                <td class="give-prize">
                                                                    @if($check->status_2 == 'Ожидает заявки')
																	<a href="#" class="btn btn_min btn_middle js-popup-button" data-popupShow="send-gift">
																		Получить приз
																	</a>
                                                                    @elseif($check->status_2 == 'Ожидает отправки')
																		<input hidden class="text-index" value="{{ $check->reg_index }}" >
																		<input hidden class="text-address" value="{{ $check->region }}, {{ $check->d_city }}, {{ $check->street }}, {{ $check->house }}, {{ $check->flat }}" >
                                                                        <a href="#" class="wait js-popup-button" data-popupShow="address">
                                                                            {{ $check->status_2 }}
                                                                        </a>
                                                                    @elseif($check->status_2 == 'Трек номер')
                                                                        <p>Трек номер:</p>
                                                                        <a target="_blank" href="https://www.pochta.ru/TRACKING#{{ $check->track_number  }}" class="track-nomber" >
                                                                            {{ $check->track_number }}
                                                                        </a>
                                                                    @else
                                                                        {{ $check->status_2 }}
                                                                    @endif
                                                                </td>
                                                            </tr>
                                                        @endif
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                                <div class="content-tabs   @if(Auth::guard('customer')->user()->codes != '[]') checks-add @endif ">
                                    @if(Auth::guard('customer')->user()->codes == '[]')
                                        <div class="checks-no">
                                            <div class="text text_big">
                                                <p>Регистрируйте коды с пачек печенья Юбилейное</p>
                                                <p>«Зимнее ассорти» и отправляйте открытки с теплыми пожеланиями.</p>
                                            </div>
                                            <div class="btn-wrap btn-wrap_bigMargin">
                                                <a href="#" class="btn btn_def js-popup-button" data-popupShow="send-card">Зарегистрировать код</a>
                                            </div>
                                            <div class="text text_little">
                                                <p>Код можно найти на внутренней стороне пачек</p>
                                                <p>печенья Юбилейное «Зимнее ассорти»</p>
                                            </div>
                                            <div class="img-code">
                                                <img src="{{ asset('/public/frontend/img/code-in-wrapper.png') }}" alt="">
                                            </div>
                                        </div>
                                    @else
                                        <div class="checks-yes">
                                            <div class="text text_big">
                                                <p>Регистрируйте коды с пачек печенья Юбилейное</p>
                                                <p>«Зимнее ассорти» и отправляйте открытки с теплыми пожеланиями.</p>
                                            </div>
                                            <div class="personal-table">
                                                <table class="table table_codes" id="table-code">
                                                    <thead class="heading-codes">
                                                    <tr>
                                                        <th>Код</th>
                                                        <th class="date-reg">Дата регистрации</th>
                                                        <th>Мои открытки</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach(Auth::guard('customer')->user()->codes as $code)
                                                        <tr class="card-text-wrap">
                                                            <td class="personal-code">{{ $code->code_name }}</td>
                                                            <td class="personal-date">{{ date('d-m-Y', strtotime($code->created_at)) }}</td>
                                                            <td class="get-card-number">
                                                                @if($code->status_1 == 'Ожидает заявки')
                                                                    <a href="#" class="btn btn_min btn_middle btn-code js-popup-button" data-popupShow="send-card-slider">
                                                                        Отправить открытку
                                                                    </a>
                                                                @else
                                                                    Открытка отправлена
                                                                @endif
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="btn-wrap btn-wrap_bigMargin">
                                                <a href="#" class="btn btn_def js-popup-button" data-popupShow="send-card">Зарегистрировать код</a>
                                            </div>
                                         </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </main>
        </div>
    </div>


    @endsection