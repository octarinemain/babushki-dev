@extends('frontend.includes.header')


@section('content')
    <div class="height-helper height-helper_new-bg">
        <div class="content-wrap">
            <!--Main-->
            <main class="main main_new-bg">
                <section class="feedback">
                    <div class="container container_little">
                        <div class="title title_big">
                            <span>Отписка от рассылок</span>
                        </div>
                        <div class="substrate">
                                <form class="form-wrap unsubscribe" >
                                    {{ csrf_field() }}
                                    <input hidden name="email" value="{{ $email }}" >
                                    <input hidden name="id" value="{{ $id }}" >
                                <div class="form flex-cont justify-sb">
                                    <div class="form__globalCard">
                                        <div class="ajax-validate-error">
                                        </div>
                                        <div class="form__card">
                                            <label class="checkbox">
                                                <input type="checkbox" name="rules" required/>
                                                <div class="checkbox__text">Я ознакомлен с <a target="_blank" href="{{ asset('/public/Правила Акции Пр 45 0401_на сайт.pdf') }}">Правила акции</a> и способами связи со мной Оператором Акции.</div>
                                            </label>
                                        </div>

                                    </div>
                                </div>
                                <div class="btn-wrap btn-wrap_bigMargin">
                                    <button type="submit" class="btn btn_def ">Отписаться</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </section>
            </main>
        </div>
    </div>

@endsection

