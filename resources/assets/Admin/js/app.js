require('./bootstrap');

window.Vue = require('vue');
import VueResource from 'vue-resource';
Vue.use(VueResource);
Vue.use(require('vue-moment'));
Vue.component('login', require('./components/Login.vue'));
Vue.component('dashboard', require('./components/dashboard.vue'));
Vue.component('admin-menu', require('./components/layouts/Menu.vue'));
Vue.component('customer', require('./components/customer.vue'));
Vue.component('all-check', require('./components/AllCheck.vue'));
Vue.component('net-check', require('./components/NetCheck.vue'));
Vue.component('all-code', require('./components/Code.vue'));
Vue.component('thank', require('./components/Thank.vue'));


Vue.http.headers.common['X-CSRF-TOKEN'] = $("meta[name=token]").attr("value");

const app = new Vue({
    el: '#app'
});
