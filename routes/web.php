<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Auth::routes();


//123

Route::get('/', function () {
    if(Auth::guard('customer')->check() && Auth::guard('customer')->user()->winner == 1 && Auth::guard('customer')->user()->is_show == 0){
        $show_winner = true;
        App\Models\Customer::where('id', Auth::guard('customer')->user()->id)->update([
            'is_show' => 1
        ]);
        return view('frontend.pages.home')->with(compact(['show_winner']));
    }else{
        return view('frontend.pages.home');
    }
})->name('home');

Route::get('/grandmothers', function () {
    $thanks = App\Models\Thank::orderby('id', 'DESC')->where('status', 'Подтверждено')->get();
    if(Auth::guard('customer')->check() && Auth::guard('customer')->user()->winner == 1 && Auth::guard('customer')->user()->is_show == 0){
        $show_winner = true;
        App\Models\Customer::where('id', Auth::guard('customer')->user()->id)->update([
            'is_show' => 1
        ]);
        return view('frontend.pages.grandmother')->with(compact(['thanks','show_winner']));
    }else{
        return view('frontend.pages.grandmother')->with(compact(['thanks', 'show_winner']));
    }
});
Route::get('/presents', function () {
    if(Auth::guard('customer')->check() && Auth::guard('customer')->user()->winner == 1 && Auth::guard('customer')->user()->is_show == 0){
        $show_winner = true;
        App\Models\Customer::where('id', Auth::guard('customer')->user()->id)->update([
            'is_show' => 1
        ]);
        return view('frontend.pages.present')->with(compact(['show_winner']));
    }else{
        return view('frontend.pages.present');
    }
});
Route::get('/products', function () {
    if(Auth::guard('customer')->check() && Auth::guard('customer')->user()->winner == 1 && Auth::guard('customer')->user()->is_show == 0){
        $show_winner = true;
        App\Models\Customer::where('id', Auth::guard('customer')->user()->id)->update([
            'is_show' => 1
        ]);
        return view('frontend.pages.product')->with(compact(['show_winner']));
    }else{
        return view('frontend.pages.product');
    }
});
Route::get('/winners', 'Frontend\CheckController@winner');
Route::get('/optimizeImage', 'Frontend\CheckController@optimizeImage');
Route::get('/contact', function () {
    if(Auth::guard('customer')->check() && Auth::guard('customer')->user()->winner == 1 && Auth::guard('customer')->user()->is_show == 0){
        $show_winner = true;
        App\Models\Customer::where('id', Auth::guard('customer')->user()->id)->update([
            'is_show' => 1
        ]);
        return view('frontend.pages.contact')->with(compact(['show_winner']));
    }else{
        return view('frontend.pages.contact');
    }
});


Route::post('/login', 'Auth\CustomerLoginController@login')->name('customer.login.submit');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');


Route::post('/customer/password/email', 'Auth\CustomerForgotPasswordController@sendResetLinkEmail')->name('customer.password.email');
Route::post('/customer/password/reset', 'Auth\CustomerResetPasswordController@reset');
Route::get('/customer/password/reset/{token}', 'Auth\CustomerResetPasswordController@showResetForm')->name('password.reset');

Route::post('/register-1', 'Frontend\CustomerController@register_step_1');
Route::post('refreshcaptcha', 'Frontend\CustomerController@refreshcaptcha');
Route::post('/register-2', 'Frontend\CustomerController@register_step_2');
Route::post('/register-3', 'Frontend\CustomerController@register_step_3');
Route::get('/customer/confirm={token}', 'Frontend\CustomerController@checkToken');
Route::post('/contact/create', 'Frontend\CustomerController@sendMessage');



Route::get('/load-codes', 'Frontend\CustomerController@createCode');
//Route::get('/test', 'Frontend\CheckController@test');

/*
 * Checks
 */
Route::post('/thank/create', 'Frontend\ThankController@create');

Route::get('/customer/cancel-email-notifications/id={id}&email={email}', function ($id,$email) {
    return view('frontend.customer.unsubscribe')->with(compact(['id', 'email']));
});
Route::post('/customer/unsubscribe', 'Frontend\CustomerController@unsubscribe');

Route::group(['middleware' => ['customer']], function () {

    Route::prefix('customer')->group(function () {

        Route::get('/profile', function () {
            if(Auth::guard('customer')->user()->winner == 1 && Auth::guard('customer')->user()->is_show == 0){
                $show_winner = true;
                App\Models\Customer::where('id', Auth::guard('customer')->user()->id)->update([
                    'is_show' => 1
                ]);
                return view('frontend.customer.profile')->with(compact(['show_winner']));
            }else{
                return view('frontend.customer.profile');
            }
        });



        /*
         * Checks
         */
        Route::post('/check/create', 'Frontend\CheckController@create');
        Route::post('/check/getPrize', 'Frontend\CheckController@getPrize');
        Route::post('/upload', 'Frontend\CheckController@upload');
        Route::post('/delete/photo', 'Frontend\CheckController@deletePhoto');

        /*
         * Codes
         */
        Route::post('/code/check', 'Frontend\CodeController@check');
        Route::post('/code/getCard', 'Frontend\CodeController@getCard');



    });

});


/*
 * Admin routes
 */
Route::prefix('admin')->group(function () {

    Route::get('/', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/', 'Auth\AdminLoginController@login')->name('admin.login.submit');

    Route::group(['middleware' => ['admin']], function () {
    /*
     * Customer
     */
    Route::get('/customers', function () {
        return view('admin.customers');
    });
    Route::post('/customers', function () {
        $customers = App\Models\Customer::orderby('id', 'DESC')->get();
        return $customers;
    });
    Route::post('/customer/create', 'Admin\CustomerController@create');
        Route::get('/download/customers', 'Admin\CustomerController@doownloadFile');



        Route::get('/dashboard', function () {
            return view('admin.dashboard');
        })->name('admin.dashboard');

        /*
         * Checks
         */
        Route::post('/all-checks', function () {
            $checks = App\Models\Check::orderby('id', 'DESC')->take(500)->get();
            $checks->load('photo');
            $checks->load('customer');
            return $checks;
        });
        Route::post('/net-checks', function () {
            $checks = App\Models\Check::orderby('id', 'DESC')->where('status_1', 'Ожидает заявки')->get();
            $checks->load('customer');
            return $checks;
        });
//        Route::post('/x5-checks', function () {
//            $checks = App\Models\Check::orderby('id', 'DESC')->where('net', 'x5')->where('status_1', 'Ожидает заявки')->get();
//            $checks->load('customer');
//            return $checks;
//        });
        Route::post('/skus', function () {
            $skus = App\Models\Sku::orderby('id', 'DESC')->get();
            return $skus;
        });
        Route::get('/all-checks', function () {
            return view('admin.all-check');
        });
        Route::get('/net-checks', function () {
            return view('admin.net-check');
        });
        Route::post('/check/update', 'Admin\CheckController@update');
        Route::post('/check/filter', 'Admin\CheckController@filter');
        Route::post('/check/export', 'Admin\CheckController@export');
        Route::get('/download/checks', 'Admin\CheckController@doownloadFile');
        Route::get('/download/win-checks', 'Admin\CheckController@downloadWinners');
        Route::post('/check/update_wins', 'Admin\CheckController@update_wins');

        /*
         * Codes
         */
        Route::post('/codes', function () {
            $codes = App\Models\NewCode::orderby('id', 'DESC')->take(500)->get();
            $codes->load('customer');
            return $codes;
        });
        Route::get('/codes', function () {
            return view('admin.code');
        });
        Route::post('/code/update', 'Admin\CodeController@update');
        Route::get('/download/codes', 'Admin\CodeController@doownloadFile');



        /*
         * Thanks
         */
        Route::post('/thanks', function () {
            $thanks = App\Models\Thank::orderby('id', 'DESC')->get();
            return $thanks;
        });
        Route::get('/thanks', function () {
            return view('admin.thank');
        });
        Route::post('/thank/update', 'Admin\ThankController@update');
        Route::post('/thank/delete/id={id}', 'Admin\ThankController@delete');
        Route::get('/download/thanks', 'Admin\ThankController@doownloadFile');




    });
});


