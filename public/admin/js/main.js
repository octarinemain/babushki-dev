jQuery(document).ready(function($) {
  //-------------main-menu-------------
  var menuButton = document.querySelector('.menu-button');
  var pageConent = document.querySelector('.page-conent');
  var swiperMenu = new Swiper('.main-menu', {
    slidesPerView: 'auto',
    initialSlide: 0,
    resistanceRatio: 0,
    shortSwipes: false,
    on: {
      init: function () {
        var slider = this;
        menuButton.addEventListener('click', function () {
          if (slider.activeIndex === 0) {
            slider.slideNext();
          } else {
            slider.slidePrev();
          }
        }, true);
      },
      slideChange: function () {
        var slider = this;
        if (slider.activeIndex === 0) {
          menuButton.classList.add('cross');
          pageConent.classList.add('page-conent__open-menu');
        } else {
          menuButton.classList.remove('cross');
          pageConent.classList.remove('page-conent__open-menu');
        }
      },
    }
  });
    $('.main-menu').on('click', '.sub-menu', function(event) {
        event.preventDefault();
        $(this).closest('.main-menu__item').toggleClass('main-menu__item--has-unhide-sub-menu');
    });
  //-------------datepicker for input-------------
  $('input.datepicker').datepicker({ dateFormat: 'dd-mm-yy' });
  //-------------table header checkbox-------------
  if ($(".table").is(".table--checkboxes")) {
    $(".table--checkboxes thead input:checkbox").change(function(){
      checkboxColumnNum = $(this).closest('td').index()+1;
      if(this.checked){
          $(this).closest('table').find('tr td:nth-child('+checkboxColumnNum+') input:checkbox').prop('checked', true);
      }else{
          $(this).closest('table').find('tr td:nth-child('+checkboxColumnNum+') input:checkbox').prop('checked', false);
      }
    });
  }
  //-------------tabs init-------------
  if($("div").is(".tab-block")){
    var activeSlide = 0;
    swiperMenu.on('transitionEnd', function () {
      tabsSlider();
    });
    swiperMenu.on('touchEnd', function () {
      tabsSlider();
    });
    tabsSlider();
    function tabsSlider(){
      function setCurrentSlide(ele,index){
        $(".tab-block__header .swiper-slide").removeClass("selected");
        ele.addClass("selected");
      }
      var tabsControl = new Swiper('.tab-block__header', {
          slidesPerView: 5,
          paginationClickable: true,
          spaceBetween: 8,
          freeMode: true,
          loop: false,
          onTab:function(swiper){
            var n = tabsControl.clickedIndex;
          }
      });
      tabsControl.slides.each(function(index,val){
        var ele=$(this);
        ele.on("click",function(){
          setCurrentSlide(ele,index);
          tabsContent.slideTo(index, 500, false);
          activeSlide = tabsContent.activeIndex;
        });
      });
      var tabsContent = new Swiper ('.tab-block__tabs-conteiner', {
          direction: 'horizontal',
          loop: false,
          slidesPerView: 1,
          spaceBetween: 40,
          touchRatio: 0,
          effect: 'coverflow',
          autoHeight: true,
        });
        $(".tab-block__header .swiper-slide").eq(activeSlide).addClass('selected');
        tabsControl.slideTo(activeSlide);
        tabsContent.slideTo(activeSlide);
        tabsContent.on('transitionEnd', function () {
        if (($('form').is('.tab-block__tab-content.swiper-slide-active form')) && (!($('input[type="submit"]').is('.tab-block__tab-content.swiper-slide-active form input')))) {
            $('.btn--get-tab-form').show(100);
          } else{
            $('.btn--get-tab-form').hide(100);
          }
        });
        $('.tab-block').css('opacity', '1');
        if (($('form').is('.tab-block__tab-content.swiper-slide-active form')) && (!($('input[type="submit"]').is('.tab-block__tab-content.swiper-slide-active form input')))) {
          $('.btn--get-tab-form').show(100);
        } else{
          $('.btn--get-tab-form').hide(100);
        }
    }


    $('.info-header__page-actions ').on('click', '.btn--get-tab-form', function(event) {
      event.preventDefault();
      $('.tab-block__tab-content.swiper-slide-active form').trigger('submit');
    });
  }
  //-------------alternative tabs init-------------

  if($("div").is(".tab-used")){
    $('.tabs').on('click', '.tabs__tab', function(event) {
      $('.tab-used').removeClass('tab-used--active');
      $('.tabs__tab').removeClass('tabs__tab--active');
      $(this).addClass('tabs__tab--active');
      $('.tab-used').eq($(this).index()).addClass('tab-used--active');
      console.log($(this).index());
    });
  }
  var file_api = ( window.File && window.FileReader && window.FileList && window.Blob ) ? true : false;
  $('.input-img-band').find('input').on('change', function(){
    var file_name = 0;
    if( file_api && $(this).closest('.input-img-band').find('input')[ 0 ].files[ 0 ] ) {
            file_name = $(this).closest('.input-img-band').find('input')[ 0 ].files[ 0 ].name;
            $(this).closest('.input-img-band').find('button').css('background-color','#d7f0fc');
    } else {
        file_name = $(this).closest('.input-img-band').find('input').val().replace( "C:\\fakepath\\", '' );
    }
    if( ! file_name.length ){
        return;
    }

    if( $(this).closest('.input-img-band').find('.input-img-band__input').is( ":visible" ) ){
        $(this).closest('.input-img-band').find('.input-img-band__file-name').text( file_name );
    }else{
        $(this).closest('.input-img-band').find('button').text( file_name );
    }
  });
  $('.popup').on('click', '.popup__close', function(event) {
    console.log('closaisa suka');
    $(this).closest('.popup').removeClass('popup--active');
  });
  // $('.popup').on('click', function(event) {
  //   if (!$(this).find('.popup__wrap').is(":hover")) {
  //     $(this).removeClass('popup--active');
  //   }
  // });
  $('body').on('click', '.open-popup-btn', function(event) {
    $($(this).attr('data-popup')).addClass('popup--active');
  });
  $('.open-child-popup').on('click', function(event) {
    $('.table-popups .popup').eq($(this).closest('tr').index()).addClass('popup--active')
  });
});


