$(document).ready(function() {
    // Jquery Validate
    $('form').each(function() {
        $(this).validate({
            ignore: [],
            errorClass: "error",
            validClass: "success",
            errorElement: "div",
            wrapper: "span",
            rules: {
                name: {
                    required: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                surname: {
                    required: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                second_name: {
                    required: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                city: {
                    required: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                phone: {
                    required: true,
                    phone: true
                },
                email: {
                    required: true,
                    email: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                password: {
                    required: true
                },
                personalAgreement: {
                    required: true
                },
                rules: {
                    required: true
                },
                captcha: {
                    required: true
                },
                from_where: {
                    required: true
                },
                whom: {
                    required: true
                },
                reg_index: {
                    required: true
                },
                region: {
                    required: true
                },
                street: {
                    required: true
                },
                house: {
                    required: true
                },
                flat: {
                    required: true
                },
                size: {
                    required: true
                },
                code_name: {
                    required: true
                },
                question: {
                    required: true
                }
            },
            messages: {
                phone: {
                    phone: "Укажите номер телефона"
                },
                email: {
                    email: "Введите правильный email"
                }
            }
        });

        jQuery.validator.addMethod("phone", function(value, element) {
            return this.optional(element) || /\+7\(\d+\)\d{3}-\d{2}-\d{2}/.test(value);
        });
    });

    /*
    Reg form step 1
     */

    $('.register-step-1').on('submit',function(e) {
        e.preventDefault();
        var formData = new FormData($(this)[0]);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '/register-1',
            type: 'POST',
            contentType: false,
            processData: false,
            data: formData,
            success: function (data) {
                $('.register-step-1').closest('.popup').removeClass('js-popup-show');
                $('.register-step-2').closest('.popup').addClass('js-popup-show');
            },
            error: function (data) {
                var errors = data.responseJSON;
                $('.register-step-1 .ajax-validate-error').html('');
                $.each(errors.errors, function (index, value) {
                    $('.register-step-1 .ajax-validate-error').append('<span><div class="error">' + value + '</div></span>');
                });
            }
        });

    });

    $('.register-step-2').on('submit',function(e) {
        e.preventDefault();
        var formData = new FormData($(this)[0]);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '/register-2',
            type: 'POST',
            contentType: false,
            processData: false,
            data: formData,
            success: function (data) {
                $('.register-step-2').closest('.popup').removeClass('js-popup-show');
                $('.register-step-3').closest('.popup').addClass('js-popup-show');
                $.ajax({
                    type:'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: 'POST',
                    contentType: false,
                    processData: false,
                    url:'register-3',
                    success:function(data){

                    }
                });
            },
            error: function (data) {
                var errors = data.responseJSON;
                $.ajax({
                    type:'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: 'POST',
                    contentType: false,
                    processData: false,
                    url:'refreshcaptcha',
                    success:function(data){
                        $(".captcha span").html(data.captcha);
                        $(".captcha__input").val('');
                    }
                });
                $('.register-step-2 .ajax-validate-error').html('');
                $.each(errors.errors, function (index, value) {
                    $('.register-step-2 .ajax-validate-error').append('<span><div class="error">' + value + '</div></span>');
                });
            }
        });

    });
    $('.login').on('submit',function(e) {
        e.preventDefault();
        var formData = new FormData($(this)[0]);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '/login',
            type: 'POST',
            contentType: false,
            processData: false,
            data: formData,
            success: function (data) {
                window.location = data;
            },
            error: function (data) {
                var errors = data.responseJSON;
                $('.login .ajax-validate-error').html('');
                $.each(errors.errors, function (index, value) {
                    $('.login .ajax-validate-error').append('<span><div class="error">' + value + '</div></span>');
                });
            }
        });

    });

    $('.reset-step-1').on('submit',function(e) {
        e.preventDefault();
        var formData = new FormData($(this)[0]);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '/customer/password/email',
            type: 'POST',
            contentType: false,
            processData: false,
            data: formData,
            success: function (data) {
                $('.reset-step-1').closest('.popup').removeClass('js-popup-show');
                $('.reset-step-2').closest('.popup').addClass('js-popup-show');
            },
            error: function (data) {
                var errors = data.responseJSON;
                $('.reset-step-1 .ajax-validate-error').html('');
                $.each(errors.errors, function (index, value) {
                    $('.reset-step-1 .ajax-validate-error').append('<span><div class="error">' + value + '</div></span>');
                });
            }
        });

    });

    $('.reset-step-3').on('submit',function(e) {
        e.preventDefault();
        var formData = new FormData($(this)[0]);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '/customer/password/reset',
            type: 'POST',
            contentType: false,
            processData: false,
            data: formData,
            success: function (data) {
                window.location = data;
            },
            error: function (data) {
                var errors = data.responseJSON;
                $('.reset-step-3 .ajax-validate-error').html('');
                $.each(errors.errors, function (index, value) {
                    $('.reset-step-3 .ajax-validate-error').append('<span><div class="error">' + value + '</div></span>');
                });
            }
        });

    });


    $('.upload-check').on('submit',function(e) {
        $('#file-photo-check').removeAttr('name');
        e.preventDefault();
        var formData = new FormData($(this)[0]);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '/customer/check/create',
            type: 'POST',
            contentType: false,
            processData: false,
            data: formData,
            beforeSend: function(){
                $('.preloader').addClass('preloader_active');
            },
            success: function (data) {
                $('.preloader').removeClass('preloader_active');
                $('.upload-check').closest('.popup').removeClass('js-popup-show');
                $('.download-check-end').addClass('js-popup-show');
                $('#file-photo-check').attr('name', 'image[]');

            },
            error: function (data) {
                $('.preloader').removeClass('preloader_active');
                var errors = data.responseJSON;
                $('.upload-check .ajax-validate-error').html('');
                $.each(errors.errors, function (index, value) {
                    $('.upload-check .ajax-validate-error').append('<span><div class="error">' + value + '</div></span>');
                });
                $('#file-photo-check').attr('name', 'image[]');

            }
        });

    });

    $('.get-prize').on('submit',function(e) {
        e.preventDefault();
        var formData = new FormData($(this)[0]);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '/customer/check/getPrize',
            type: 'POST',
            contentType: false,
            processData: false,
            data: formData,
            success: function (data) {
                $('.get-prize').closest('.popup').removeClass('js-popup-show');
                $('.send-prize').addClass('js-popup-show');
            },
            error: function (data) {
                var errors = data.responseJSON;
                $('.upload-check .ajax-validate-error').html('');
                $.each(errors.errors, function (index, value) {
                    $('.upload-check .ajax-validate-error').append('<span><div class="error">' + value + '</div></span>');
                });
            }
        });

    });

    $('.send-code').on('submit',function(e) {
        e.preventDefault();
        var formData = new FormData($(this)[0]);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '/customer/code/check',
            type: 'POST',
            contentType: false,
            processData: false,
            data: formData,
            success: function (data) {
                $('.code-status').text('');
                $('.code-status-text').text('');
                $('.code-status').text('Отправить открытку');
                $('.code-status-text').text('Регистрируйте коды с пачек печенья Юбилейное «Зимнее ассорти», чтобы отправлять друзьям и родным открытки с теплыми пожеланиями.');
                $('.send-code').closest('.popup').removeClass('js-popup-show');
                $('.yes-reg-card ').addClass('js-popup-show');
            },
            error: function (data) {
                var errors = data.responseJSON;
                $('.code-status').text('');
                $('.code-status-text').text('');
                $('.code-status').text('Код отклонён');
                $('.code-status-text').text('Кажется, ваш код не подходит. Возможно, вы ошиблись при наборе. Попробуйте еще раз.');
            }
        });

    });

    $('.get-card').on('submit',function(e) {
        e.preventDefault();
        var formData = new FormData($(this)[0]);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '/customer/code/getCard',
            type: 'POST',
            contentType: false,
            processData: false,
            data: formData,
            success: function (data) {
                $('.get-card').closest('.popup').removeClass('js-popup-show');
                $('.yes-send-card-slider').addClass('js-popup-show');
            },
            error: function (data) {

            }
        });

    });

    $('.send-grandmother-thank').on('submit',function(e) {
        e.preventDefault();
        var $form = $('.send-grandmother-thank');
        var $inputs = $('input[type="file"]:not([disabled])', $form);
        $inputs.each(function(_, input) {
            if (input.files.length > 0) return;
            $(input).prop('disabled', true)
        });
        var formData = new FormData($form[0])
        $inputs.prop('disabled', false);
        // var formData = new FormData($(this)[0]);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '/thank/create',
            type: 'POST',
            contentType: false,
            processData: false,
            data: formData,
            beforeSend: function(){
                $('.preloader').addClass('preloader_active');
            },
            success: function (data) {
                $('.preloader').removeClass('preloader_active');
                $('.send-grandmother-thank').closest('.popup').removeClass('js-popup-show');
            },
            error: function (data) {
                $('.preloader').removeClass('preloader_active');
            }
        });

    });

    $('.contact-form').on('submit',function(e) {
        e.preventDefault();
        var formData = new FormData($(this)[0]);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '/contact/create',
            type: 'POST',
            contentType: false,
            processData: false,
            data: formData,
            success: function (data) {
                $('.contact-thank-form').addClass('js-popup-show');
                $("#contact-message").val('');
                $("#contact-email").val('');
                $("#contact-name").val('');
                $(".captcha__input").val('');

            },
            error: function (data) {
                var errors = data.responseJSON;
                $.ajax({
                    type:'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: 'POST',
                    contentType: false,
                    processData: false,
                    url:'refreshcaptcha',
                    success:function(data){
                        $(".contact-form .captcha span").html(data.captcha);
                        $(".captcha__input").val('');
                    }
                });
                $('.contact-form .ajax-validate-error').html('');
                $.each(errors.errors, function (index, value) {
                    $('.contact-form .ajax-validate-error').append('<span><div class="error">' + value + '</div></span>');
                });
            }
        });

    });

    $('.unsubscribe').on('submit',function(e) {
        e.preventDefault();
        var formData = new FormData($(this)[0]);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '/customer/unsubscribe',
            type: 'POST',
            contentType: false,
            processData: false,
            data: formData,
            success: function (data) {
                window.location = '/customer/profile'
            },
            error: function (data) {
                var errors = data.responseJSON;
                $('.contact-form .ajax-validate-error').html('');
                $.each(errors.errors, function (index, value) {
                    $('.contact-form .ajax-validate-error').append('<span><div class="error">' + value + '</div></span>');
                });
            }
        });

    });

    $('#search-winners').on('keyup', function(){
        var searchVal = $(this).val();
        $('.winners__table').find('tr.search-tr').each(function() {
            currenVal = $(this).find('.win-number').text();
            result = currenVal.indexOf(searchVal);
            if(result == -1){
                $(this).hide();
            }else{
                $(this).show();
            }
        });
    });

    // Btn-Disable
    function btnDis() {
        var popup = $('.popup');

        popup.each(function () {
            var validateI = $(this).find('input');
            var validateT = $(this).find('textarea');
            var validateForm = $(this).find('form');
            var btn = $(this).find('.btn');

            validateI.on('blur keyup click', function () {
                var checked = $('input:checked').length;
                if (validateForm.valid() && checked) {
                    btn.prop('disabled', false).removeClass('btn_disable');
                }
                if (validateForm.valid()) {
                    btn.prop('disabled', false).removeClass('btn_disable');
                } else {
                    btn.prop('disabled', 'disabled').addClass('btn_disable');
                }
            });

            validateT.on('blur keyup click', function () {
                if (validateForm.valid()) {
                    btn.prop('disabled', false).removeClass('btn_disable');
                } else {
                    btn.prop('disabled', 'disabled').addClass('btn_disable');
                }
            });
        });
    };
    btnDis();

    // Masked Phone
    $("input[type='tel']").mask("+7(999)999-99-99");

});