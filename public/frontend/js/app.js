'use strict';

$(document).ready(function() {

    // Home Slider
    var homeSwipper = new Swiper('.conditions__content', {
        slidesPerView: 3,
        allowTouchMove: false,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev'
        },
        breakpoints: {
            992: {
                slidesPerView: 1,
                allowTouchMove: true,
                spaceBetween: 60
            },
            480: {
                slidesPerView: 1,
                allowTouchMove: true,
                spaceBetween: 80
            }
        }
    });

    // Rules Swiper
    var productSwiper = new Swiper('.product-slider', {
        slidesPerView: 3,
        spaceBetween: 25,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev'
        },
        breakpoints: {
            768: {
                slidesPerView: 2,
                spaceBetween: 40
            },
            480: {
                slidesPerView: 1,
                spaceBetween: 40
            }
        }
    });

    // Cookies
    function cookies() {
		var stock = $('.stock');
		var data = sessionStorage.getItem('stock');
		if (data === null) {
			stock.addClass('js-show');
		}

		$('.stock .stock__close').on('click', function(e) {
			e.preventDefault();
			stock.removeClass('js-show');
			sessionStorage.setItem('stock', 'ok');
		
			var cookies = $('.cookies');
			var data = sessionStorage.getItem('cookie');
			if (data === null) {
				cookies.addClass('js-show');
			}

			$('.cookies .btn').on('click', function(e) {
				e.preventDefault();
				cookies.removeClass('js-show');
				sessionStorage.setItem('cookie', 'ok');
			});
		});
    }
    cookies();

    // PopUp
    function popUp() {
        $('.js-popup-button').on('click', function() {
            $('.popup').removeClass('js-popup-show');
            var popupClass = '.' + $(this).attr('data-popupShow');
            $(popupClass).addClass('js-popup-show');
            $('body').addClass('no-scroll');
        });
        closePopup();
    }

    // Close PopUp
    function closePopup() {
        $('.js-close-popup').on('click', function() {
            $('.popup').removeClass('js-popup-show');
            $('body').removeClass('no-scroll');
        });

        $('.popup').on('click', function(e) {
            var div = $(".popup__wrap");
            if (!div.is(e.target) && div.has(e.target).length === 0) {
                $('.popup').removeClass('js-popup-show');
                $('body').removeClass('no-scroll');
            }
        });
    }
    popUp();

    // Select
    $('select').select2();

    // Scroll
    $('.winners__table').scrollbar();
    $('.personal-table').scrollbar();
    $('.text-download-wrap').scrollbar();
    $('.photo-download-wrap').scrollbar();

    // Tabs
    function tabs() {
        var tabsContent = $('.content-tabs-wrap .content-tabs');

        // Reset
        $('.nav-tabs li').eq(0).addClass('active');
        tabsContent.eq(0).addClass('active');

        // Tabs Main Action
        $('.nav-tabs').on('click', 'li', function() {
            var i = $(this).index();
            $('.nav-tabs li').removeClass('active');
            $(this).addClass('active');
            tabsContent.removeClass('active');
            tabsContent.eq(i).addClass('active');
        });
    }
    tabs();

    // Burger Menu
    function toggleMobMenu() {
        $('.header__burger').on('click', function() {
            $(this).toggleClass('active');
            $('.nav').toggleClass('active');

            $(document).on('click', function(e) {
                var div = $(".nav, .header__burger");
                if (!div.is(e.target) && div.has(e.target).length === 0) {
                    $('.header__burger').removeClass('active');
                    $('.nav').removeClass('active');
                }
            });
            return false;
        });
    }
    toggleMobMenu();

    // Download-check


        function readURL(input) {
            console.log('123123');

        }

    // $('#download-photo').on('change', '#file-photo-check', function() {
    //
    // });

    $('#file-photo-check').fileupload({
        dataType: 'json',
        add: function (e, data) {
            $('.preloader').addClass('preloader_active');
            data.submit();
        },
        done: function (e, data) {

            $.each(data.result.files, function (index, file) {
                var wrapDownload = $('#download-wrap');
                var btnDownload = $('#photo-download');
                var newBlock = $('<div>').attr('class', 'photo-download-wrap__check');
                var delImg = $('<div>').attr('class', 'photo-del').appendTo(newBlock);
                var img = $('<img>').attr({
                    'src': '/public/storage/Check/'+file.name,
                    'class': 'class-for-deleting',
                    'alt': 'photo-check',
                    'data-attr-photo-id': file.fileID
                }).appendTo(newBlock);

                wrapDownload.append(newBlock);
                var new_file = $('#file-photo-check').prop('files');

                var maxNewBlock = $('#download-wrap').find('.photo-download-wrap__check').length;
                if (maxNewBlock > 4) {
                    btnDownload.addClass('file-photo_none');
                }
                $('#photo_ids').val($('#photo_ids').val() + file.fileID+',');
            });
            $('.preloader').removeClass('preloader_active');
        },
        error: function(data){
            var errors = data.responseJSON;
            $('.upload-check .ajax-validate-error').html('');
            $.each(errors.errors, function (index, value) {
                $('.upload-check .ajax-validate-error').append('<span><div class="error">' + value + '</div></span>');
            });
            $('.preloader').removeClass('preloader_active');
        }
    });

        //Delete photo check
    $('body').on('click', '.photo-del',function() {
        $('.preloader').addClass('preloader_active');
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '/customer/delete/photo',
            type: 'POST',
            data: 'id=' + $(this).next('.class-for-deleting').attr('data-attr-photo-id'),
            context: this,
            success: function (data) {
                $(this).parent().remove();
                var str = $('#photo_ids').val();
                var res = str.replace($(this).next('.class-for-deleting').attr('data-attr-photo-id')+',', "");
                $('#photo_ids').val(res);

                var maxNewBlock = $('#download-wrap').find('.photo-download-wrap__check').length;
                if (maxNewBlock <= 4) {
                    $('#photo-download').removeClass('file-photo_none');
                }else{
                    $('#photo-download').addClass('file-photo_none');
                }
                $('.preloader').removeClass('preloader_active');
            }
        });

    });


    // Find Personal nunber, address
    $(function() {
        var table = $('#table-prize');
        var table_code = $('#table-code');
        var btn = table.find('.btn_middle');
        var btn_wait = table.find('.wait');
        var btn_wait_code = table_code.find('.wait-code');
        var btn_code = table_code.find('.btn-code');
        var input = $('#check_id');
        var input_code = $('#code_name');


        btn.on('click', function() {
            var giveText = $(this).closest('.give-prize').closest('.prize-text-wrap').find('.personal-num').text();
            var prize = $(this).closest('.give-prize').closest('.prize-text-wrap').find('#prize-type').val();
            if(prize == 'Свитер'){
                $('.prize-size').show()
            }else{
                $('.prize-size').hide()
            }
            var textIndex = $(this).closest('.prize-text-wrap').find('.text-index').val();
            var textAddress = $(this).closest('.prize-text-wrap').find('.text-address').val();
            var blockIndex = $('#text-address-wrap').find('p:first-child');
            var blockAddress = $('#text-address-wrap').find('p:last-child');
            blockIndex.text(textIndex);
            blockAddress.text(textAddress);
            input.val(giveText);
        });

        btn_wait.on('click', function() {
            var textIndex = $(this).closest('.prize-text-wrap').find('.text-index').val();
            var textAddress = $(this).closest('.prize-text-wrap').find('.text-address').val();
            var blockIndex = $('#text-address-wrap').find('p:first-child');
            var blockAddress = $('#text-address-wrap').find('p:last-child');
            blockIndex.text(textIndex);
            blockAddress.text(textAddress);
        });

        btn_wait_code.on('click', function() {
            var textIndex = $(this).closest('.card-text-wrap').find('.text-index').val();
            var textAddress = $(this).closest('.card-text-wrap').find('.text-address').val();
            var blockIndex = $('#text-address-card-wrap').find('p:first-child');
            var blockAddress = $('#text-address-card-wrap').find('p:last-child');
            blockIndex.text(textIndex);
            blockAddress.text(textAddress);
        });

        btn_code.on('click', function() {
            var giveText = $(this).closest('.get-card-number').closest('.card-text-wrap').find('.personal-code').text();
            input_code.val(giveText);
        });


    });

    // Where Checks
    $(function() {
        var btn = $('#where-checks');
        var wrap = $('.where-checks-wrap');

        btn.on('click', function() {
            wrap.toggleClass('where-checks-wrap-active');
            return false;
        })
    });

    // Choose slider &  active slide input val
    $(function() {
        var input = $('#select-slider');
        var btn = $('#choose-btn');
        var img = $('#choose-card');
        var chooseSlider = new Swiper('#choose-slider', {
			slidesPerView: 1,
			spaceBetween: 30,
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev'
            },
            pagination: {
                el: '.swiper-pagination',
                type: 'bullets',
                clickable: true
            },
            breakpoints: {
                768: {
                    spaceBetween: 50
                }
            },
            on: {
                slideChange: function() {
                    input.val(chooseSlider.activeIndex);
                    var sliderImg = $('#choose-slider').find('.swiper-slide-active img').attr('src');

                    btn.on('click', function() {
                        img.attr('src', sliderImg);
                    })
                },
            },
        });
        input.val(chooseSlider.activeIndex);

        var sliderImg = $('#choose-slider').find('.swiper-slide-active img').attr('src');

        btn.on('click', function() {
            img.attr('src', sliderImg);
        })
    });

    // Kladrapi
    $(function() {
        var $region = $('[name="region"]'),
            $city = $('[name="city"]'),
            $street = $('[name="street"]');

        $.kladr.setDefault({
            parentInput: '.js-form-address',
            verify: true,
            select: function(obj) {
                setLabel($(this), obj.type);
            },
            check: function(obj) {
                var $input = $(this);

                if (obj) {
                    setLabel($input, obj.type);
                }
            },
            checkBefore: function() {
                var $input = $(this);
            }
        });

        $region.kladr('type', $.kladr.type.region);
        $city.kladr('type', $.kladr.type.city);
        $street.kladr('type', $.kladr.type.street);

        function setLabel($input, text) {
            text = text.charAt(0).toUpperCase() + text.substr(1).toLowerCase();
        }

        var hiddenBlock = $('#kladr_autocomplete').find('.autocomplete');
        var popup = $('.send-gift');

        popup.on('scroll', function() {
            hiddenBlock.css('display', 'none');
        })
    });

    // Slider Grandmothers && auto-height
    $(function() {
        var blockGrandM = $('#grandmothers-slider');
        var btn = blockGrandM.find('.btn_congratulate');
        var popup = $('#gallery-card-add');
        var inputNameGranM = popup.find('.name-grandmother');
        var grandSwiper = new Swiper('#grandmothers-slider', {
            slidesPerView: 3,
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev'
            },
            breakpoints: {
                1200: {
                    slidesPerView: 2
                },
                768: {
                    slidesPerView: 1
                }
            }
        });

        var boxEl = blockGrandM.find('.grandmothers-slider__item');
        var minHeight = 430;

        boxEl.each(function() {
            if ($(this).outerHeight() > minHeight) {
                minHeight = $(this).outerHeight();
                boxEl.css('min-height', minHeight);
            }
        });

        btn.on('click', function() {
            var grandmName = $(this).closest('.grandmothers-slider__item').find('.about-grandmother__name').text();

            inputNameGranM.val(grandmName);
        });
    });

    // Slider-card
    $(function() {
        var galleryPop = $('#gallery-card-slider');
        var whom = galleryPop.find('.title span');
        var from = galleryPop.find('.title p');
        var textCongratulate = galleryPop.find('.form__text');
        var congratulate = $('#congratulate-wrap');
		var btn = congratulate.find('.congratulate-box__all');
		var popImg = galleryPop.find('.slide-card__wrap img');
		var popVideo = galleryPop.find('.slide-card__wrap video');
		var btnIcon = congratulate.find('.icon-btn');
		var cardSlider = new Swiper('.slide-card', {
			slidesPerView: 1,
			navigation: {
				nextEl: '.swiper-button-next',
				prevEl: '.swiper-button-prev'
			},
			breakpoints: {
				768: {
					spaceBetween: 40,
				}
			}
		});
        
        btn.on('click', function() {
            var textWhom = $(this).closest('.congratulate-box').find('.congratulate-box__heading_for').text();
            var textFrom = $(this).closest('.congratulate-box').find('.congratulate-box__heading_from').text();
			var textCon = $(this).closest('.congratulate-box').find('.congratulate-box__text').text();
			var img_url = $(this).closest('.congratulate-box').find('.grand-img-name').val().length;
			var img = $(this).closest('.congratulate-box').find('.grand-img').val();
			var video_url = $(this).closest('.congratulate-box').find('.grand-video-name').val().length;
			var video = $(this).closest('.congratulate-box').find('.grand-video').val();
			var swiperNone = galleryPop.find('.slide-card');
			var slideLineNone = swiperNone.find('.slide-card__wrap');
			var swipperBtn = swiperNone.find('.card-btn');

			if (img_url == 0 && video_url == 0) {
				swiperNone.css('display', 'none').addClass('slide-card_noactive');
				slideLineNone.find('.slide-card__item_video').css('display', 'none');
				slideLineNone.find('.slide-card__item_img').css('display', 'none');
				swipperBtn.css('display', 'none');
			} else if (img_url > 0 && video_url == 0) {
				swiperNone.css('display', 'block').addClass('slide-card_noactive');
				slideLineNone.find('.slide-card__item_img').css('display', 'block');
				cardSlider.slideTo(0); 
				slideLineNone.find('.slide-card__item_video').css('display', 'none');
				swipperBtn.css('display', 'none');
				swiperNone.addClass('slide-card_noactive');
				popImg.attr('src', img);
			} else if (img_url == 0 && video_url > 0) {
				swiperNone.css('display', 'block').removeClass('slide-card_noactive');
				slideLineNone.addClass('slide-card__wrap_noctive');
				slideLineNone.find('.slide-card__item_video').css('display', 'block');
				cardSlider.slideTo(0); 
				slideLineNone.find('.slide-card__item_img').css('display', 'none');
				swipperBtn.css('display', 'none');
				popVideo.attr('src', video);
			} else if (img_url > 0 && video_url > 0) {
				swiperNone.css('display', 'block').removeClass('slide-card_noactive');
				slideLineNone.removeClass('slide-card__wrap_noctive');
				slideLineNone.find('.slide-card__item_img').css('display', 'block');
				slideLineNone.find('.slide-card__item_video').css('display', 'block');
				swipperBtn.css('display', 'block');
				popImg.attr('src', img);
				popVideo.attr('src', video);
			}

			whom.text(textWhom);
			from.text(textFrom);
			textCongratulate.text(textCon);
			return false;
		});	
		
		btnIcon.on('click', function() {
			$(this).closest('.congratulate-box').find('.congratulate-box__all').trigger('click');
		});
    });

    // Show more
    $(function() {
		var wrapBoxGallery = $('#congratulate-wrap');
        var boxGallery = wrapBoxGallery.find('.congratulate-box-wrap');
		var btn = $('#show-more');
		var resizeI;

		// resize function
        function onResize() {
			var width = window.innerWidth;

			if (width >= 1200) {
				resizeI = 2;
			} else {
				resizeI = 1;
			}

			boxGallery.each(function(i) {
				if (i <= resizeI) {
					$(this).addClass('congratulate-box-wrap_active');
				} else {
					$(this).removeClass('congratulate-box-wrap_active');
				}
			});
		};
		
		// review show more
		btn.on('click', function(e) {
			e.preventDefault();
			var activeBox = wrapBoxGallery.find('.congratulate-box-wrap.congratulate-box-wrap_active');
			var lenght = boxGallery.length;
			var lenghtActive = activeBox.length;

			if (lenght > lenghtActive) {
				boxGallery.each(function(i) {
					if (i <= (lenghtActive + resizeI) && !$(this).hasClass('congratulate-box-wrap_active')) {
						$(this).addClass('congratulate-box-wrap_active');
					}
				});
			}

			lenghtActive = wrapBoxGallery.find('.congratulate-box-wrap.congratulate-box-wrap_active').length;

			if (lenght === lenghtActive) {
				btn.hide();
			}
		});
		
        var doit;
        doit = setTimeout(onResize, 400);
        window.onresize = function() {
            clearTimeout(doit);
            doit = setTimeout(onResize, 400);
		};
	});
	
	// Tabs
	$('ul.tabs__caption').on('click', 'li:not(.active)', function() {
		$(this).addClass('active').siblings().removeClass('active').closest('div.tabs').find('div.tabs__content').removeClass('active').eq($(this).index()).addClass('active');
	});

	// Slider HNY
	function initSliderHny() {
		var wrap = $('.newy-wrap');
		new Swiper(wrap, {
			slidesPerView: 1,
			spaceBetween: 20,
			navigation: {
				nextEl: '.swiper-button-next',
				prevEl: '.swiper-button-prev'
			}
		});
	}
	initSliderHny();


});