<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Customer;


class unsetAttemptCount extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'customer:unsetAttemptCount';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Убираем кол-во попыток у юзеров';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       Customer::where('id', '>', 0)->update([
          'attempt_count' => 0
       ]);
    }
}
