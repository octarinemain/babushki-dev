<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Check;
use App\Models\Customer;
use Mail;
use App\Mail\CheckAllPrize;
use App\Mail\CheckPrizex5;



class createWinner extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:createWinner';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Выбираем победителя за сутки по сети x5 и всем сетям';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        if(date('Y') == '2019'){
                $checks = Check::where('status_1', 'Принят')->get();
                foreach($checks as $check){
                    if($check->customer->winner == 1){
                        Check::where('id', $check->id)->update([
                            'status_1' => 'Участвовал в розыгрыше',
                            'date_of_participation' => date('d-m-Y')
                        ]);
                    }
                }
                $random_checks = Check::where('status_1', 'Принят')->get()->toArray();
                $count = count($random_checks);
                $result = $count/24+1;
                $winner = intval(round($result)-1);
                $winner = $random_checks[$winner];
                $random_check = Check::where('id', $winner['id'])->first();
                if($random_check){
                    if($random_check->customer->winner != 1){
                        Check::where('customer_id', $random_check->customer->id)->where('status_1', 'Принят')->update([
                            'status_1' => 'Участвовал в розыгрыше',
                            'date_of_participation' => date('d-m-Y')
                        ]);
                        Check::where('id', $random_check->id)->update([
                            'status_1' => 'Ожидает заявки',
                            'status_2' => 'Ожидает заявки',
                            'date_of_participation' => date('d-m-Y'),
                            'prize' => 'Носки'
                        ]);
                        Check::where('status_1', 'Принят')->update([
                            'status_1' => 'Участвовал в розыгрыше',
                            'date_of_participation' => date('d-m-Y')
                        ]);
                        Customer::where('id', $random_check->customer->id)->update([
                            'winner' => 1
                        ]);
                        if ($random_check->customer->none_mail == 0){
                            Mail::to($random_check->customer->email)->send(new CheckAllPrize($random_check));
                        }
                    }
                }
        }else{

            $prize_day = date('l');
            $checks = Check::where('status_1', 'Принят')->get();
            foreach($checks as $check){
                if($check->customer->winner == 1){
                    Check::where('id', $check->id)->update([
                        'status_1' => 'Участвовал в розыгрыше',
                        'date_of_participation' => date('d-m-Y')
                    ]);
                }
            }
            if($prize_day == 'Monday'){
                $random_checks = Check::where('status_1', 'Принят')->get()->toArray();
                $count = count($random_checks);
                $result = $count/24+1;
                $winner = intval(round($result)-1);
                $winner = $random_checks[$winner];
                $random_check = Check::where('id', $winner['id'])->first();
                if($random_check){
                    if($random_check->customer->winner != 1){
                        Check::where('customer_id', $random_check->customer->id)->where('status_1', 'Принят')->update([
                            'status_1' => 'Участвовал в розыгрыше',
                            'date_of_participation' => date('d-m-Y')
                        ]);
                        Check::where('id', $random_check->id)->update([
                            'status_1' => 'Ожидает заявки',
                            'status_2' => 'Ожидает заявки',
                            'date_of_participation' => date('d-m-Y'),
                            'prize' => 'Носки'
                        ]);
                        Check::where('status_1', 'Принят')->update([
                            'status_1' => 'Участвовал в розыгрыше',
                            'date_of_participation' => date('d-m-Y')
                        ]);
                        Customer::where('id', $random_check->customer->id)->update([
                            'winner' => 1
                        ]);
                        if ($random_check->customer->none_mail == 0){
                            Mail::to($random_check->customer->email)->send(new CheckAllPrize($random_check));
                        }
                    }
                }
            }elseif($prize_day == 'Tuesday'){
                $random_checks = Check::where('status_1', 'Принят')->get()->toArray();
                $count = count($random_checks);
                $result = $count/24+1;
                $winner = intval(round($result)-1);
                $winner = $random_checks[$winner];
                $random_check = Check::where('id', $winner['id'])->first();
                if($random_check){
                    Check::where('customer_id', $random_check->customer->id)->where('status_1', 'Принят')->update([
                        'status_1' => 'Участвовал в розыгрыше',
                        'date_of_participation' => date('d-m-Y')
                    ]);
                    if($random_check->customer->winner != 1){
                        Check::where('id', $random_check->id)->update([
                            'status_1' => 'Ожидает заявки',
                            'status_2' => 'Ожидает заявки',
                            'date_of_participation' => date('d-m-Y'),
                            'prize' => 'Варежки'
                        ]);
                        Check::where('status_1', 'Принят')->update([
                            'status_1' => 'Участвовал в розыгрыше',
                            'date_of_participation' => date('d-m-Y')
                        ]);
                        Customer::where('id', $random_check->customer->id)->update([
                            'winner' => 1
                        ]);
                        if ($random_check->customer->none_mail == 0){
                            Mail::to($random_check->customer->email)->send(new CheckAllPrize($random_check));
                        }
                    }
                }
            }elseif($prize_day == 'Wednesday'){
                $random_checks = Check::where('status_1', 'Принят')->get()->toArray();
                $count = count($random_checks);
                $result = $count/24+1;
                $winner = intval(round($result)-1);
                $winner = $random_checks[$winner];
                $random_check = Check::where('id', $winner['id'])->first();
                if($random_check){
                    if($random_check->customer->winner != 1){
                        Check::where('customer_id', $random_check->customer->id)->where('status_1', 'Принят')->update([
                            'status_1' => 'Участвовал в розыгрыше',
                            'date_of_participation' => date('d-m-Y')
                        ]);
                        Check::where('id', $random_check->id)->update([
                            'status_1' => 'Ожидает заявки',
                            'status_2' => 'Ожидает заявки',
                            'date_of_participation' => date('d-m-Y'),
                            'prize' => 'Шапка'
                        ]);
                        Check::where('status_1', 'Принят')->update([
                            'status_1' => 'Участвовал в розыгрыше',
                            'date_of_participation' => date('d-m-Y')
                        ]);
                        Customer::where('id', $random_check->customer->id)->update([
                            'winner' => 1
                        ]);
                        if ($random_check->customer->none_mail == 0){
                            Mail::to($random_check->customer->email)->send(new CheckAllPrize($random_check));
                        }
                    }
                }
            }elseif($prize_day == 'Thursday'){
                $random_checks = Check::where('status_1', 'Принят')->get()->toArray();
                $count = count($random_checks);
                $result = $count/24+1;
                $winner = intval(round($result)-1);
                $winner = $random_checks[$winner];
                $random_check = Check::where('id', $winner['id'])->first();
                if($random_check){
                    if($random_check->customer->winner != 1){
                        Check::where('customer_id', $random_check->customer->id)->where('status_1', 'Принят')->update([
                            'status_1' => 'Участвовал в розыгрыше',
                            'date_of_participation' => date('d-m-Y')
                        ]);
                        Check::where('id', $random_check->id)->update([
                            'status_1' => 'Ожидает заявки',
                            'status_2' => 'Ожидает заявки',
                            'date_of_participation' => date('d-m-Y'),
                            'prize' => 'Носки'
                        ]);
                        Check::where('status_1', 'Принят')->update([
                            'status_1' => 'Участвовал в розыгрыше',
                            'date_of_participation' => date('d-m-Y')
                        ]);
                        Customer::where('id', $random_check->customer->id)->update([
                            'winner' => 1
                        ]);
                        if ($random_check->customer->none_mail == 0){
                            Mail::to($random_check->customer->email)->send(new CheckAllPrize($random_check));
                        }

                    }
                }
            }elseif($prize_day == 'Friday'){
                $random_checks = Check::where('status_1', 'Принят')->get()->toArray();
                $count = count($random_checks);
                $result = $count/24+1;
                $winner = intval(round($result)-1);
                $winner = $random_checks[$winner];
                $random_check = Check::where('id', $winner['id'])->first();
                if($random_check){
                    Check::where('customer_id', $random_check->customer->id)->where('status_1', 'Принят')->update([
                        'status_1' => 'Участвовал в розыгрыше',
                        'date_of_participation' => date('d-m-Y')
                    ]);
                    if($random_check->customer->winner != 1){
                        Check::where('id', $random_check->id)->update([
                            'status_1' => 'Ожидает заявки',
                            'status_2' => 'Ожидает заявки',
                            'date_of_participation' => date('d-m-Y'),
                            'prize' => 'Варежки'
                        ]);
                        Customer::where('id', $random_check->customer->id)->update([
                            'winner' => 1
                        ]);
                        if ($random_check->customer->none_mail == 0){
                            Mail::to($random_check->customer->email)->send(new CheckAllPrize($random_check));
                        }
                    }
                }
                $random_checks_1 = Check::where('status_1', 'Принят')->get()->toArray();
                $count_1 = count($random_checks_1);
                $result_1 = $count_1/24+1;
                $winner_1 = intval(round($result_1)-1);
                $winner_1 = $random_checks_1[$winner_1];
                $random_check_1 = Check::where('id', $winner_1['id'])->first();
                if($random_check_1){
                    Check::where('customer_id', $random_check_1->customer->id)->where('status_1', 'Принят')->update([
                        'status_1' => 'Участвовал в розыгрыше',
                        'date_of_participation' => date('d-m-Y')
                    ]);
                    if($random_check_1->customer->winner != 1){
                        Check::where('id', $random_check_1->id)->update([
                            'status_1' => 'Ожидает заявки',
                            'status_2' => 'Ожидает заявки',
                            'date_of_participation' => date('d-m-Y'),
                            'prize' => 'Варежки'
                        ]);
                        Customer::where('id', $random_check_1->customer->id)->update([
                            'winner' => 1
                        ]);
                        if ($random_check_1->customer->none_mail == 0){
                            Mail::to($random_check_1->customer->email)->send(new CheckAllPrize($random_check_1));
                        }
                    }
                }
                $random_checks_2 = Check::where('status_1', 'Принят')->get()->toArray();
                $count_2 = count($random_checks_2);
                $result_2 = $count_2/24+1;
                $winner_2 = intval(round($result_2)-1);
                $winner_2 = $random_checks_2[$winner_2];
                $random_check_2 = Check::where('id', $winner_2['id'])->first();
                if($random_check_2){
                    Check::where('customer_id', $random_check_2->customer->id)->where('status_1', 'Принят')->update([
                        'status_1' => 'Участвовал в розыгрыше',
                        'date_of_participation' => date('d-m-Y')
                    ]);
                    if($random_check_2->customer->winner != 1){
                        Check::where('id', $random_check_2->id)->update([
                            'status_1' => 'Ожидает заявки',
                            'status_2' => 'Ожидает заявки',
                            'date_of_participation' => date('d-m-Y'),
                            'prize' => 'Варежки'
                        ]);
                        Customer::where('id', $random_check_2->customer->id)->update([
                            'winner' => 1
                        ]);
                        if ($random_check_2->customer->none_mail == 0){
                            Mail::to($random_check_2->customer->email)->send(new CheckAllPrize($random_check_2));
                        }
                    }
                }
                Check::where('status_1', 'Принят')->update([
                    'status_1' => 'Участвовал в розыгрыше',
                    'date_of_participation' => date('d-m-Y')
                ]);
            }elseif($prize_day == 'Saturday'){
                $random_checks = Check::where('status_1', 'Принят')->get()->toArray();
                $count = count($random_checks);
                $result = $count/24+1;
                $winner = intval(round($result)-1);
                $winner = $random_checks[$winner];
                $random_check = Check::where('id', $winner['id'])->first();
                if($random_check){
                    Check::where('customer_id', $random_check->customer->id)->where('status_1', 'Принят')->update([
                        'status_1' => 'Участвовал в розыгрыше',
                        'date_of_participation' => date('d-m-Y')
                    ]);
                    if($random_check->customer->winner != 1){
                        Check::where('id', $random_check->id)->update([
                            'status_1' => 'Ожидает заявки',
                            'status_2' => 'Ожидает заявки',
                            'date_of_participation' => date('d-m-Y'),
                            'prize' => 'Шапка'
                        ]);
                        Customer::where('id', $random_check->customer->id)->update([
                            'winner' => 1
                        ]);
                        if ($random_check->customer->none_mail == 0){
                            Mail::to($random_check->customer->email)->send(new CheckAllPrize($random_check));
                        }
                    }
                }
                $random_checks_1 = Check::where('status_1', 'Принят')->get()->toArray();
                $count_1 = count($random_checks_1);
                $result_1 = $count_1/24+1;
                $winner_1 = intval(round($result_1)-1);
                $winner_1 = $random_checks_1[$winner_1];
                $random_check_1 = Check::where('id', $winner_1['id'])->first();
                if($random_check_1){
                    Check::where('customer_id', $random_check_1->customer->id)->where('status_1', 'Принят')->update([
                        'status_1' => 'Участвовал в розыгрыше',
                        'date_of_participation' => date('d-m-Y')
                    ]);
                    if($random_check_1->customer->winner != 1){
                        Check::where('id', $random_check_1->id)->update([
                            'status_1' => 'Ожидает заявки',
                            'status_2' => 'Ожидает заявки',
                            'date_of_participation' => date('d-m-Y'),
                            'prize' => 'Шапка'
                        ]);
                        Customer::where('id', $random_check_1->customer->id)->update([
                            'winner' => 1
                        ]);
                        if ($random_check_1->customer->none_mail == 0){
                            Mail::to($random_check_1->customer->email)->send(new CheckAllPrize($random_check_1));
                        }
                    }
                }
                $random_checks_2 = Check::where('status_1', 'Принят')->get()->toArray();
                $count_2 = count($random_checks_2);
                $result_2 = $count_2/24+1;
                $winner_2 = intval(round($result_2)-1);
                $winner_2 = $random_checks_2[$winner_2];
                $random_check_2 = Check::where('id', $winner_2['id'])->first();
                if($random_check_2){
                    Check::where('customer_id', $random_check_2->customer->id)->where('status_1', 'Принят')->update([
                        'status_1' => 'Участвовал в розыгрыше',
                        'date_of_participation' => date('d-m-Y')
                    ]);
                    if($random_check_2->customer->winner != 1){
                        Check::where('id', $random_check_2->id)->update([
                            'status_1' => 'Ожидает заявки',
                            'status_2' => 'Ожидает заявки',
                            'date_of_participation' => date('d-m-Y'),
                            'prize' => 'Шапка'
                        ]);
                        Customer::where('id', $random_check_2->customer->id)->update([
                            'winner' => 1
                        ]);
                        if ($random_check_2->customer->none_mail == 0){
                            Mail::to($random_check_2->customer->email)->send(new CheckAllPrize($random_check_2));
                        }
                    }
                }
                Check::where('status_1', 'Принят')->update([
                    'status_1' => 'Участвовал в розыгрыше',
                    'date_of_participation' => date('d-m-Y')
                ]);
            }elseif($prize_day == 'Sunday'){
                $random_checks = Check::where('status_1', 'Принят')->where('net', 'x5')->get()->toArray();
                $count = count($random_checks);
                $result = $count/24+1;
                $winner = intval(round($result)-1);
                $winner = $random_checks[$winner];
                $random_check = Check::where('id', $winner['id'])->first();
                if($random_check){
                    Check::where('customer_id', $random_check->customer->id)->where('status_1', 'Принят')->update([
                        'status_1' => 'Участвовал в розыгрыше',
                        'date_of_participation' => date('d-m-Y')
                    ]);
                    if($random_check->customer->winner != 1){
                        Check::where('id', $random_check->id)->update([
                            'status_1' => 'Ожидает заявки',
                            'status_2' => 'Ожидает заявки',
                            'date_of_participation' => date('d-m-Y'),
                            'prize' => 'Свитер'
                        ]);
                        Customer::where('id', $random_check->customer->id)->update([
                            'winner' => 1
                        ]);
                        if ($random_check->customer->none_mail == 0){
                            Mail::to($random_check->customer->email)->send(new CheckPrizex5($random_check));
                        }
                    }
                }
                $random_checks_1 = Check::where('status_1', 'Принят')->where('net', 'x5')->get()->toArray();
                $count_1 = count($random_checks_1);
                $result_1 = $count_1/24+1;
                $winner_1 = intval(round($result_1)-1);
                $winner_1 = $random_checks_1[$winner_1];
                $random_check_1 = Check::where('id', $winner_1['id'])->first();
                if($random_check_1){
                    Check::where('customer_id', $random_check_1->customer->id)->where('status_1', 'Принят')->update([
                        'status_1' => 'Участвовал в розыгрыше',
                        'date_of_participation' => date('d-m-Y')
                    ]);
                    if($random_check_1->customer->winner != 1){
                        Check::where('id', $random_check_1->id)->update([
                            'status_1' => 'Ожидает заявки',
                            'status_2' => 'Ожидает заявки',
                            'date_of_participation' => date('d-m-Y'),
                            'prize' => 'Свитер'
                        ]);
                        Customer::where('id', $random_check_1->customer->id)->update([
                            'winner' => 1
                        ]);
                        if ($random_check_1->customer->none_mail == 0){
                            Mail::to($random_check_1->customer->email)->send(new CheckPrizex5($random_check_1));
                        }
                    }
                }
                Check::where('status_1', 'Принят')->where('net', 'x5')->update([
                    'status_1' => 'Участвовал в розыгрыше',
                    'date_of_participation' => date('d-m-Y')
                ]);
            }
        }

    }
}
