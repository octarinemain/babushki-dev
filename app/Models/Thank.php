<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;


class Thank extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'status', 'grand_name', 'name', 'message', 'video', 'image','moderator'
    ];

    public function photo()
    {
        return $this->hasMany('App\Models\PhotoCheck')->orderBy('id', 'DESC');
    }

}
