<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;


class NewCode extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'customer_id','status_1','code_name','reg_index','region','d_city','street','house','flat', 'whom', 'from_whom', 'from_where', 'card_number', 'moderator'
    ];

    public function customer()
    {
        return $this->belongsTo('App\Models\Customer');
    }

}
