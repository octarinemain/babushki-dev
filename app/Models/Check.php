<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;


class Check extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'status_1', 'status_2', 'win_date', 'customer_id', 'date_of_participation', 'net', 'add_net', 'city', 'date_of_buy', 'prize',
        'track_number', 'reg_index', 'region', 'd_city', 'street', 'house', 'flat', 'size', 'moderator', 'sku', 'sku_count',
        'sku_2', 'sku_count_2', 'sku_3', 'sku_count_3', 'sku_4', 'sku_count_4', 'sku_5', 'sku_count_5', 'cancel_is_send', 'message'
    ];

    public function photo()
    {
        return $this->hasMany('App\Models\PhotoCheck')->orderBy('id', 'DESC');
    }

    public function customer()
    {
        return $this->belongsTo('App\Models\Customer');
    }
    public function test()
    {
        return $this->belongsTo('App\Models\Customer')->where('winner',0);
    }

}
