<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\ResetPassword as ResetPasswordNotification;

class Customer extends Authenticatable
{
    use Notifiable;

    protected $guard = 'customer';

    public function sendPasswordResetNotification($token)
    {
        // Your your own implementation.
        $this->notify(new ResetPasswordNotification($token));
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'surname', 'second_name',  'email',  'password', 'confirm_token', 'confirm', 'key_word', 'attempt_count', 'global_attempt_count', 'winner', 'is_show', 'none_mail', 'moderator'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function checks()
    {
        return $this->hasMany('App\Models\Check')->orderBy('id', 'DESC');
    }
    public function codes()
    {
        return $this->hasMany('App\Models\NewCode')->orderBy('id', 'DESC');
    }
    public function new_codes()
    {
        return $this->hasMany('App\Models\NewCode')->where('status_1', 'Ожидает заявки')->orderBy('id', 'DESC');
    }

}
