<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Auth;



class CheckPrizex5 extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($check)
    {
        $this->check = $check;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $check = $this->check;
        $from = env('MAIL_FROM');
        return $this->from($from)->subject('Вы выиграли приз')->view('mail.check-x5')->with(compact(['check']));
    }
}
