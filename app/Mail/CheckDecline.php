<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Auth;



class CheckDecline extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($check,$message)
    {
        $this->check = $check;
        $this->message = $message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $check = $this->check;
        $admin_message = $this->message;
        $from = env('MAIL_FROM');
        return $this->from($from)->subject('Ваш чек отклонён')->view('mail.check-decline')->with(compact(['check','admin_message']));
    }
}
