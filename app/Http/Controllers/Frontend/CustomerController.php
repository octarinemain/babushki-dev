<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Requests\Frontend\Contact;
use App\Http\Requests\Frontend\RegisterStepOne;
use App\Http\Requests\Frontend\RegisterStepTwo;
use App\Mail\ContactMessage;
use App\Models\NewCode;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Mail;
use App\Mail\ConfirmRegistration;
use Auth;
use App\Models\Customer;
use App\Models\DataCode;
use App\Models\RandomWin;
use App\Models\Check;
use \Validator;
use Illuminate\Support\Facades\Input;
use Intervention\Image\ImageManagerStatic as Image;
use \Session;
use Illuminate\Support\Facades\Hash;
use PDF;




class CustomerController extends Controller
{
    public function register_step_3()
    {
        $customer = Session::get('customer');
        $token = str_random(16);
        $customer = Customer::create([
            'name' => $customer[0]['name'],
            'surname' => $customer[0]['surname'],
            'second_name' =>  $customer[0]['second_name'],
            'email' => $customer[0]['email'],
            'password' => Hash::make($customer[1]['password']),
            'key_word' => $customer[1]['password'],
            'confirm_token' => $token,
        ]);

        Mail::to($customer->email)->send(new ConfirmRegistration($customer));
        Session::forget('customer');

        return response()->json(
            '/successful-register'
        );
    }

    public function register_step_1(RegisterStepOne $request)
    {
        Session::forget('customer');
        $customer['name'] = $request->name;
        $customer['surname'] = $request->surname;
        $customer['second_name'] = $request->second_name;
        $customer['email'] = $request->email;
        Session::push('customer', $customer);
        return response()->json(
            'true'
        );
    }

    public function register_step_2(RegisterStepTwo $request)
    {
        $customer['password'] = $request->password;
        Session::push('customer', $customer);
        return response()->json(
            'true'
        );
    }
    public function refreshCaptcha()
    {
        return response()->json(['captcha'=> captcha_img('flat')]);
    }

    public function checkToken($token)
    {
        $customer = Customer::where('confirm_token', $token)->first();

        if($customer != '') {
            Customer::where('id', $customer->id)->update([
                'confirm' => 1
            ]);
            if (Auth::guard('customer')->attempt(['email' => $customer->email, 'password' => $customer->key_word])) {
                $accessToken = true;
                return view('frontend.customer.profile')->with(compact(['accessToken']));
            }else{
                return view('frontend.pages.home');
            }
        }else{
            return view('frontend.pages.home');
        }
    }

    public function createCode()
    {
        $file = 'http://xn--90aiakgkqi1l.xn--p1ai/public/codes.txt';
        $contents = file_get_contents($file);
        $codes = explode("\r\n", $contents);
        foreach ($codes as $code)
        {
            DataCode::create([
                'name' => $code
            ]);
        }
    }

    public function sendMessage(Contact $request){
        Mail::to('info@jubileynoe.ru')->send(new ContactMessage($request));
    }

    public function unsubscribe(Request $request){
        Customer::where('id', $request->id)->where('email', $request->email)->update([
            'none_mail' => 1
        ]);
    }
}
