<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Thank;



class ThankController extends Controller
{
    public function create(Request $request)
    {
        if($request->video){
            $video = $request->video;
            $video->store('Video');
            $video_name = $video->hashName();
        }else{
            $video_name = '';
        }
        if($request->image){
            $image = $request->image;
            $image->store('GrandImage');
            $image_name = $image->hashName();
        }else{
            $image_name = '';
        }
        Thank::create([
            'grand_name' => $request->grand_name,
            'name' => $request->name,
            'message' => $request->message,
            'video' => $video_name,
            'image' => $image_name,
            'status' => 'На модерации'
        ]);

        return response()->json(
            'true'
        );

    }



}
