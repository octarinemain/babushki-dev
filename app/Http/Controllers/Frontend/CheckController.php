<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Requests\Frontend\UploadCheck;
use App\Mail\CheckAllPrize;
use App\Mail\CheckDecline;
use App\Mail\CheckPrizex5;
use App\Mail\CheckRegister;
use App\Models\PhotoCheck;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Mail;
use Auth;
use App\Models\Customer;
use App\Models\Check;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;



class CheckController extends Controller
{
    public function create(Request $request)
    {
        if(!$request->photo_ids || $request->photo_ids  == ''){
            $error = array();
            $error['image'][0] = 'Необходимо загрузить хоть одно фото.';
            return response()->json([
                'errors' => $error
            ], 422);
        }

        $check = Check::create([
            'customer_id' => Auth::guard('customer')->user()->id,
            'status_1' => 'На модерации'
        ]);

        $check->load('customer');
        $photo_ids = explode(",", $request->photo_ids);
        foreach($photo_ids as $photo_id){
            $photo = PhotoCheck::where('id', $photo_id)->update([
                'check_id' => $check->id,
            ]);
        }
        if ($check->customer->none_mail == 0) {
            Mail::to($check->customer->email)->send(new CheckRegister($check));
        }

        return response()->json(
            'true'
        );
    }


    public function upload(UploadCheck $request)
    {
        foreach($request->image as $file){
            $contents = file_get_contents($file);
            $str_ran = str_random(20);

            Image::make($contents)->save(public_path('/storage/Check/' . $str_ran . '.jpg'), 30);
            $photo = PhotoCheck::create([
                'check_id' => 0,
                'image' => $str_ran.'.jpg'
            ]);
            $photos = [];
            $photo_object = new \stdClass();
            $photo_object->name = $str_ran.'.jpg';
            $photo_object->fileID = $photo->id;
            $photos[] = $photo_object;

        }
        return response()->json(
            array('files' => $photos), 200
        );
    }

    public function deletePhoto(Request $request)
    {
        $photo = PhotoCheck::find($request->id);
        Storage::delete('Check/'.$photo->image);
        $photo->delete();
        return response()->json(
            'true'
        );
    }
    public function optimizeImage()
    {
        $files = Storage::files('Check');
        foreach ($files as $file){
            $contents = Storage::get($file);
            Storage::delete($file);
            Image::make($contents)->save(public_path('/storage/' . $file), 30);
        }
    }

    public function getPrize(Request $request){

        Check::where('id', $request->check_id)->update([
            'reg_index' => $request->reg_index,
            'region' => $request->region,
            'd_city' => $request->city,
            'street' => $request->street,
            'house' => $request->house,
            'flat' => $request->flat,
            'size' => $request->size,
            'status_2' => 'Ожидает отправки',
        ]);

        return response()->json(
            'true'
        );
    }

    public function winner(){
        $winners = Check::where('prize', '!=', '')->get();
        $winners->load('customer');
        return view('frontend.pages.winner')->with(compact(['winners']));
    }

    public function test(){
        $check = Check::where('id', 3031)->first();
        Check::where('id', 3031)->update([
            'status_1' => 'Ожидает заявки',
            'status_2' => 'Ожидает заявки',
            'date_of_participation' => date('d-m-Y'),
            'prize' => 'Шапка'
        ]);

        Customer::where('id', 1270)->update([
            'winner' => 1
        ]);
        Mail::to('t.lilek80@mail.ru')->send(new CheckAllPrize($check));

    }

}
