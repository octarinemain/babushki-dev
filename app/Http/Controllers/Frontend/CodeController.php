<?php

namespace App\Http\Controllers\Frontend;

use App\Mail\CodeRegister;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Mail;
use App\Models\Customer;
use App\Models\NewCode;
use App\Models\DataCode;



class CodeController extends Controller
{

    public function check(Request $request)
    {
        if(Auth::guard('customer')->user()->global_attempt_count > 99){
            $error = array();
            $error['error'][0] = 'Код отклонён.';
            return response()->json([
                'errors' => $error
            ], 422);
        }else{
            $data_code = DataCode::where('name', $request->code_name)->where('active', 1)->first();
            if($data_code){
                if(Auth::guard('customer')->user()->attempt_count < 15){

                    Customer::where('id', Auth::guard('customer')->user()->id)->update([
                        'attempt_count' => Auth::guard('customer')->user()->attempt_count + 1,
                        'global_attempt_count' => Auth::guard('customer')->user()->global_attempt_count + 1
                    ]);
                    $code = NewCode::create([
                        'code_name' => $request->code_name,
                        'status_1' => 'Ожидает заявки',
                        'customer_id' => Auth::guard('customer')->user()->id,
                    ]);
                    $code->load('customer');
                    DataCode::where('id', $data_code->id)->update([
                        'active' => 0
                    ]);
                    if ($code->customer->none_mail == 0){
                        Mail::to($code->customer->email)->send(new CodeRegister($code));
                    }

                }else{
                    $error = array();
                    $error['error'][0] = 'Код отклонён.';
                    return response()->json([
                        'errors' => $error
                    ], 422);
                }
            }else{
                Customer::where('id', Auth::guard('customer')->user()->id)->update([
                    'attempt_count' => Auth::guard('customer')->user()->attempt_count + 1,
                    'global_attempt_count' => Auth::guard('customer')->user()->global_attempt_count + 1
                ]);
                $error = array();
                $error['error'][0] = 'Код отклонён.';
                return response()->json([
                    'errors' => $error
                ], 422);
            }
        }


        return response()->json(
            'true'
        );
    }

    public function getCard(Request $request){

        if($request->code_name_hide){
            NewCode::where('code_name', $request->code_name_hide)->update([
                'reg_index' => $request->reg_index,
                'region' => $request->region,
                'd_city' => $request->city,
                'street' => $request->street,
                'house' => $request->house,
                'flat' => $request->flat,
                'whom' => $request->whom,
                'from_where' => $request->from_where,
                'card_number' => $request->card_number,
                'from_whom' => $request->from_whom,
                'status_1' => 'Открытка отправлена',
            ]);

            return response()->json(
                'true'
            );

        }else{
            $new_code = NewCode::where('customer_id', Auth::guard('customer')->user()->id)->first();
            if($new_code){
                NewCode::where('id', $new_code->id)->update([
                    'reg_index' => $request->reg_index,
                    'region' => $request->region,
                    'd_city' => $request->city,
                    'street' => $request->street,
                    'house' => $request->house,
                    'flat' => $request->flat,
                    'whom' => $request->whom,
                    'from_where' => $request->from_where,
                    'card_number' => $request->card_number,
                    'from_whom' => $request->from_whom,
                    'status_1' => 'Открытка отправлена',
                ]);
            }
        }
    }


}
