<?php

namespace App\Http\Controllers\Auth;

use App\Http\Requests\Frontend\Login;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use \Validator;
use Illuminate\Support\Facades\Input;
use App\Models\Customer;



class CustomerLoginController extends Controller
{
    public function login(Login $request)
    {
        if(!$this->checkConfirm($request->email)){
            $error = array();
            $error['email'][0] = 'Данная почта не подтверждена.';
            return response()->json([
                'errors' => $error
            ], 422);
        }
        if(Auth::guard('customer')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember))
        {
            return response()->json(
                '/customer/profile'
            );
        }else{
            $error = array();
            $error['password'][0] = 'Пароль неправильный.';
            return response()->json([
                'errors' => $error
            ], 422);
        }
    }


    public function checkConfirm($email){
        $customer = Customer::where('email', $email)->first();
        if($customer->confirm == 1){
            return true;
        }else{
            return false;
        }
    }
}
