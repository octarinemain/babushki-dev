<?php

namespace App\Http\Controllers\Admin;

use App\Models\Customer;
use App\Models\Check;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Mail;
use App\Mail\CheckDecline;
use App\Mail\CheckAccept;
use Excel;
use Auth;
use Illuminate\Support\Facades\Storage;



class CheckController extends Controller
{

    public function update(Request $request)
    {
        $check = Check::where('id', $request->check_id)->first();
        if($request->date_of_buy == ''){
            if($check->date_of_buy == ''){
                $request->date_of_buy = '';
            }else{
                $request->date_of_buy = date('Y-m-d',strtotime($check->date_of_buy));
            }
        }else{
            $request->date_of_buy = date('Y-m-d',strtotime($request->date_of_buy));
        }
           Check::where('id', $request->check_id)->update([
                'date_of_buy' => $request->date_of_buy,
                'city' => $request->city,
                'net' => $request->net,
                'add_net' => $request->add_net,
                'sku' => $request->sku,
                'sku_count' => $request->sku_count,
                'sku_2' => $request->sku_2,
                'sku_count_2' => $request->sku_count_2,
                'sku_3' => $request->sku_3,
                'sku_count_3' => $request->sku_count_3,
                'sku_4' => $request->sku_4,
                'sku_count_4' => $request->sku_count_4,
                'sku_5' => $request->sku_5,
                'sku_count_5' => $request->sku_count_5,
                'status_1' => $request->status_1,
                'message' => $request->message,
                'moderator' => Auth::guard('admin')->user()->name,
            ]);
        $check->load('customer');
        if ($check->customer->none_mail == 0){
            if($request->status_1 == 'Отклонен' && $request->message != '' && $check->cancel_is_send == 0){
                Check::where('id', $request->check_id)->update([
                    'cancel_is_send' => 1,
                ]);
                Mail::to($check->customer->email)->send(new CheckDecline($check,$request->message));
            }
            if($request->status_1 == 'Принят'){
                Mail::to($check->customer->email)->send(new CheckAccept($check));
            }
        }


        return response([
            'status' => 'success',
        ], 200);

    }

    public function update_wins(Request $request)
    {
        $check = Check::where('id', $request->check_id)->first();
        if($request->date_of_buy == ''){
            if($check->date_of_buy == ''){
                $request->date_of_buy = '';
            }else{
                $request->date_of_buy = date('d-m-Y',strtotime($check->date_of_buy));
            }
        }else{
            $request->date_of_buy = date('d-m-Y',strtotime($request->date_of_buy));
        }
            Check::where('id', $request->check_id)->update([
                'track_number' => $request->track_number,
                'status_2' => $request->status_2,
                'moderator' => Auth::guard('admin')->user()->name,

            ]);

        return response([
            'status' => 'success',
        ], 200);

    }

    public function delete($id)
    {
        $faq = Faq::where('id', $id)->first();
        $faq->delete();
        return response([
            'status' => 'success',
        ], 200);
    }

    public function filter(Request $request)
    {
        $query_filter = Check::orderby('id', 'DESC');
        if($request->filter_surname){
            $surname = $request->filter_surname;
            $query_filter->whereHas('customer', function ($query) use ($surname){
                $query->where('surname', 'LIKE' , '%'.$surname.'%');
            });
        }

        if($request->filter_date_of_buy){
            $query_filter->where('date_of_buy', date('d-m-Y', strtotime($request->filter_date_of_buy)));
        }
        if($request->filter_city){
            $query_filter->where('city', $request->filter_city);
        }
        if($request->filter_net){
            $query_filter->where('net', $request->filter_net);
        }
        if($request->filter_created_at){
            $query_filter->whereRaw('date(created_at) = ?', $request->filter_created_at);
        }
        if($request->filter_date_of_participation){
            $query_filter->where('date_of_participation',  date('d-m-Y', strtotime($request->filter_date_of_participation)));
        }
        if($request->filter_status_1){
            $query_filter->where('status_1', $request->filter_status_1);
        }
        $checks = $query_filter->take(500)->get();
        $checks->load('customer');
        $checks->load('photo');
        return $checks;
    }

    public function export(Request $request)
    {
        Storage::delete('Все чеки.xlsx');
        $query_filter = Check::orderby('id', 'DESC');
        if($request->filter_surname){
            $surname = $request->filter_surname;
            $query_filter->whereHas('customer', function ($query) use ($surname){
                $query->where('surname', 'LIKE' , '%'.$surname.'%');
            });
        }

        if($request->filter_date_of_buy){
            $query_filter->where('date_of_buy', date('d-m-Y', strtotime($request->filter_date_of_buy)));
        }
        if($request->filter_city){
            $query_filter->where('city', $request->filter_city);
        }
        if($request->filter_net){
            $query_filter->where('net', $request->filter_net);
        }
        if($request->filter_created_at){
            $query_filter->whereRaw('date(created_at) = ?', $request->filter_created_at);
        }
        if($request->filter_date_of_participation){
            $query_filter->where('date_of_participation',  date('d-m-Y', strtotime($request->filter_date_of_participation)));
        }
        if($request->filter_status_1){
            $query_filter->where('status_1', $request->filter_status_1);
        }
        if($request->filter_sku){
            $query_filter->where('sku', $request->filter_sku);
        }
        $checks = $query_filter->get();
        $checks->load('customer');
      Excel::create('Все чеки 20000++', function($excel) use ($checks) {
            $excel->sheet('First sheet', function($sheet)use($checks) {
                $i = 2;
                $sheet->row(1, array(
                    'id', 'ФИО', 'Город', 'Сеть для розыгрыша', 'Сеть для аналитики', 'SKU', 'SKU 2', 'SKU 3', 'SKU 4', 'SKU 5', 'Дата покупки', 'Дата учатсия в розыгрыше', 'Приз', 'Размер', 'Трек номер', 'Индекс', 'Регион', 'Город (доставка)', 'Улица', 'Дом', 'Квартира', 'Дата регистрации чека', 'Статус', 'Статус для победителей', 'Причина отклона', 'Модератор'
                ));
                foreach($checks as $check){
                    $sheet->row($i, array(
                        $check->id, $check->customer->surname.' '.$check->customer->name.' '.$check->customer->second_name,
                        $check->city,$check->net,$check->add_net,
                        $check->sku.' '.$check->sku_count.' шт.',
                        $check->sku_2.' '.$check->sku_count_2.' шт.',
                        $check->sku_3.' '.$check->sku_count_3.' шт.',
                        $check->sku_4.' '.$check->sku_count_4.' шт.',
                        $check->sku_5.' '.$check->sku_count_5.' шт.',
                        $check->date_of_buy,$check->date_of_participation,
                        $check->prize,$check->size,$check->track_number,$check->reg_index,$check->region,
                        $check->d_city,$check->street,$check->house,$check->flat,$check->created_at,
                        $check->status_1,$check->status_2, $check->message,$check->moderator
                    ));
                    $i++;
                }
            })->store('xlsx',public_path('/storage'));
        });

    }

    public function doownloadFile(){
        return response()->download(public_path('/storage/Все чеки 20000++.xlsx'));
    }

    public function downloadWinners(Request $request)
    {
        $checks = Check::orderby('id', 'DESC')->where('prize', '!=', '')->get();
        $checks->load('customer');
        Excel::create('Все победители', function($excel) use ($checks) {
            $excel->sheet('First sheet', function($sheet)use($checks) {
                $i = 2;
                $sheet->row(1, array(
                    'id', 'ФИО', 'Город', 'Сеть для розыгрыша', 'Сеть для аналитики', 'SKU', 'SKU 2', 'SKU 3', 'SKU 4', 'SKU 5', 'Дата покупки', 'Дата учатсия в розыгрыше', 'Приз', 'Размер', 'Трек номер', 'Индекс', 'Регион', 'Город (доставка)', 'Улица', 'Дом', 'Квартира', 'Дата регистрации чека', 'Статус', 'Статус для победителей', 'Причина отклона', 'Модератор'
                ));
                foreach($checks as $check){
                    $sheet->row($i, array(
                        $check->id, $check->customer->surname.' '.$check->customer->name.' '.$check->customer->second_name,
                        $check->city,$check->net,$check->add_net,
                        $check->sku.' '.$check->sku_count.' шт.',
                        $check->sku_2.' '.$check->sku_count_2.' шт.',
                        $check->sku_3.' '.$check->sku_count_3.' шт.',
                        $check->sku_4.' '.$check->sku_count_4.' шт.',
                        $check->sku_5.' '.$check->sku_count_5.' шт.',
                        $check->date_of_buy,$check->date_of_participation,
                        $check->prize,$check->size,$check->track_number,$check->reg_index,$check->region,
                        $check->d_city,$check->street,$check->house,$check->flat,$check->created_at,
                        $check->status_1,$check->status_2, $check->message, $check->moderator
                    ));
                    $i++;
                }
            })->download('xlsx');
        });

    }





}
