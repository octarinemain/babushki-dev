<?php

namespace App\Http\Controllers\Admin;

use App\Models\Thank;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Mail;
use Illuminate\Support\Facades\Storage;
use Auth;
use Excel;



class ThankController extends Controller
{

    public function update(Request $request)
    {
        Thank::where('id', $request->thank_id)->update([
                'status' => $request->status,
            'moderator' => Auth::guard('admin')->user()->name,

        ]);

        return response([
            'status' => 'success',
        ], 200);

    }

    public function delete($id)
    {
        $thank = Thank::where('id', $id)->first();
        Storage::delete('Video/' . $thank->video);
        Storage::delete('GrandImage/' . $thank->image);
        $thank->delete();
        return response([
            'status' => 'success',
        ], 200);
    }

    public function doownloadFile(Request $request)
    {

        $thanks = Thank::orderby('id', 'DESC')->get();

        Excel::create('Все благодарности', function($excel) use ($thanks) {
            $excel->sheet('First sheet', function($sheet)use($thanks) {
                $i = 2;
                $sheet->row(1, array(
                    'id', 'ФИО', 'Кому',  'Сообщение', 'Статус',  'Дата создания','Модератор'
                ));
                foreach($thanks as $thank){
                    $sheet->row($i, array(
                        $thank->id, $thank->name,
                        $thank->grand_name, $thank->message,$thank->status,$thank->created_at,$thank->moderator,

                    ));
                    $i++;
                }
            })->download('xlsx');
        });

    }

}
