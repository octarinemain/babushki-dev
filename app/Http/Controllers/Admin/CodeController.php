<?php

namespace App\Http\Controllers\Admin;

use App\Models\Customer;
use App\Models\NewCode;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Mail;
use Auth;
use Excel;





class CodeController extends Controller
{

    public function update(Request $request)
    {
        NewCode::where('id', $request->code_id)->update([
                'status_1' => $request->status_1,
            'moderator' => Auth::guard('admin')->user()->name,

        ]);

        return response([
            'status' => 'success',
        ], 200);

    }

    public function doownloadFile(Request $request)
    {
       
        $codes = NewCode::orderby('id', 'DESC')->get();
        $codes->load('customer');
        
        Excel::create('Все коды', function($excel) use ($codes) {
            $excel->sheet('First sheet', function($sheet)use($codes) {
                $i = 2;
                $sheet->row(1, array(
                    'id', 'ФИО', 'Код',  'Кому', 'От кого', 'От куда', 'Индекс', 'Регион', 'Город', 'Улица', 'Дом', 'Квартира', 'Дата регистрации кода', 'Статус', 'Номер открытки', 'Модератор'
                ));
                foreach($codes as $code){
                    $sheet->row($i, array(
                        $code->id, $code->customer->surname.' '.$code->customer->name.' '.$code->customer->second_name,
                        $code->code_name,$code->whom,$code->from_whom,$code->from_where,$code->reg_index,$code->region,
                        $code->d_city,$code->street,$code->house,$code->flat,$code->created_at,
                        $code->status_1, $code->card_number, $code->moderator
                    ));
                    $i++;
                }
            })->download('xlsx');
        });

    }

}
