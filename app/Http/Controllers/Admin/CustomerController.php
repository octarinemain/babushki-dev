<?php

namespace App\Http\Controllers\Admin;

use App\Models\Customer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Auth;
use Excel;




class CustomerController extends Controller
{

    public function create(Request $request)
    {
        if($request->customer_id){
            Customer::where('id', $request->customer_id)->update([
                'surname' => $request->surname,
                'name' => $request->name,
                'second_name' => $request->second_name,
                'email' => $request->email,
                'moderator' => Auth::guard('admin')->user()->name,
            ]);
        }

        return response([
            'status' => 'success',
        ], 200);

    }

    public function doownloadFile()
    {
        $customers = Customer::orderby('id', 'DESC')->get();

        Excel::create('Все пользователи', function($excel) use ($customers) {
            $excel->sheet('First sheet', function($sheet)use($customers) {
                $i = 2;
                $sheet->row(1, array(
                    'id', 'ФИО', 'почта', 'Модератор'
                ));
                foreach($customers as $customer){
                    $sheet->row($i, array(
                        $customer->id, $customer->surname.' '.$customer->name.' '.$customer->second_name, $customer->email, $customer->moderator
                    ));
                    $i++;
                }
            })->download('xlsx');
        });

    }

}
