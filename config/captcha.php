<?php

return [

    'characters' => '2346789abcdefghjmnpqrtuxyz',

    'default'   => [
        'length'    => 5,
        'width'     => 80,
        'height'    => 36,
        'quality'   => 90,
        'lines'     => 0,
        'bgImage'   => false,
        'bgColor'   => '#fff',
        'fontColors'=> ['#000'],
        'contrast'  => -5,
    ],

    'flat'   => [
        'length'    => 8,
        'width'     => 80,
        'height'    => 36,
        'quality'   => 90,
        'lines'     => 0,
        'bgImage'   => false,
        'bgColor'   => '#fff',
        'fontColors'=> ['#000'],
        'contrast'  => -5,
    ],

    'mini'   => [
        'length'    => 3,
        'width'     => 60,
        'height'    => 32,
    ],

    'inverse'   => [
        'length'    => 5,
        'width'     => 120,
        'height'    => 36,
        'quality'   => 90,
        'sensitive' => true,
        'angle'     => 12,
        'sharpen'   => 10,
        'blur'      => 2,
        'invert'    => true,
        'contrast'  => -5,
    ]

];
